/**
* \class AIComponent
*
* \brief Component to control the behaviour of enemies
*
* This class is the base to create new IA Components which inherit
* from this.
*
* Created on: 2016-06-08
*/

#ifndef AIComponent_H
#define AIComponent_H

#include "Component.h"

class AIComponent : public Component {

public:
    /// Constructor
    AIComponent(std::shared_ptr<GameObject> gameObject);
    /// Destructor
    virtual ~AIComponent();

protected:
};

#endif
