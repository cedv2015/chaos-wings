/**
* \class AIMissileComponent
*
* \brief AI component for missiles
*
* This component manages the behaviour of enemy missiles those
* enemies will just choose a player (if there are more than one) and follow him
* until they are destroyed or they surpass the player's Z position.
*
* Created on: 2016-06-09
*/

#ifndef AIMISSILECOMPONENT_H
#define AIMISSILECOMPONENT_H

#include <Ogre.h>
#include "AIComponent.h"
#include "PhysicsComponent.h"
#include "CameraManager.h"

class AIMissileComponent : public AIComponent {
public:
    /// Constructor
    AIMissileComponent(std::shared_ptr<GameObject> gameObject, std::shared_ptr<PhysicsComponent> physicsCmp);
    /// Destructor
    virtual ~AIMissileComponent();

    /**
     * This method manages the behaviour of the physical body of the missile.
     * It checks the objective's position and changes it's direction
     * accordingly. */
    void update (float delta) override;

    /// Acceleration of the missile movements
    static const float ACCELERATION;
    /// Max speed of the missile
    static const float MAX_VELOCITY;

protected:
    /// SceneNode of the objective of the missile
    Ogre::SceneNode *_objective;
    /// Physical component of the missile
    std::shared_ptr<PhysicsComponent> _physicsCmp;
    /**
     * Pointer to the CameraManager of the game. This is necessary to get the
     * objective when the missile is created or when the objective player is
     * removed from the game */
    CameraManager *_cameraMgr;

    /// Velocity of the missile in a certain point in time
    Ogre::Vector3 _velocity;
};

#endif
