/**
* \class BurstWeaponComponent
*
* \brief Specialization of WeaponComponent
*
* This weapon creates a number of bullets specified in its constructor instead
* of shooting once. This specialization was necessary to save the angle it's
* shooting and the bullets remaining.
*
* Created on: 2016-06-04
*/

#ifndef BURSTWEAPONCOMPONENT_H
#define BURSTWEAPONCOMPONENT_H

#include "WeaponComponent.h"

class BurstWeaponComponent : public WeaponComponent{
public:
    /// Constructor
    BurstWeaponComponent(std::shared_ptr<GameObject> gameObject, std::shared_ptr<RigidBodyAnimationComponent> animatorCmp, float min_frequence, float max_frequence, int number_of_shots, float time_between_shots, Ogre::Vector3 relative_position = Ogre::Vector3::ZERO) :
        WeaponComponent(gameObject, animatorCmp, min_frequence, max_frequence, WeaponComponent::BulletType::NORMAL, relative_position),
        _number_of_shots(number_of_shots),
        _time_between_shots(time_between_shots),
        _shots_remaining(number_of_shots),
        _angle_burst(0) {
        };
    /// Destructor
    virtual ~BurstWeaponComponent() {};

    /**
     * This method resets all parameters of this weapon, it's called when the
     * state of the owner GameObject is changed */
    virtual void resetCounters () {
        _shots_remaining = _number_of_shots;
    };

    /**
     * This method creates new bursts when a certain time has passed. The angle
     * of this burst is selected randomly, but all bullets in a burst will have
     * this angle */
    void update (float delta) override;
    /// With of the arc of the burst
    static const float BURST_WIDTH;
protected:
    /// Number of bullets in a burst
    int _number_of_shots;
    /// Time between bursts
    float _time_between_shots;
    /// Number of shoots remaining in a burst
    int _shots_remaining;
    /// Angle used to shoot the bullets
    int _angle_burst;
};

#endif
