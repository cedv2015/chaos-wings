/**
* \class CEGUIComponent
*
* \brief Component to control graphical user interfaces
*
* Some Game Object need their own interfaces,
* players need a flag with his life and the time remaining until
* the invulnerability, and final boss need a lifebar
*
* Created on: 2016-06-05
*/

#ifndef CEGUICOMPONENT_H
#define CEGUICOMPONENT_H

#include "Component.h"
#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI.h>

class CEGUIComponent : public Component {
public:
    /// Constructor
    CEGUIComponent(std::shared_ptr<GameObject> gameObject, int id, int type);
    /// Destructor
    virtual ~CEGUIComponent() {
        if(_hud)
            CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->removeChild(_hud);
    };
    /**
     * This method updates the hud of the players and the lifebar
     * of the bosses when they appear in the game. */
    void update (float delta) override;

    enum Type {
        PLAYER,
        FINALBOSS1,
        PHASE
    };
    void visibilityHUD(bool visibility) { _hud->setVisible(visibility); };

protected:
    int _id, _type;
    bool _finish;

    /// CEGUI Windows are the visual part
    CEGUI::Window *_hud, *_windowScore, *_windowHUD, *_lifeWindow, *_invulnerabilityWindow;
    CEGUI::USize _size, _sizeInvulnerability;
    CEGUI::UVector2 _position, _positionInvulnerability;

    /// Instance that allows to do animations
    CEGUI::AnimationInstance* _lifeAnimInstance;

};

#endif
