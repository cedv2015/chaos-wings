/**
* \class Component
*
* \brief Components make GameObjects have properties
*
* Components are the second most importants class of the game. These classes may
* contain AI, animations, input management, SceneNodes, particles and any other
* property.
*
* Created on: 2016-05-17
*/

#ifndef Component_H
#define Component_H

#include <memory>

class GameObject;

class Component {
public:
    /// Constructor
    Component(std::shared_ptr<GameObject> gameObject, int type) : _gameObject(gameObject), _type(type){};
    /// Destructor
    virtual ~Component() {
        _gameObject = nullptr;
    };

    /**
     * This virtual method is called every frame by it's owner GameObject. It
     * will be overwritten by the Component which inherits from it. */
    virtual void update(float deltaT) {};

    /// Returns the type of the Component
    int getType () { return _type; };

    /// Returns if the GameObject has ended or not
    virtual bool isFinished () { return false; };

    /// Types of Component
    enum Type {
        AI,
        ANIMATION,
        CAMERA,
        INPUT,
        NODE,
        PARTICLES,
        PHYSICS,
        SOUND,
        STATUS,
        RIGIDBODYANIMATION,
        WEAPON,
        CEGUI
    };

protected:
    /// GameObject owner of this Component
    std::shared_ptr<GameObject> _gameObject;
    /// Type of the Component
    int _type;
};

#endif
