/**
* \class InputComponent
*
* \brief Controls the player's behaviour
*
* This component controls the player's movement and attack. It checks the
* keys/buttons pressed and moves the player accordingly and make him attack.
*
* \note
*
* Created on: 2016-05-17
*/

#ifndef InputComponent_H
#define InputComponent_H

#include "Component.h"
#include <Ogre.h>
#include "PhysicsComponent.h"
#include "GameObjectCreator.h"
#include "CameraManager.h"
#include "StatusComponent.h"

class InputComponent : public Component {
public:
    /// Constructor
    InputComponent (std::shared_ptr<GameObject> gameObject, int player_number, std::shared_ptr<PhysicsComponent> physicsComponent) : Component(gameObject, Component::Type::INPUT), _eyehole_node(nullptr), _eyehole_entity(nullptr), _gameObjectCreator(nullptr), _button1_pressed(false), _button2_pressed(false), _player_number(player_number), _physicsComponent(physicsComponent), _player_eyehole_distance(0), _time_since_last_shot(0), _time_to_next_invulnerability(0), _camera(nullptr), _statusCmp(nullptr) {
        _sceneMgr = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
        _eyehole_entity = _sceneMgr->createEntity("EyeholeEntity" + std::to_string(_player_number), "Eyehole" + std::to_string(_player_number) + ".mesh");
        _eyehole_node = _sceneMgr->createSceneNode("EyeholeNode" + std::to_string(_player_number));
        _sceneMgr->getRootSceneNode()->addChild(_eyehole_node);
        _eyehole_node->attachObject(_eyehole_entity);
        _eyehole_node->translate(Ogre::Vector3(0, 0, -18));
        _player_eyehole_distance = _physicsComponent->getPosition().z - _eyehole_node->getPosition().z;
        _gameObjectCreator = GameObjectCreator::getSingletonPtr();
        _camera = CameraManager::getSingletonPtr()->getCamera();
    };

    /// Destructor
    virtual ~InputComponent () {
        if (_eyehole_node) {
            _eyehole_node->getParent()->removeChild(_eyehole_node);
            _eyehole_node->detachAllObjects();
            _sceneMgr->destroySceneNode(_eyehole_node);
            _eyehole_node = nullptr;
        }
        if (_eyehole_entity) {
            _sceneMgr->destroyEntity(_eyehole_entity);
            _eyehole_entity = nullptr;
        }
        _statusCmp = nullptr;
        _sceneMgr = nullptr;
        _physicsComponent = nullptr;
        _camera = nullptr;
    };

    /// This method is used to asign the status of the owner GameObject (Player).
    void setStatusComponent (std::shared_ptr<StatusComponent> statusCmp) { _statusCmp = statusCmp; };

    /**
     * This method returns the position of the eyehole. The Z axis is not
     * neccesary because it will never change. */
    Ogre::Vector2 getPosition () {
        if (_eyehole_node)
            return Ogre::Vector2(_eyehole_node->getPosition().x, _eyehole_node->getPosition().y);
        else
            return Ogre::Vector2(0, 0);
    };

    /// This method returns the position in the X axis of the eyehole.
    float getX () {
        if (_eyehole_node)
            return _eyehole_node->getPosition().x;
        else
            return 0;
    }
    /// This method returns the position in the Y axis of the eyehole.
    float getY () {
        if (_eyehole_node)
            return _eyehole_node->getPosition().y;
        else
            return 0;
    }

    /**
     * This method checks the position of the eyehole and moves and rotates the
     * player accordingly. Also, it checks if the buttons have been pressed. If
     * the button 1 is pressed, it creates player bullets in the direction
     * specified by the eyehole. If the button 2 is pressed, it makes the player
     * invulnerable for a little time. */
    virtual void update (float delta);
    /// This method changes the visibility of the eyehole
    void visibilityEyehole(bool visibility);

    /**
     * This method makes the player invulnerable of a little time and change
     * the invulnerability timer to it's maximum value */
    void secondButtonReleased ();
    /// This method returns the maximum cooldown of the invulnerability ability
    float getTimeInvulnerabilityCooldown() { return TIME_INVULNERABILITY_COOLDOWN; };
    /// This method returns the actual cooldown of the invulnerability ability
    float getTimeToNextInvulnerability () { return _time_to_next_invulnerability; };

    /// Maximum eyehole's horizontal position
    static const float HORIZONTAL_LIMIT;
    /// Maximum eyehole's vertical position
    static const float VERTICAL_LIMIT;
    /// Minimum distance between the eyehole and the player to move it
    static const float MINIMUM_DISTANCE;
    /// Maximum speed of the player to follow the eyehole
    static const float MAXIMUM_SPEED;
    /// Acceleration of the player
    static const float ACCELERATION;
    /// Time between the player's shoots
    static const float TIME_BETWEEN_SHOTS;
    /// Maximum range of the joystick
    static const float RANGE_JOYSTICK;
    /// Maximum rotation of the player
    static const float ROTATIONLIMIT;
    /// Maximum invlunerability's cooldown time
    static const float TIME_INVULNERABILITY_COOLDOWN;
    /// Time in which the player is invulnerable when this ability is active
    static const float TIME_INVULNERABLE;

    /// Types of controller
    enum Type {
        KEYBOARD, JOYSTICK
    };
protected:
    Ogre::SceneNode *_eyehole_node;
    Ogre::Entity *_eyehole_entity;
    Ogre::SceneManager *_sceneMgr;
    GameObjectCreator *_gameObjectCreator;
    bool _button1_pressed;
    bool _button2_pressed;
    /// Id of the Player
    int _player_number;
    /// PhysicsComponent of the Player
    std::shared_ptr<PhysicsComponent> _physicsComponent;
    float _player_eyehole_distance;
    float _time_since_last_shot;
    float _time_to_next_invulnerability;

    /// This function rotates the player depending on it's distance to the eyehole
    void checkRotation (btQuaternion& rotation);

    Ogre::Camera *_camera;
    /// StatusComponent of the Player
    std::shared_ptr<StatusComponent> _statusCmp;
};

#endif
