/**
* \class InputJoystickComponent
*
* \brief Component to control joystick input
*
* This component controls the buttons pressed and the movement of the joystick.
*
* \note
*
* Created on: 2016-05-17
*/

#ifndef INPUTJOYSTICKCOMPONENT_H
#define INPUTJOYSTICKCOMPONENT_H

#include "InputComponent.h"
#include "InputManager.h"

class InputJoystickComponent : public InputComponent, public OIS::JoyStickListener{
public:

    /// Constructor
    InputJoystickComponent (std::shared_ptr<GameObject> gameObject, int player_number, std::shared_ptr<PhysicsComponent> physicsComponent);
    /// Destructor
    ~InputJoystickComponent () {
        InputManager *inputMgr = InputManager::getSingletonPtr();
        inputMgr->removeJoystickListener(this);
    };
    bool povMoved( const OIS::JoyStickEvent &e, int pov );
    /// This method checks if the gamepad's joystick has been moved
    bool axisMoved( const OIS::JoyStickEvent &e, int axis );
    bool sliderMoved( const OIS::JoyStickEvent &e, int sliderID );
    /// This method checks if the buttons have been pressed
    bool buttonPressed( const OIS::JoyStickEvent &e, int button );
    /// This method checks if the buttons have been released
    bool buttonReleased ( const OIS::JoyStickEvent &e, int button);

    
    /**
     * This method checks if the parameters of the eyehole's position have been
     * changed and moves the eyehole accordingly. */
    virtual void update (float delta);
protected:
    /// Sensivity of the gamepad's joystick
    float _sensivity;
    /// Parameter to avoid light movements of the joystick
    float _dead_zone;
    /// Eyehole's position in X axis
    float _x_position;
    /// Eyehole's position in Y axis
    float _y_position;

    /// Map of gamepad's buttons
    enum Constants
        {
            JOYSTICK_BUTTON_1 = 0,
            JOYSTICK_BUTTON_2 = 2
        };
};

#endif
