/**
* \class InputMouseComponent
*
* \brief Component to control mouse input
*
* This component controls the buttons pressed and the movement of the mouse.
*
* \note 
*
* Created on: 2016-05-17
*/

#ifndef INPUTMOUSECOMPONENT_H
#define INPUTMOUSECOMPONENT_H

#include "InputComponent.h"
#include "InputManager.h"

class InputMouseComponent : public InputComponent, public OIS::MouseListener {
public:

    /// Constructor
    InputMouseComponent (std::shared_ptr<GameObject> gameObject, int player_number, std::shared_ptr<PhysicsComponent> physicsComponent);
    /// Destructor
    ~InputMouseComponent () {
        InputManager *inputMgr = InputManager::getSingletonPtr();
        inputMgr->removeMouseListener(this);
    };

    /**
     * This method checks if the mouse has been moved and moves the eyehole
     * accordingly */
    bool mouseMoved (const OIS::MouseEvent &e);
    /// This method checks if the mouse's buttons have been pressed
    bool mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
    /// This method checks if the mouse's buttons have been released
    bool mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

protected:
    /// Sensivity of the mouse's movements
    float _sensivity;
};

#endif
