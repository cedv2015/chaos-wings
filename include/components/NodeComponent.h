/**
* \class NodeComponent
*
* \brief Component that contains the GameObject's graphicals elements
*
* This component contains the SceneNode and its Entity and manages the Entity's
* material when the GameObject is damaged.
*
* Created on: 2016-05-17
*/

#ifndef NodeComponent_H
#define NodeComponent_H

#include "Component.h"
#include <Ogre.h>

class NodeComponent : public Component {
public:
    /// Constructor
    NodeComponent(std::shared_ptr<GameObject> gameObject, std::string mesh_name, int id, bool setCastShadows = true);
    /// Destructor
    virtual ~NodeComponent();
    /// This method returns the position of the SceneNode
    Ogre::Vector3 getPosition ();
    /// This method returns the orientation of the SceneNode
    Ogre::Quaternion getOrientation ();
    /// This method returns the Entity
    Ogre::Entity* getEntity () { return _entity; };
    /// This method returns the SceneNode
    Ogre::SceneNode* getNode () { return _node; };

    /// This method changes the Entity's material to a damaged one
    void changeToDamagedMaterial ();

    /**
     * This method changes the Entity's material to it's normal one when a
     * certain time has passed */
    void update (float delta) override;

    /// Maximum time with the damaged material
    static const float MAX_TIME_DAMAGED;
protected:
    Ogre::SceneNode *_node;
    Ogre::Entity *_entity;
    Ogre::SceneManager *_sceneMgr;
    bool _damaged;
    /// Time remaining of the damaged material
    float _time_damaged, _time;
};

#endif
