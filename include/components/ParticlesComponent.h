/**
* \class ParticlesComponent
*
* \brief Component that controls the particles of the game.
*
* This component manages the creation, updating and deleting of particles.
*
* Created on: 2016-05-17
*/

#ifndef ParticlesComponent_H
#define ParticlesComponent_H

#include "Component.h"
#include "MyParticleSystem.h"

class ParticlesComponent : public Component {
public:
    /// Constructor
    ParticlesComponent(std::shared_ptr<GameObject> gameObject, Ogre::Vector3 position, int type, Ogre::Node *node = nullptr, Ogre::Vector3 relative_position = Ogre::Vector3::ZERO, Ogre::Vector3 relative_velocity = Ogre::Vector3::ZERO);
    /// Destructor
    virtual ~ParticlesComponent();

    /**
     * This method changes the position and orientation of the particle 
     * depending of it's parent (if it has one) */
    void update (float delta);

    /// This method asigns a parent to the component
    void setParent (Ogre::Node *node) { _parent = node; };

    /// This method changes the type of particle
    void switchToParticle (int type);

    /// This method returns the position of the particle
    Ogre::Vector3 getPosition ();

    /// This method returns the orientation of the particle
    Ogre::Vector3 getDirection ();

    /// This method returns the minimum speed of the particles
    float getMinVelocity ();

    bool isFinished () override;
    /// Types of particles
    enum Type {
        VOID,
        SPEEDPHASE1,
        SPEEDPHASE2,
        WATERBULLET,
        SNOWBULLET,
        PLAYERPROPULSION,
        PLAYERPROPULSION2,
        WINGS_FRICTION,
        PLAYER_LASER,
        BULLET_COLLISION,
        MISSILE,
        ENEMY_PART,
        FINAL_BOSS_PROPELLER_NORMAL,
        FINAL_BOSS_PROPELLER_CHARGING,
        FINAL_BOSS_PROPELLER_SHOOTING,
        FINAL_BOSS_1_EXPLOSION,
        ENEMY_DEATH,
        FINAL_BOSS_2_RISING,
        FINAL_BOSS_2_SPEED,
        FINAL_BOSS_2_COLLISION_WATER1,
        FINAL_BOSS_2_COLLISION_WATER2,
        FINAL_BOSS_2_EXPLOSION
    };
protected:
    std::shared_ptr<MyParticleSystem> _particle;
    Ogre::Node *_parent;
    /// Relative position to it's parent
    Ogre::Vector3 _relative_position;
    /// Relative orientation to it's parent
    Ogre::Vector3 _relative_velocity;
};

#endif
