/**
* \class PhysicsComponent
*
* \brief Component to manage the physical part of an entity
*
* This component contains the rigidbody and shape of a GameObject. It also
* manages the colission between bodies (its behaviour after a collision).
*
* Created on: 2016-05-17
*/
#ifndef PhysicsComponent_H
#define PhysicsComponent_H

#include "Component.h"
#include <Ogre.h>
#include "PhysicsManager.h"
#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsCapsuleShape.h>
#include <Shapes/OgreBulletCollisionsConvexHullShape.h>
#include <Shapes/OgreBulletCollisionsTrimeshShape.h>
#include <Shapes/OgreBulletCollisionsSphereShape.h>
#include <Utils/OgreBulletCollisionsMeshToShapeConverter.h>
#include "StatusComponent.h"
#include "MyRigidBody.h"

class PhysicsComponent : public Component {
public:
    /// Constructor
    PhysicsComponent(std::shared_ptr<GameObject> gameObject, Ogre::SceneNode *node, Ogre::Entity *entity, std::string id, int type, int shape, bool manually_controlled, Ogre::Vector3 position = Ogre::Vector3::ZERO, Ogre::Vector3 size = Ogre::Vector3::ZERO, Ogre::Quaternion orientation = Ogre::Quaternion::IDENTITY);
    /// Destructor
    virtual ~PhysicsComponent() {
        if (_body) {
            delete _body;
        }
        if (_shape) {
            delete _shape;
        }
        _statusCmp = nullptr;
    };
    /// Changes the whole transformation of the physical body
    void setTransformation(btTransform transform);
    /// This method returns the position of the physical body
    Ogre::Vector3 getPosition () { if (_body) { return _body->getCenterOfMassPosition(); } return Ogre::Vector3::ZERO; }
    /// This method returns the orientation of the physical body
    Ogre::Quaternion getOrientation () { if (_body) { return _body->getWorldOrientation(); } return Ogre::Quaternion::ZERO; }
    /// This method returns the whole transformation applied to the body
    btTransform getTransformation();
    /// This method returns the type of PhysicsComponent for colission purposes
    int getType () { return _type; };

    /// This method applies a certain linear velocity to the physical body
    void setLinearVelocity (Ogre::Vector3 velocity) { if (_body) { _body->setLinearVelocity(velocity); } };
    /// This method applies a certain angular velocity to the physical body
    void setAngularVelocity (Ogre::Vector3 velocity) { if (_body) { _body->setAngularVelocity(velocity); } };

    /**
     * This method manages the behaviour of the PhysicsComponent when it
     * collides with another one */
    void onCollision (PhysicsComponent *cmp);

    /// This method sets the StatusComponent of it's owner GameObject
    void setStatusComponent (std::shared_ptr<StatusComponent> statusCmp) { _statusCmp = statusCmp; };

    /**
     * This method activates/deactivates the debug mode of the physical body
     * (See MyRigidBody for more information). */
    void showDebugShape(bool val) {
        _body->showDebugShape(val);
    }

    /// This method sets the gravity to the physical body
    void setGravity (Ogre::Vector3 gravity);

    /// Possible types of the physical body
    enum Type {
        PLAYER = 1,
        ENEMY = 2,
        PLAYER_BULLET = 4,
        ENEMY_BULLET = 8,
        PHASE = 16,
        ENEMY_PART = 32
    };

    /// Possible shapes of the physical body
    enum Shape {
        CONVEX,
        STATIC_CONVEX,
        CYLINDER,
        BOX,
        SPHERE,
        STATIC_PLANE,
        CAPSULE
    };

    /// This method allows bodies to be unaffected by collisions
    void setAffectedByCollisions(bool affected) { _affected_by_collisions = affected; };
    /// This method is used to see if a body is affected by collisions
    bool isAffectedByCollisions() { return _affected_by_collisions; };

    static const short player_collides_with = ENEMY | ENEMY_BULLET;
    static const short enemy_collides_with = PLAYER | PLAYER_BULLET | ENEMY_PART;
    static const short player_bullet_collides_with = ENEMY | PHASE;
    static const short enemy_bullet_collides_with = PLAYER | PHASE;
    static const short phase_collides_with = PLAYER_BULLET | ENEMY_BULLET | ENEMY_PART;
    static const short enemy_part_collides_with = ENEMY | PHASE;

protected:
    MyRigidBody *_body;
    OgreBulletCollisions::CollisionShape *_shape;
    int _type;
    std::shared_ptr<StatusComponent> _statusCmp;
    bool _affected_by_collisions;

};

#endif
