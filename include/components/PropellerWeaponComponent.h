/**
* \class PropellerWeaponComponent
*
* \brief Specialization of WeaponComponent
*
* This weapon is used by FinalBoss1.This weapon is more complex than the
* others, it has multiple states and manages the particles associanted to it.
*
* Created on: 2016-06-15
*/

#ifndef PROPELLERWEAPONCOMPONENT_H
#define PROPELLERWEAPONCOMPONENT_H

#include "WeaponComponent.h"
#include "ParticlesComponent.h"
#include "GameObjectCreator.h"

class PropellerWeaponComponent : public WeaponComponent{
public:
    /// Constructor
    PropellerWeaponComponent(std::shared_ptr<GameObject> gameObject, std::shared_ptr<RigidBodyAnimationComponent> animatorCmp);
    /// Destructor
    virtual ~PropellerWeaponComponent();

    /**
     * This method changes the state of the weapon, changes it's particle and
     * creates GameObjects that represent the file of the propeller which can
     * collide with the player */
    void update (float delta) override;

    /// This method allows to add new ParticleComponet to this weapon
    void addParticleComponent (std::shared_ptr<ParticlesComponent> particle) { _particles.push_back(particle); };

    /// This method removes all particles from the weapon
    void clearParticles () { _particles.clear();};

    /// This method changes all the variable to default values
    void resetCounters () {
        _state = State::CHARGING;
        _time = 0;
        _active_propellers.clear();
    }

    /// States of the weapon
    enum State {
        CHARGING,
        SHOOTING,
        FINISHED
    };

    /// Time in CHARGING state
    const static float CHARGING_TIME;
    /// Time in SHOOTING state
    const static float SHOOTING_TIME;
    /// Time between new bullets are created
    const static float TIME_BETWEEN_SHOTS;

protected:
    /// State of the weapon
    int _state;
    /// Time the weapon has been on the actual state
    float _time;
    /// Time from the last shoot
    float _time_last_shot;
    /// It contains the propellers that are shooting in a certain time
    std::vector<int> _active_propellers;
    std::vector<std::shared_ptr<ParticlesComponent>> _particles;
    GameObjectCreator *_gameObjectCrt;
};

#endif
