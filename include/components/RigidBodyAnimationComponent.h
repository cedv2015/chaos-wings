/**
* \class RigidBodyAnimationComponent
*
* \brief Component to control the animation of the physical body
*
* This component was made to make posible to move the object using only blender's
* animations. The reason for this is to make the enemy creation faster than making
* all the movements by coding its behaviour.
*
* \note There is not a real alternative inside Ogre3D. See entities/MyRigidBody.h
* for more information.
*
* Created on: 2016-05-22
*/

#ifndef RIGIDBODYANIMATIONCOMPONENT_H
#define RIGIDBODYANIMATIONCOMPONENT_H

#include "Component.h"
#include "PhysicsComponent.h"

class RigidBodyAnimationComponent : public Component {
public:
    /// Constructor
    RigidBodyAnimationComponent(std::shared_ptr<GameObject> gameObject, std::shared_ptr<PhysicsComponent> physicsComponent, Ogre::Entity *entity, bool can_attack = true, bool is_boss = false);
    /// Destructor
    virtual ~RigidBodyAnimationComponent();

    /**
     * This method changes the state of the animation when the previous one has
     * finished. */
    void update (float delta) override;

    /// This method returns the state of the animation
    int getState () { return _state; };

    /// This method returns the position of the PhysicsComponent
    Ogre::Vector3 getPosition () { return _physicsComponent->getPosition(); };
    /// This method returns the orientation of the PhysicsComponent
    Ogre::Quaternion getOrientation () { return _physicsComponent->getOrientation(); };

    /// This method adds a certain value to the initial position Vector3
    void addToInitialPosition(Ogre::Vector3 quantity);

    /// Posible states of the animation
    enum State {
        SPAWNING,
        ATTACKING,
        DISSAPEARING
    };

protected:
    Ogre::AnimationState *_animation;
    std::shared_ptr<PhysicsComponent> _physicsComponent;
    Ogre::Entity *_entity;
    bool _can_attack;
    /// Actual state of the animation
    int _state;
    /// Root used to get the position and orientation of the physical body
    Ogre::Bone *_root;
    Ogre::Vector3 _initialPosition;
    bool _active;
    bool _is_boss;
};

#endif
