/**
* \class SoundComponent
*
* \brief Component to control Game Object's sounds
*
* This component manages the sounds associated
* with a particular game object, depending on its kind,
* the sounds are different
*
* Created on: 2016-05-03
*/

#ifndef SoundComponent_H
#define SoundComponent_H

#include "Component.h"
#include "SoundFX.h"

class SoundComponent : public Component {
public:
    enum Type {
        MISSILE,
        LASER,
        BULLET,
        BOSS1,
        BOSS2,
        EXPLOSION,
        PLAYER_BULLET,
        PART,
        LASER_WALL,
        ENEMY,
        PLAYER
    };
    /// Constructor
    SoundComponent(std::shared_ptr<GameObject> gameObject, int type, int loop = 0);
    /// Destructor
    ~SoundComponent();

    void update(float deltaT) override;

    /**
     * This method checks if any sound effect is been reproduced
     * then it stops the sound, and reproduces another one. */
    void disappear();


protected:

    SoundFXPtr _appear, _disappear, _attack;
    /// Variable which indicates if it is playing a sound in loop
    bool _loop;
    /// Variable which indicates channel when a sound is played
    int _channel;
};

#endif
