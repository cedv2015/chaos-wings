/**
* \class StatusComponent
*
* \brief This component manages if a GameObject is alive or not.
*
* Component to manage the GameObject's status, including its life, the time its
* been alive it's type, and it's invulnerability time.
*
* Created on: 2016-05-22
*/

#ifndef STATUSCOMPONENT_H
#define STATUSCOMPONENT_H

#include "Component.h"
#include <Ogre.h>

class StatusComponent : public Component {
public:
    /// Constructor
    StatusComponent(std::shared_ptr<GameObject> gameObject, int life, int type);
    /// Destructor
    ~StatusComponent();

    /**
     * This method checks if the GameObject is a bullet or a missile and checks
     * if the life time has expired. It also creates the parts of the ships
     * when the enemies/players are destroyed. */
    void update (float delta) override;
    void reduceLife(int l);
    void addLife(int l);
    int getLife() { return _life; };
    int getMaxLife() { return _max_life; };
    bool isAlive() { return _life > 0; };
    bool isBullet() { return _type == Type::BULLET; };
    void setTimeInvulnerable (float time) { _time_invulnerable_remaining = time; };
    float getTimeInvulnerable () { return _time_invulnerable_remaining; };
    int getType() { return _type; };
    float getTimeAlive () { return _time_alive; };

    enum Type {
        PLAYER1,
        PLAYER2,
        ENEMY1,
        ENEMY2,
        ENEMY3,
        ENEMY4,
        ENEMYMISSILE,
        BULLET,
        FINALBOSS1,
        FINALBOSS2
    };

    static const int LIFE_TIME_BULLET = 8;
    static const int LIFE_TIME_MISSILE = 8;

protected:
    int _life, _type, _max_life;
    float _time_alive, _time_invulnerable_remaining;

    /**
     * This variable is used to check if a phase has ended when an enemy is
     * destroyed.*/
    static int _num_active_enemies;
    bool _score_added;
};

#endif
