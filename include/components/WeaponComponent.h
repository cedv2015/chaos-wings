/**
* \class WeaponComponent
*
* \brief This component creates enemy bullets.
*
* This component shoots to the player with a frequency specified with a minimum
* and a maximum time between shots. Depending of its type, the
* GameObjectCreator will create one or more bullets at a time.
*
* Created on: 2016-06-02
*/

#ifndef WEAPONCOMPONENT_H
#define WEAPONCOMPONENT_H

#include "Component.h"
#include "RigidBodyAnimationComponent.h"
#include "GameObjectCreator.h"

class WeaponComponent : public Component {
public:
    /// Constructor
    WeaponComponent(std::shared_ptr<GameObject> gameObject, std::shared_ptr<RigidBodyAnimationComponent> animatorCmp, float min_frequence, float max_frequence, int bullet_type, Ogre::Vector3 relative_position = Ogre::Vector3::ZERO) :
        Component(gameObject, Component::Type::WEAPON),
        _animatorCmp(animatorCmp),
        _min_freq(min_frequence),
        _max_freq(max_frequence),
        _bullet_type(bullet_type),
        _next_shoot(min_frequence),
        _gameObjectCreator(nullptr),
        _relative_position(relative_position)
        {
            _gameObjectCreator = GameObjectCreator::getSingletonPtr();
        };
    /// Destructor
    virtual ~WeaponComponent() {
        _animatorCmp = nullptr;
    };

    /**
     * This method checks if the time from the last shoot is bigger than a
     * certain amount, and creates new bullets if necessary. */
    void update (float delta) override;

    /// This method changes the variable to it's default value
    virtual void resetCounters () { _next_shoot = _min_freq; };

    /// Possible bullet types
    enum BulletType {
        NORMAL,
        SHOTGUN,
        MISSILE,
        WALL
    };

protected:
    std::shared_ptr<RigidBodyAnimationComponent> _animatorCmp;

    /// Minimum frequence between shoots
    int _min_freq;
    /// Maximum frequence between shoots
    int _max_freq;
    int _bullet_type;
    float _next_shoot;
    GameObjectCreator *_gameObjectCreator;
    Ogre::Vector3 _relative_position;
};

#endif
