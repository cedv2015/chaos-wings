/**
* \class FinalBoss1
*
* \brief First final boss of the game.
*
* This is a specialization of the GameObject class. It has many components with
* some special cases (a weapon contains a ParticleComponent), that's why it's
* been created, but it could have been created as a normal GameObject in
* GameObjectCreator.
*
* Created on: 2016-06-13
*/

#ifndef FINALBOSS1_H
#define FINALBOSS1_H

#include "GameObject.h"
#include "RigidBodyAnimationComponent.h"
#include "WeaponComponent.h"
#include "SoundComponent.h"

class FinalBoss1 : public GameObject {
public:
    /// Constructor
    FinalBoss1();
    /// Destructor
    virtual ~FinalBoss1();

    /// This method initializes all the components of the boss.
    void initComponents ();

    /// This method change to the next state when the previous one has ended.
    void update (float delta) override;

    /// This method removes all the components from the boss.
    void removeComponents () override;

    /// This method overrides the default. It changes the state of the boss to dead.
    void finish () override;

    /// This method removes the boss.
    void eliminate ();

    /// Possible states of the boss
    enum State {
        WEAPON1,
        WEAPON2,
        WEAPON3
    };

    /// Maximum time in Weapon1 state.
    const float TIME_WEAPON_1 = 7;
    /// Maximum time in Weapon2 state.
    const float TIME_WEAPON_2 = 10;
    /// Maximum time in Weapon3 state.
    const float TIME_WEAPON_3 = 7;

protected:
    std::vector<std::shared_ptr<WeaponComponent>> _weapons1;
    std::vector<std::shared_ptr<WeaponComponent>> _weapons2;
    std::vector<std::shared_ptr<WeaponComponent>> _weapons3;

    int _state;
    float _time_in_state;
    float _time_limit;
    std::shared_ptr<RigidBodyAnimationComponent> _animationCmp;
    std::shared_ptr<SoundComponent> _soundCmp;
    static int _id;
    bool _dead;
};

#endif
