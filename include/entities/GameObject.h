/**
* \class GameObject
*
* \brief Base class of the game.
*
* The GameObject is the main class of the game. It's basically a container of
* components. Every element of the game will be a GameObject with its own
* components. Without components, a GameObject is a useless entity.
*
* \note
*
* Created on: 2016-05-17
*/

#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <vector>
#include <memory>
#include "Component.h"
#include <iostream>

class GameObject : public std::enable_shared_from_this<GameObject> {
public:
    /// Constructor
    GameObject() : _is_finished(false) {};
    /// Destructor
    virtual ~GameObject() {
        _components.clear();
    };

    /**
     * This method updates all the components that belong to this GameObject
     * and if one of its component is finished, it's removed from the vector.*/
    virtual void update (float delta);

    /**
     * Returns if the GameObject has finished its activity depending on its
     * variable _is_finished. */
    bool isFinished () { return _is_finished; };

    /// Finishes the activity of the GameObject, setting _is_finished to true.*/
    virtual void finish() { _is_finished = true; };

    /// Adds a component to the GameObject's vector
    void addComponent (std::shared_ptr<Component> component);

    /// Removes all the components of the GameObject
    virtual void removeComponents () { _components.clear(); };

    /// It returns a certain Component
    std::shared_ptr<Component> getComponent (int type);

protected:
    std::vector<std::shared_ptr<Component>> _components;
    bool _is_finished;
};

#endif
