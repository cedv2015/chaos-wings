/**
* \class MyParticleSystem
*
* \brief Own implementation of ParticleSystem
*
* This entity is responsible of managing a certain particle including its position
* and its state (finished or not).
*
* \note 
*
* Created on: 2016-05-17
*/

#ifndef MYPARTICLESYSTEM_H
#define MYPARTICLESYSTEM_H

#include <Ogre.h>
#include <OgreParticleSystem.h>

class MyParticleSystem {
public:
    /// Constructor
    MyParticleSystem(Ogre::Vector3 position, std::string name);
    /// Destructor
    ~MyParticleSystem();

    /// Updates the particle and finishes it if necessary
    void update (float delta);

    /// Changes the position of the particle
    void setPosition (Ogre::Vector3 position);

    void finish ();
    
    bool isFinished () { return _is_finished; };

    /// Changes the direction of spawn of the particle
    void setDirection (Ogre::Vector3 direction);

    float getMinVelocity ();
private:
    Ogre::ParticleSystem *_particle;
    /**
     * Empty SceneNode that will be used to change the final position of the
     * particle.*/
    Ogre::SceneNode *_parent;
    bool _is_finished;
    /**
     * Time until the particle is finished. After that time has passed, the
     * particle is removed from the Scene. */
    float _time_to_finish;
    static int _id;
};

#endif
