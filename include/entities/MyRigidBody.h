/**
* \class MyRigidBody
*
* \brief Implementation of RigidBody
*
* It seems that Ogre3D doesn't allow to move bones that are not asociated
* with vertices unless the body is in debug mode. In our implementation of
* the animation of rigid bodies, bones without vertices are necesary. So 
* debug is activated when these animations are used and then, the debug
* SceneNodes are hidden.
*
* Created on: 2016-06-05
*/

#ifndef MYRIGIDBODY_H
#define MYRIGIDBODY_H

#include <Ogre.h>
#include <OgreBulletDynamicsRigidBody.h>
#include <OgreBulletDynamicsWorld.h>

class MyRigidBody : public OgreBulletDynamics::RigidBody {
public:
    MyRigidBody(const Ogre::String &name,
                OgreBulletDynamics::DynamicsWorld *world,
                const short collisionGroup = 0,
                const short collisionMask = 0);
    virtual ~MyRigidBody();

    /// Activates the debug mode of the RigidBody to enable empty bones.
    void showDebugShape (bool val);

};

#endif
