/**
* \class Phase
*
* brief Phase's GameObject
*
* GameObject that contains the components relative of the levels of the game.
*
* Created on: 2016-05-10
*/

#ifndef FASE1_H
#define FASE1_H

#include "GameObject.h"
class Phase : public GameObject {
public:
    /// Constructor
    Phase(int id) : GameObject(), _id(id){};
    /// Destructor
    ~Phase() {};

    /// This method initializes all the components of the boss.
    void initComponents ();
private:
    int _id;
};

#endif
