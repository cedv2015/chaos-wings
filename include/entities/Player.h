/**
* \class Player
*
* \brief Player of the game
*
* This is a specialization of the GameObject class. It has some special cases
* (like the InputComponent used to control it and its own CEGUI menu), that's
* why it's been created, but it could have been created as a normal GameObject
* in GameObjectCreator.
*
* \note This class could be removed in the future and be implemented totally
* in GameObjectCreator.
*
* Created on: 2016-05-18
*/
#ifndef PLAYER_H
#define PLAYER_H

#include "GameObject.h"

class Player : public GameObject {
public:
    /// Constructor
    Player(int player_number) : GameObject(), _player_number(player_number) {};
    /// Destructor
    virtual ~Player();

    /// This method initializes all the components of the boss.
    void initComponents ();

    /// This method removes all the components from the boss.
    void removeComponents () override;

    /// This method changes the visibility of the Eyehole's node
    void visibilityEyehole(bool visibility);
    /// This method changes the visibility of the Player's HUD
    void visibilityHUD(bool visibility);

protected:
    int _player_number, _id_player;
    static unsigned int _number_of_players_created;
};

#endif
