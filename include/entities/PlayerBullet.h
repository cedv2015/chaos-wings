/**
* \class PlayerBullet
*
* \brief Player bullets.
*
* This is a specialization of the GameObject class. Its creation needs some
* calculations to determine its position and direction, that's why this class
* was created, but it could have been created as a normal GameObject in
* GameObjectCreator.
*
* \note This class could be removed in the future and be implemented totally
* in GameObjectCreator.
*
* Created on: 2016-05-18
*/

#ifndef PLAYERBULLET_H
#define PLAYERBULLET_H

#include "GameObject.h"
#include <Ogre.h>

class PlayerBullet : public GameObject {
public:
    /// Constructor
    PlayerBullet() : GameObject() {};
    /// Destructor
    ~PlayerBullet() {};

    /// This method initializes all the components of the boss.
    void initComponents(Ogre::Vector3 position, Ogre::Vector3 direction);

private:
    static int _id;
    const float PLAYER_BULLET_VELOCITY = 70;
};

#endif
