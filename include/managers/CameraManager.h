/**
* \class CameraManager
*
* \brief This class manages the camera movement
*
* This manager moves the camera depending the position and the number of players
* in the game. With one player, it will only move in X and Y axis, if there are
* more than one, it will move the camera in the Z axis to make sure all players
* are visible on the screen.
*
* \note It has a new state with trailer creation purpouses. In this mode, the
* camera moves around the player to create a movie-like effect.
*
* Created on: 2016-05-19
*/

#ifndef CAMERAMANAGER_H
#define CAMERAMANAGER_H

#include <Ogre.h>
#include <map>

class CameraManager : public Ogre::Singleton<CameraManager> {
public:
    /// Constructor
    CameraManager();
    /// Destructor
    ~CameraManager();

    static CameraManager& getSingleton ();
    static CameraManager* getSingletonPtr ();

    /**
     * This method updates the position of the camera depending on the number
     * of players and their position on the scene. */
    void update (float delta);

    /// This method adds a new player to the vector of players this class has
    void addPlayer (Ogre::SceneNode* player, int player_number);

    /// This method removes all the players from the vector
    void clearPlayers () { _players.clear(); };

    /// This method removes a certain player from the vector
    void removePlayer (int player_number);

    /// This method returns a pointer to a random player
    Ogre::SceneNode* getRandomPlayer ();

    Ogre::Camera* getCamera() { return _camera; };

    /**
     * This method checks if the player indicated still exists in the vector
     * and hence, is still alive. */
    bool isPlayerAlive (Ogre::SceneNode *node);

    int getNumberOfPlayers () { return _number_of_players; };

    /// Changes the state to "trailer" or "normal" state.
    void switchState ();

    static const float MINIMUM_DISTANCE;
    static const float Z_MULTIPLIER;

protected:
    /// This map contains all the players from the scene
    std::map<int, Ogre::SceneNode*> _players;
    Ogre::Camera *_camera;
    int _number_of_players;
    int _type_of_camera;
    float _time;
};

#endif
