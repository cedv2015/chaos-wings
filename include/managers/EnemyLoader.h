/**
* \class EnemyLoader
*
* \brief Class to load the .phs files.
*
* This manager reads the file specified with a number and creates the enemies
* at the time specified, of the type specified and in a certain position. It
* makes the phase creation very easy (just write this values in a specified
* format).
*
* Created on: 2016-06-03
*/

#ifndef ENEMYLOADER_H
#define ENEMYLOADER_H

#include <Ogre.h>
#include "GameObjectCreator.h"

class EnemyLoader : public Ogre::Singleton<EnemyLoader> {
public:
    /// Constructor
    EnemyLoader();
    /// Destructor
    ~EnemyLoader();

    static EnemyLoader* getSingletonPtr ();
    static EnemyLoader& getSingleton ();

    /**
     * This method reads from the file of the phase indicated by the parameter
     * 'number' and fills the vector with the information to create the enemies
     * of that certain phase. */
    void loadPhase (int number);

    /**
     * This method checks if the time passed is bigger than the spawn time of
     * the next enemy and creates it. */
    void update (float delta);

    /// This method returns the percentaje of the time until the boss
    int getPercentajeUntilBoss();

    /// This method checks if there are enemies left to spawn.
    void checkEnemiesState();

    int getPhaseNumber () { return _phase_number; };

    const static char DELIM = ':';
private:
    GameObjectCreator* _gameObjCreator;
    /**
     * This vector contains the information of the spawn of the enemies. The
     * structure of the information is the following:
     * [float time, int type, float x, float y, float z]*/
    std::vector<std::string> _data;
    float _time;
    float _next_enemy;
    int _phase_number;
};

#endif
