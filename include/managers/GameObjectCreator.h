/**
* \class GameObjectCreator
*
* \brief This manager creates the GameObjects of the game.
*
* This manager will create every posible GameObject of the game just by calling
* function from this class.
*
* \note
*
* Created on: 2016-05-18
*/

#ifndef GAMEOBJECTCREATOR_H
#define GAMEOBJECTCREATOR_H

#include "GameObjectManager.h"
#include "CameraManager.h"
#include "Player.h"
#include "Phase.h"

class GameObjectCreator : public Ogre::Singleton<GameObjectCreator> {
public:
    /// Constructor
    GameObjectCreator();
    /// Destructor
    ~GameObjectCreator();

    static GameObjectCreator &getSingleton ();
    static GameObjectCreator *getSingletonPtr ();

    /// Creates a player with mouse as its input
    std::shared_ptr<Player> createPlayerMouse();

    /// Creates a player with joystick as its input
    std::shared_ptr<Player> createPlayerJoystick();

    /// Creates a Phase GameObject
    std::shared_ptr<Phase> createPhase(int id);

    /// Creates a player bullet
    void createPlayerBullet(Ogre::Vector3 position, Ogre::Vector3 direction);

    /// Creates a normal enemy bullet
    void createNormalEnemyBullet(Ogre::Vector3 position, Ogre::Vector3 direction, bool sound);

    /// Creates the special wall attack of FinalBoss2
    void createWallEnemyBullet(Ogre::Vector3 position, Ogre::Vector3 direction);

    /// Creates the enemy specified by type
    void createEnemy(int type, Ogre::Vector3 position);

    void createEnemy1(Ogre::Vector3 position);
    void createEnemy2(Ogre::Vector3 position);
    void createEnemy3(Ogre::Vector3 position);
    void createEnemy4(Ogre::Vector3 position);

    /// Creates an enemy missile
    void createMissile(Ogre::Vector3 position);

    /// Creates the parts of an enemy when that enemy is destroyed
    void createPartsEnemy(Ogre::Vector3 position, int type);

    /// Creates the parts of a player when that player is destroyed
    void createPartsPlayer(Ogre::Vector3 position, int player_number);

    /// Creates an enemy bullet
    void createEnemyBullet (int type, Ogre::Vector3 position, Ogre::Vector3 direction = Ogre::Vector3::ZERO);

    /// Creates a FinalBoss1
    void createFinalBoss1();

    /// Creates a FinalBoss2
    void createFinalBoss2();

    /// Creates the special bullets of the propeller of FinalBoss1
    void createPropellerBullet(Ogre::Vector3 position, Ogre::Vector3 direction);

    /// Restarts all the counters of this manager
    void resetCounters () { _enemy_bullets = _enemy_1 = _enemy_2 = _enemy_3 = _enemy_4 = 0; _actual_phase = nullptr; };

    std::shared_ptr<Phase> getActualPhase() { return _actual_phase; };
    bool getPlayerMouse(){return _player_mouse;};
    void setPlayerMouse(bool state){_player_mouse = state;};
    bool getPlayerJoystick(){return _player_joystick;};
    void setPlayerJoystick(bool state){_player_joystick = state;};

    static const float MAX_WALL_BULLETS;
    static const float ENEMY_BULLET_VELOCITY;
    static const int NUMBER_ENEMY_PARTS = 3;
    static const int NUMBER_PLAYER_PARTS = 3;

protected:
    GameObjectManager *_gameObjectMgr;
    CameraManager *_cameraMgr;
    bool _player_mouse, _player_joystick;
    unsigned int _enemy_bullets;
    unsigned int _enemy_1, _enemy_2, _enemy_3, _enemy_4, _enemy_missile, _part, _propeller_bullet;
    Ogre::SceneManager *_sceneMgr;
    std::shared_ptr<Phase> _actual_phase;
};

#endif
