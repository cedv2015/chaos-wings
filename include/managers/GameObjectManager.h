/**
* \class GameObjectManager
*
* \brief Class to manage GameObjects
*
* This manager updates every GameObject of the game every frame and removes those
* ones which have finished its tasks.
*
* Created on: 2016-05-01
*/
#ifndef GAMEOBJECTMANAGER_H
#define GAMEOBJECTMANAGER_H

#include <Ogre.h>
#include "GameObject.h"

class GameObjectManager : public Ogre::Singleton<GameObjectManager> {

public:
    /// Constructor
    GameObjectManager();
    /// Destructor
    ~GameObjectManager();

    static GameObjectManager &getSingleton ();
    static GameObjectManager *getSingletonPtr ();

    /// This method updates all the GameObjects of the game
    void update (float delta);

    /// Adds a GameObject to the vector
    void addGameObject (std::shared_ptr<GameObject> gameObject);

    /// Clears all the GameObjects of the game
    void removeAllObjects ();

    /// Removes the GameObject that contains the Phase
    void removeActualPhase();

    void hideHUD ();

    void showHUD ();

    int getNumGameObject() { return _gameObjects.size(); };


private:
    std::vector<std::shared_ptr<GameObject>> _gameObjects;

};

#endif
