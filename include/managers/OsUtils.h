#ifndef OSUTILS_H
#define OSUTILS_H

class OsUtils {
public:
    enum OS {
        LINUX,
        WINDOWS,
        MAC
    };

    static int getOs () {
#ifdef __unix 
        return OS::LINUX;
#endif
#ifdef __unix__ 
        return OS::LINUX;
#endif
#ifdef __linux__ 
        return OS::LINUX;
#endif
#ifdef __APPLE__ 
        return OS::MAC;
#endif
#ifdef __MACH__
        return OS::MAC;
#endif
#ifdef _WIN32 
        return OS::WINDOWS;
#endif
#ifdef _WIN64
        return OS::WINDOWS;
#endif
        return -1;
    }
};

#endif


