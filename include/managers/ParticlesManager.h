/**
* \class ParticlesManager
*
* \brief This class manages all particles of the game
*
* This manager creates, updates and removes the particles of the game. This
* manager won't move the particles, it will just update them and remove them
* from the game, its own ParticleComponent has to do this task.
*
*
* \note 
*
* Created on: 2016-05-22
*/

#ifndef PARTICLESMANAGER_H
#define PARTICLESMANAGER_H

#include "MyParticleSystem.h"
#include <memory>

class ParticlesManager : public Ogre::Singleton<ParticlesManager> {
public:
    /// Constructor
    ParticlesManager();
    /// Destructor
    ~ParticlesManager();

    /// Updates the particles and removes them when necessary
    void update (float delta);

    /// Creates a new MyParticleSystem
    std::shared_ptr<MyParticleSystem> createParticleSystem (Ogre::Vector3 position, int type);

    /// Removes all particles from the game
    void destroyAllParticles ();

    int getNumberOfParticles() { return _particles.size(); };

    static ParticlesManager& getSingleton ();
    static ParticlesManager* getSingletonPtr ();

protected:
    std::vector<std::shared_ptr<MyParticleSystem>> _particles;
    int _id_counter;
};

#endif
