#ifndef PHYSICSMANAGER_H
#define PHYSICSMANAGER_H

#include <Ogre.h>

#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>

class PhysicsManager : public Ogre::Singleton<PhysicsManager>  {
    public:
        /// Constructor
        PhysicsManager();
        /// Destructor
        ~PhysicsManager();

        static PhysicsManager& getSingleton();
        static PhysicsManager* getSingletonPtr();

        /// Updates the physics world.
        void update(float delta);
        OgreBulletDynamics::DynamicsWorld * getWorld() { return _world; };

        void switchDebug ();

    private:
        Ogre::Root *_root;
        Ogre::SceneManager *_sceneMgr;
        OgreBulletDynamics::DynamicsWorld *_world;
        OgreBulletCollisions::DebugDrawer *_debugDrawer;
        /**
         * Checks all the collisions in the game and notificates the
         * corresponding PhysicsComponent.*/
        void checkCollisions();
        bool _debug;
};

#endif
