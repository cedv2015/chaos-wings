/**
* \class RankingManager
*
* \brief Class that manages the score of the game.
*
* This manager makes all the score operations necessary for the game, including
* saving, loading and giving its actual value the class that requests it.
*
* Created on: 2015-12-12
*/
#ifndef RankingManager_H
#define RankingManager_H

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <Ogre.h>


class RankingManager : public Ogre::Singleton<RankingManager> {
    public:
        RankingManager (): _score(0) {};
        ~RankingManager () {};

        static RankingManager& getSingleton ();
        static RankingManager* getSingletonPtr ();

        std::vector<std::string> getRanking ();
        bool checkRanking ();
        void setRanking (std::string);
        void addScore(int type);
        void resetScore(){ _score = 0;};
        int getScore() { return _score; };

        static const int SCORE_ENEMY1 = 10;
        static const int SCORE_ENEMY2 = 15;
        static const int SCORE_ENEMY3 = 20;
        static const int SCORE_ENEMY4 = 25;
        static const int SCORE_ENEMY_MISSILE = 5;
        static const int SCORE_FINAL_BOSS1 = 50;
        static const int SCORE_FINAL_BOSS2 = 50;
    private:
        std::vector<std::string> _rankings;
        int _score;
        void saveRankings ();
        void loadRankings ();

};

#endif
