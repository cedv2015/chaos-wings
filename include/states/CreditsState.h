/**
* \class CreditsState
*
* \brief This state shows game's credits
*
* Credits State displays a list of all people who have
* participated in the game. It is divided into categories
* indicating their roll.
*
* Created on: 2016-06-14
*/
#ifndef CreditsState_H
#define CreditsState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/Renderer.h>
#include "GameState.h"
#include "MenuState.h"

class CreditsState : public Ogre::Singleton<CreditsState>, public GameState {
    public:
        /// Constructor
        CreditsState ();
        /// Destructor
        ~CreditsState();

        static CreditsState &getSingleton ();
        static CreditsState *getSingletonPtr ();

        void enter();
        void exit();
        void pause();
        void resume();
        bool menu(const CEGUI::EventArgs &e);

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);
        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        CEGUI::MouseButton convertMouseButton(const OIS::MouseButtonID id);

        virtual bool povMoved( const OIS::JoyStickEvent &e, int pov );
        virtual bool axisMoved( const OIS::JoyStickEvent &e, int axis );
        virtual bool sliderMoved( const OIS::JoyStickEvent &e, int sliderID );
        virtual bool buttonPressed( const OIS::JoyStickEvent &e, int button );
        virtual bool buttonReleased ( const OIS::JoyStickEvent &e, int button);

        bool frameStarted (const Ogre::FrameEvent &e);
        bool frameEnded (const Ogre::FrameEvent &e);

        /// This method processes a text file at the same time, it stores the information obtained in vector _data
        void loadCredits();

        /// This method initializes animations that will be used in the CreditsState
        void setupAnimations();

    private:
        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;

        /// CEGUI Windows are the visual part
        CEGUI::Window* _credits;
        /// Instance that allows to do animations
        CEGUI::AnimationInstance *_creditsAnimInst;

        OIS::Keyboard *_keyboard;
        OIS::Mouse *_mouse;
        bool _exit;
        /// Vector that stores the information read from a file
        std::vector<std::string> _data;
};

#endif
