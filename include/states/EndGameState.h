/**
* \class EndGameState
*
* \brief State of end Game
*
* The game enters in this state when the player has passed all phases.
*
* Created on: 2016-06-16
*/
#ifndef EndGameState_H
#define EndGameState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/Renderer.h>
#include "PhysicsManager.h"
#include "GameState.h"
#include "PlayState.h"
#include "MenuState.h"
#include "RankingManager.h"

class EndGameState : public Ogre::Singleton<EndGameState>, public GameState {
    public:
        /// Constructor
        EndGameState();
        /// Destructor
        ~EndGameState();
        static EndGameState& getSingleton ();
        static EndGameState* getSingletonPtr ();
        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);
        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        CEGUI::MouseButton convertMouseButton(const OIS::MouseButtonID id);

        virtual bool povMoved( const OIS::JoyStickEvent &e, int pov );
        virtual bool axisMoved( const OIS::JoyStickEvent &e, int axis );
        virtual bool sliderMoved( const OIS::JoyStickEvent &e, int sliderID );
        virtual bool buttonPressed( const OIS::JoyStickEvent &e, int button );
        virtual bool buttonReleased ( const OIS::JoyStickEvent &e, int button);

        bool frameStarted (const Ogre::FrameEvent &e);
        bool frameEnded (const Ogre::FrameEvent &e);

        bool close(const CEGUI::EventArgs &e);
        bool accept(const CEGUI::EventArgs &e);

        /// This method initializes animations that will be used in this state
        void setupAnimations();

        /// This method updates the text "Game Over" or "You Win" in order to show the message keyed
        void updateText(float delta);

        /// This method checks if score is better than the saved scores
        void addRanking();

        /// Show a game resume
        void endGame();

    private:
        Ogre::Root *_root;
        Ogre::SceneManager *_sceneMgr;
        Ogre::Camera *_camera;
        OIS::Keyboard *_keyboard;
        OIS::Mouse *_mouse;
        bool _exit, _eGame;
        PhysicsManager *_physicsMgr;
        RankingManager *_rankingMgr;
        //// CEGUI Windows are the visual part
        CEGUI::Window* _endGame;
        /// Instance that allows to do animations
        CEGUI::AnimationInstance *_backgroundAnimInst;
        float d_timeSinceStart;
};

#endif
