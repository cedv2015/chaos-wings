/**
* \class GameOverState
*
* \brief The game is over when players are dead
*
* The game enters this state when all players are dead.
* Then it checks if the score is better than the saved scores.
* After that, the option of add new score or Game Over shows up.
*
* Created on: 2016-06-14
*/
#ifndef GameOverState_H
#define GameOverState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/Renderer.h>
#include "PhysicsManager.h"
#include "GameState.h"
#include "PlayState.h"
#include "MenuState.h"
#include "RankingManager.h"

class GameOverState : public Ogre::Singleton<GameOverState>, public GameState {
    public:
        /// Constructor
        GameOverState();

        /// Destructor
        ~GameOverState();

        static GameOverState& getSingleton ();
        static GameOverState* getSingletonPtr ();
        void enter ();
        void exit ();
        void pause ();
        void resume ();

        /// The method checks if score is better than the saved scores
        void addRanking();

        /// This method displays the game over
        void gameOver();

        /// This method initializes animations that will be used in the DialogueState
        void setupAnimations();

        /// This method updates the text dialogue in order to show the message keyed
        void updateText(float delta);

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);
        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        CEGUI::MouseButton convertMouseButton(const OIS::MouseButtonID id);

        virtual bool povMoved( const OIS::JoyStickEvent &e, int pov );
        virtual bool axisMoved( const OIS::JoyStickEvent &e, int axis );
        virtual bool sliderMoved( const OIS::JoyStickEvent &e, int sliderID );
        virtual bool buttonPressed( const OIS::JoyStickEvent &e, int button );
        virtual bool buttonReleased ( const OIS::JoyStickEvent &e, int button);

        bool frameStarted (const Ogre::FrameEvent &e);
        bool frameEnded (const Ogre::FrameEvent &e);
        bool hide(const CEGUI::EventArgs &e);
        bool accept(const CEGUI::EventArgs &e);
        bool close(const CEGUI::EventArgs &e);

    private:
        Ogre::Root *_root;
        Ogre::SceneManager *_sceneMgr;
        Ogre::Camera *_camera;
        OIS::Keyboard *_keyboard;
        OIS::Mouse *_mouse;

        bool _exit, _gOver;
        /// It is required to update the physical components
        PhysicsManager *_physicsMgr;
        /// It is required to check the best scores
        RankingManager *_rankingMgr;
        /// CEGUI Windows are the visual part
        CEGUI::Window* _ranking, *_exitButton;
        /// Instance that allows to do animations
        CEGUI::AnimationInstance* _backgroundAnimInst;
        float d_timeSinceStart;
};

#endif
