/**
* \class InstructionsState
*
* \brief Different panels with game instructions
*
* There is an option in the menu to access this state.
* Moreover, there are several panels that display information about the game.
* It emphasizes the use of CEGUI.
*
* Created on: 2016-05-24
*/
#ifndef InstructionsState_H
#define InstructionsState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/Renderer.h>
#include "GameState.h"

class InstructionsState : public Ogre::Singleton<InstructionsState>, public GameState {
    public:
        /// Constructor
        InstructionsState ();
        static InstructionsState& getSingleton ();
        static InstructionsState* getSingletonPtr ();
        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);
        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        bool povMoved( const OIS::JoyStickEvent &e, int pov );
        bool axisMoved( const OIS::JoyStickEvent &e, int axis );
        bool sliderMoved( const OIS::JoyStickEvent &e, int sliderID );
        bool buttonPressed( const OIS::JoyStickEvent &e, int button );
        bool buttonReleased ( const OIS::JoyStickEvent &e, int button);

        CEGUI::MouseButton convertMouseButton(const OIS::MouseButtonID id);
        CEGUI::MouseButton convertJoystickButton(int id);
        bool handleMouseEntersLeftArrowArea(const CEGUI::EventArgs&);
        bool handleMouseLeavesLeftArrowArea(const CEGUI::EventArgs&);
        bool handleMouseEntersRightArrowArea(const CEGUI::EventArgs&);
        bool handleMouseLeavesRightArrowArea(const CEGUI::EventArgs&);
        bool handleCheckIfNaviIconAnimationNeedsChange(const CEGUI::EventArgs&);
        bool handleLeftArrowClick(const CEGUI::EventArgs&);
        bool handleRightArrowClick(const CEGUI::EventArgs&);
        bool handleNaviLabelClick(const CEGUI::EventArgs&);
        bool handleBackMenuClick(const CEGUI::EventArgs&);

        bool frameStarted (const Ogre::FrameEvent &e);
        bool frameEnded (const Ogre::FrameEvent &e);

        bool hide(const CEGUI::EventArgs &e);

        /// This method loads intructions layout
        void instructionsShow();
        /// This method initializes some CEGUI windows and subscribes to some events
        void setupWindows();
        /// This method initializes navigation CEGUI windows and subscribes to some events
        void setupNaviIconAnimationEventHandlers();
        /// This method initializes the state animations
        void resetAnimations();
        /// This method processes a text file, and stores the information obtained in vector _data
        void loadInstructions();

    private:
  	    Ogre::Root* _root;
  	    Ogre::SceneManager* _sceneMgr;
  	    Ogre::Viewport* _viewport;
  	    Ogre::Camera* _camera;

  	    OIS::Keyboard *_keyboard;
        OIS::Mouse *_mouse;
        bool _exit;
        bool d_mouseIsHoveringNavi;

        /// CEGUI Windows are the visual part

        CEGUI::Window* _instructions;
        CEGUI::Window* d_topBarLabel;
        CEGUI::Window* d_botBarLabel;
        CEGUI::Window* d_botNaviLeftArrowArea;
        CEGUI::Window* d_botNaviRightArrowArea;
        CEGUI::Window* d_botNaviContainer;
        CEGUI::Window* d_botNaviCenter;
        CEGUI::Window* d_navigationTravelIcon;
        CEGUI::Window* d_navigationSelectionIcon;
        CEGUI::Window* d_startButtonClickArea;
        CEGUI::Window* d_naviLabel;

        /// Instance that allows to do animations

        CEGUI::AnimationInstance* d_botBarLabelBlendOutInst;
        CEGUI::AnimationInstance* d_topBarAnimInst;
        CEGUI::AnimationInstance* d_botBarAnimInst;
        CEGUI::AnimationInstance* d_naviButtonLeftMoveInInst;
        CEGUI::AnimationInstance* d_naviButtonRightMoveInInst;
        CEGUI::AnimationInstance* d_naviButtonLeftMoveOutInst;
        CEGUI::AnimationInstance* d_naviButtonRightMoveOutInst;
        CEGUI::AnimationInstance* d_naviBotMoveInInst;
        CEGUI::AnimationInstance* d_naviBotMoveOutInst;
        CEGUI::AnimationInstance* d_startButtonBlendInAnimInst;
        CEGUI::AnimationInstance* d_navigationLabelBlendInAnimInst;
        CEGUI::AnimationInstance* d_navigationLabelPartialBlendOutInst;
        CEGUI::AnimationInstance* d_naviPartialBlendOutInst;
        CEGUI::AnimationInstance* d_naviBlendInInst;
        CEGUI::AnimationInstance* d_centerButtonsPartialBlendOutInst;
        CEGUI::AnimationInstance* d_centerButtonsBlendInInst;
        CEGUI::AnimationInstance* d_buttonBack;

        CEGUI::UVector2 _posR, _posL, _posRA, _posLA, _posLabel;

        const float RANGE_JOYSTICK = 32767;
        enum Constant {
            BUTTON_A = 2,
            BUTTON_B = 1
        };

        /// Vector that stores the information read from a file
        std::vector<std::string> _list_instructions, _data;
        /// it indicates what instruction should be showed in layout
        int inst;
        /// Maximum number of characters per line
        const int LIMIT_CHARACTERS = 40;
};

#endif
