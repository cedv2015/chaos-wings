#ifndef MainState_H
#define MainState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/Renderer.h>
#include "GameState.h"

class MainState : public Ogre::Singleton<MainState>, public GameState {
    public:
        MainState();
        static MainState& getSingleton ();
        static MainState* getSingletonPtr ();
        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);
        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        bool povMoved( const OIS::JoyStickEvent &e, int pov );
        bool axisMoved( const OIS::JoyStickEvent &e, int axis );
        bool sliderMoved( const OIS::JoyStickEvent &e, int sliderID );
        bool buttonPressed( const OIS::JoyStickEvent &e, int button );
        bool buttonReleased ( const OIS::JoyStickEvent &e, int button);

        bool frameStarted (const Ogre::FrameEvent &e);
        bool frameEnded (const Ogre::FrameEvent &e);

    private:
        Ogre::Root *_root;
        Ogre::SceneManager *_sceneMgr;
        Ogre::Camera *_camera;
        Ogre::Viewport *_viewport;
        OIS::Keyboard *_keyboard;
        OIS::Mouse *_mouse;
        bool _exit;
        std::vector<Ogre::String> _meshes = {"Player1.mesh", "Player2.mesh", "Player1Part1.mesh", "Player1Part2.mesh", "Player1Part3.mesh", "Player1Part4.mesh", "Player2Part1.mesh", "Player2Part2.mesh", "Player2Part3.mesh", "Player2Part4.mesh", "Enemy1.mesh", "Enemy2.mesh", "Enemy3.mesh", "Enemy4.mesh", "Enemy1Part1.mesh", "Enemy1Part2.mesh", "Enemy1Part3.mesh", "Enemy2Part1.mesh", "Enemy2Part2.mesh", "Enemy2Part3.mesh", "Enemy3Part1.mesh", "Enemy3Part2.mesh", "Enemy3Part3.mesh", "Enemy4Part1.mesh", "Enemy4Part2.mesh", "Enemy4Part3.mesh", "FinalBoss1.mesh", "FinalBoss2.mesh", "Enemy_bullet.mesh", "Missile.mesh", "PlayerBullet.mesh", "Scenario1.mesh"};

        void initMeshes ();
};

#endif
