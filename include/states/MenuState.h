/***************************************************************************
*   Copyright (C) 2004 - 2012 Paul D Turner & The CEGUI Development Team
*
*   Permission is hereby granted, free of charge, to any person obtaining
*   a copy of this software and associated documentation files (the
*   "Software"), to deal in the Software without restriction, including
*   without limitation the rights to use, copy, modify, merge, publish,
*   distribute, sublicense, and/or sell copies of the Software, and to
*   permit persons to whom the Software is furnished to do so, subject to
*   the following conditions:
*
*   The above copyright notice and this permission notice shall be
*   included in all copies or substantial portions of the Software.
*
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
*   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
*   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
*   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
*   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
*   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
*   OTHER DEALINGS IN THE SOFTWARE.
***************************************************************************/

/**
* \class MenuState
*
* \brief This state shows game menu
*
* Description MenuState displays the options that allow access to the game,
* instructions, credits or ranking
*
* Created on: 2016-05-09
*/

#ifndef MenuState_H
#define MenuState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/Renderer.h>
#include "GameState.h"
#include "TrackManager.h"

class MenuState : public Ogre::Singleton<MenuState>, public GameState {
    public:
        enum WriteFocus
        {
          WF_TopBar,
          WF_BotBar,

          WF_Count
        };
        /// Constructor
        MenuState ();
        static MenuState& getSingleton ();
        static MenuState* getSingletonPtr ();
        void enter ();
        void exit ();
        void pause ();
        void resume ();

        bool handlealphabutton(const CEGUI::EventArgs&);
        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);
        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        bool povMoved( const OIS::JoyStickEvent &e, int pov );
        bool axisMoved( const OIS::JoyStickEvent &e, int axis );
        bool sliderMoved( const OIS::JoyStickEvent &e, int sliderID );
        bool buttonPressed( const OIS::JoyStickEvent &e, int button );
        bool buttonReleased ( const OIS::JoyStickEvent &e, int button);

        CEGUI::MouseButton convertMouseButton(const OIS::MouseButtonID id);
        CEGUI::MouseButton convertJoystickButton(int id);

        bool frameStarted (const Ogre::FrameEvent &e);
        bool frameEnded (const Ogre::FrameEvent &e);

        /// This method loads menu layout
        void menu();

        /// This method set selection icons invisible
        void makeAllSelectionIconsInvisible();

        /// This method initializes some CEGUI windows and subscribes to some events
        void setupWindows();

        /// This method initializes some animations that will be used in the this state
        void setupAnimations();

        /// This method subscribes navigation icons to some events
        void setupNaviIconAnimationEventHandlers();

        /// This method subscribes suboptions icons to some events
        void setupInnerButtonsSubOptionsLabels();

        /// This method subscribes navigation arrows to some events
        void setupNaviArrowWindows();

        /// This method subscribes option icons to some events
        void setupButtonClickHandlers();

        /// This method reset popup lines animations
        void stopStartPopupLinesAnimations();

        /// This method initializes popup lines animations
        void setupPopupLinesAnimations();

        /// This method initializes icons animations
        void setupSelectionIconAnimations();

        /// It activates navigation bar elements
        void enableNavigationBarElements();

        /// This method activates interactive planet elements
        void enableInteractivePlanetElements();

        /// This method updates the introduction text in order to show the message keyed
        void updateIntroText();

        /// This method updates the welcome text in order to show the message keyed
        void updateLoginWelcomeText();

        /// This method deactivates interactive planet elements
        void disableInteractivePlanetElements();

        /// It deactivates navigation bar elements
        void disableNavigationBarElements();

        /// It initializes some variables
        void entering();

        /// This method initializes the state animations
        void resetAnimations();

        /// It actives entrance animations
        void startEntranceAnimations();

        bool handleStartPopupLinesInstructionsDisplay(const CEGUI::EventArgs&);
        bool handleMouseEntersLeftArrowArea(const CEGUI::EventArgs&);
        bool handleMouseLeavesLeftArrowArea(const CEGUI::EventArgs&);
        bool handleMouseEntersRightArrowArea(const CEGUI::EventArgs&);
        bool handleMouseLeavesRightArrowArea(const CEGUI::EventArgs&);
        bool handleStartPopupLinesRankingDisplay(const CEGUI::EventArgs&);
        bool handleStartPopupLinesCharactersDisplay(const CEGUI::EventArgs&);
        bool handleStartPopupLinesQuitDisplay(const CEGUI::EventArgs&);
        bool handleInnerButtonsLabelEntered(const CEGUI::EventArgs&);
        bool handleInnerButtonsLabelLeft(const CEGUI::EventArgs&);
        bool handleLoginAcceptButtonClicked(const CEGUI::EventArgs&);
        bool handleInnerPartStartClickAreaClick(const CEGUI::EventArgs&);
        bool handleNaviSelectionIconAnimStart(const CEGUI::EventArgs&);
        bool handleCheckIfNaviIconAnimationNeedsChange(const CEGUI::EventArgs&);
        bool close(const CEGUI::EventArgs&);
        bool instructions(const CEGUI::EventArgs&);
        bool credits(const CEGUI::EventArgs&);
        bool ranking(const CEGUI::EventArgs&);
        bool handleLeftArrowClick(const CEGUI::EventArgs&);
        bool handleRightArrowClick(const CEGUI::EventArgs&);
        bool handleNaviLabelClick(const CEGUI::EventArgs&);

        CEGUI::Window* getIconWindowFromLabel(CEGUI::Window* window);

        float getAngle(const CEGUI::Quaternion& quat);
        void update(float timeSinceLastUpdate);


    private:
        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;


        OIS::Keyboard *_keyboard;
        OIS::Mouse *_mouse;
        bool _exit;
        bool d_interactiveElementsWereInitiallyBlendedOut;
        bool d_interactivePlanetElementsAreEnabled;
        bool d_navigationIsEnabled;
        bool d_loginWasAccepted;
        bool d_mouseIsHoveringNavi;
        bool d_startButtonClicked;

        float d_timeSinceStart;
        float d_timeSinceLoginAccepted;

        WriteFocus d_currentWriteFocus;

        CEGUI::String d_userName;

        /// CEGUI Windows are the visual part

        CEGUI::Window *_layout;
        CEGUI::Window* d_topBarLabel;
        CEGUI::Window* d_botBarLabel;
        CEGUI::Window* d_loginContainer;
        CEGUI::Window* d_botNaviLeftArrowArea;
        CEGUI::Window* d_botNaviRightArrowArea;
        CEGUI::Window* d_botNaviCenter;
        CEGUI::Window* d_loginAcceptButton;
        CEGUI::Window* d_startButtonClickArea;
        CEGUI::Window* d_navigationTravelIcon;
        CEGUI::Window* d_navigationSelectionIcon;
        CEGUI::Window* d_botNaviContainer;
        CEGUI::Window* d_naviLabel;

        /// Instance that allows to do animations

        CEGUI::AnimationInstance* d_topBarAnimInst;
        CEGUI::AnimationInstance* d_botBarAnimInst;
        CEGUI::AnimationInstance* d_insideBlendInAnimInst;
        CEGUI::AnimationInstance* d_insideImage3RotateInInst;
        CEGUI::AnimationInstance* d_insideImage4RotateInInst;
        CEGUI::AnimationInstance* d_insideImageRingsContainerSizeInInst;

        CEGUI::AnimationInstance* d_buttonFadeInAnimInst1;
        CEGUI::AnimationInstance* d_buttonFadeInAnimInst2;
        CEGUI::AnimationInstance* d_buttonFadeInAnimInst3;
        CEGUI::AnimationInstance* d_buttonFadeInAnimInst4;
        CEGUI::AnimationInstance* d_buttonFadeInAnimInst5;

        CEGUI::AnimationInstance* d_loginContainerMoveInInst;
        CEGUI::AnimationInstance* d_loginContainerMoveOutInst;
        CEGUI::AnimationInstance* d_naviButtonLeftMoveInInst;
        CEGUI::AnimationInstance* d_naviButtonRightMoveInInst;
        CEGUI::AnimationInstance* d_naviBotMoveInInst;
        CEGUI::AnimationInstance* d_startButtonBlendInAnimInst;
        CEGUI::AnimationInstance* d_navigationLabelBlendInAnimInst;
        CEGUI::AnimationInstance* d_navigationLabelPartialBlendOutInst;

        CEGUI::AnimationInstance* d_naviPartialBlendOutInst;
        CEGUI::AnimationInstance* d_naviBlendInInst;
        CEGUI::AnimationInstance* d_centerButtonsPartialBlendOutInst;
        CEGUI::AnimationInstance* d_centerButtonsBlendInInst;

        CEGUI::AnimationInstance* d_botBarLabelBlendOutInst;


        CEGUI::AnimationInstance* d_popupLinesInstructionsAnimInst;
        CEGUI::AnimationInstance* d_popupLinesRankingAnimInst;
        CEGUI::AnimationInstance* d_popupLinesCharactersAnimInst;
        CEGUI::AnimationInstance* d_popupLinesOptionsAnimInst;
        CEGUI::AnimationInstance* d_popupLinesQuitAnimInst;


        /// It's start time of first message
        const float s_firstStartDelay = 5.0f;

        /// It's start time of seconf message
        const float s_secondStartDelay = 11.0f;

        /// This constant is range joystick
        const float RANGE_JOYSTICK = 32767;

        /// They are gamepad button
        enum Constant {
            BUTTON_A = 2,
            BUTTON_B = 1
        };

        std::vector<std::string> _list_phases;
        int i;
        TrackManager *_trackMgr;
};

#endif
