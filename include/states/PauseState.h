/**
* \class PauseState
*
* \brief This state stops the game
*
* Pause State stops game, and returns to the
* main menu, return to the game, or exit
*
* Created on: 2016-05-24
*/
#ifndef PauseState_H
#define PauseState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/Renderer.h>
#include "GameState.h"
#include "MenuState.h"
#include "PlayState.h"

class PauseState : public Ogre::Singleton<PauseState>, public GameState {
    public:
        /// Constructor
        PauseState ();

        static PauseState &getSingleton ();
        static PauseState *getSingletonPtr ();

        void enter();
        void exit();
        void pause();
        void resume();
        bool quit(const CEGUI::EventArgs &e);
        bool menu(const CEGUI::EventArgs &e);
        bool play(const CEGUI::EventArgs &e);

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);
        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        CEGUI::MouseButton convertMouseButton(const OIS::MouseButtonID id);

        virtual bool povMoved( const OIS::JoyStickEvent &e, int pov );
        virtual bool axisMoved( const OIS::JoyStickEvent &e, int axis );
        virtual bool sliderMoved( const OIS::JoyStickEvent &e, int sliderID );
        virtual bool buttonPressed( const OIS::JoyStickEvent &e, int button );
        virtual bool buttonReleased ( const OIS::JoyStickEvent &e, int button);

        bool frameStarted (const Ogre::FrameEvent &e);
        bool frameEnded (const Ogre::FrameEvent &e);

    private:
        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;

        CEGUI::Window* _pause;
        OIS::Keyboard *_keyboard;
        OIS::Mouse *_mouse;
        bool _exit;

};

#endif
