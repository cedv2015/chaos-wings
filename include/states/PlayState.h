/**
* \class PlayState
*
* \brief Main game state where game occurs
*
* This state calls the necessary managers to create players, phases
* and enemies, to load dialogues, to control the game.
*
* Created on: 2016-05-09
*/

#ifndef PLAYSTATE_H
#define PLAYSTATE_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/Renderer.h>
#include "GameState.h"
#include "GameObjectManager.h"
#include "GameObjectCreator.h"
#include "PhysicsManager.h"
#include "CameraManager.h"
#include "RankingManager.h"
#include "ParticlesManager.h"
#include "DialogueManager.h"
#include "EnemyLoader.h"
#include "Player.h"
#include "PauseState.h"
#include "GameOverState.h"
#include "Phase.h"
#include "TrackManager.h"


class PlayState : public Ogre::Singleton<PlayState>, public GameState {
public:
    PlayState();
    ~PlayState();
    static PlayState& getSingleton ();
    static PlayState* getSingletonPtr ();

    virtual void enter ();
    virtual void exit ();
    virtual void pause ();
    virtual void resume ();

    virtual void keyPressed (const OIS::KeyEvent &e);
    virtual void keyReleased (const OIS::KeyEvent &e);
    virtual void mouseMoved (const OIS::MouseEvent &e);
    virtual void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
    virtual void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

    virtual bool povMoved( const OIS::JoyStickEvent &e, int pov );
    virtual bool axisMoved( const OIS::JoyStickEvent &e, int axis );
    virtual bool sliderMoved( const OIS::JoyStickEvent &e, int sliderID );
    virtual bool buttonPressed( const OIS::JoyStickEvent &e, int button );
    virtual bool buttonReleased ( const OIS::JoyStickEvent &e, int button);

    virtual bool frameStarted (const Ogre::FrameEvent &e);
    virtual bool frameEnded (const Ogre::FrameEvent &e);

    bool getControllMouse(){return _controllMouse;};
    void setControllMouse(bool c){ _controllMouse=c;};

    /// It adds a new stage
    void addStage();

    /// This method initializes some CEGUI windows
    void initCEGUI();

    /// This methos displays some game informations, like fps, number of GameObject or number of particles
    void info(float fps);

    /// This method initializes some animations that will be used in this state
    void setupAnimations();

    /// It changes panel information visibility
    void changeVisibilityInfo();

    void setMenu(bool menu) { _menu=menu; };

    /// Performs some checks, like the state players or the state dialogues
    void checkGameState();

    /// It reset some variables
    void finishPhase();

    /// This method check when a message is finished if there are anymore
    void finishDialogue();

    /// This method changes HUD phase visibility
    void visibilityHUDPhase(bool visibility);

    /// This method changes HUD visibility
    void visibilityHUD(bool visibility) { _hud->setVisible(visibility); visibilityHUDPhase(visibility); };

private:
    /* data */
    Ogre::Root *_root;
    Ogre::SceneManager *_sceneMgr;
    Ogre::Camera *_camera;
    Ogre::Viewport *_viewport;
    Ogre::SceneNode *_node;
    Ogre::Entity *_entity;
    bool _exit, _controllMouse, _menu, _finishPhase, _finishDialogue, _newStage;

    /// It manages the created gameobject
    GameObjectManager *_gameObjectMgr;
    /// It is necessary to create some objects such as players or phases
    GameObjectCreator *_gameObjectCreator;
    /// It is required to update the physical components
    PhysicsManager *_physicsMgr;
    /// It is required th change the camera mode
    CameraManager* _cameraMgr;
    /// This manager is required yo updated the particles
    ParticlesManager *_particlesMgr;
    /// It is required to updates the best scores
    RankingManager *_rankingMgr;
    DialogueManager *_dialogueMgr;
    /// CEGUI Windows are the visual part
    CEGUI::Window* _hud, *_fps, *_gameObjectLabel, *_scoreLabel, *_label1, *_score;
    /// Instance that allows to do animations
    CEGUI::AnimationInstance* d_totBarAnimInst, *_backgroundAnimInst;
    /// It's required to load the enemies of a phase
    EnemyLoader *_enemyLoader;

    float _time_fps;
    float _sum_fps;
    float _time_delay;
    float _time_fade;
    float _time_new_stage;
    float _time_dialogue;

    /// Time after completion of a phase to initialize a dialogue
    const float TIME_DELAY = 5.0f;
    /// Time after dialogues to change phase
    const float TIME_FADE_DELAY = 1.0f;
    /// Time to display dialogue when new phase starts
    const float TIME_NEW_STAGE_DELAY = 1.0f;

    int _count_fps, _phase;

    int _percentaje_until_boss, _numDialogue;

    /// They are gamepad button
    enum Constant {  START_BUTTON = 7 };
    /// Pointers to actual stage
    std::shared_ptr<Phase> _phaseStage;
    TrackManager *_trackMgr;
};

#endif
