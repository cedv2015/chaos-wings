/**
* \class RankingState
*
* \brief RankingState shows three best scores stored in the game
*
* This class loads the graphic resources in order to display
* the best scores stored.
*
* Created on: 2016-05-24
*/
#ifndef RankingState_H
#define RankingState_H

#include <Ogre.h>
#include <OIS/OIS.h>
#include <CEGUI.h>
#include <RendererModules/Ogre/Renderer.h>
#include "GameState.h"
#include "RankingManager.h"

class RankingState : public Ogre::Singleton<RankingState>, public GameState {
    public:
        RankingState ();
        static RankingState& getSingleton ();
        static RankingState* getSingletonPtr ();
        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);
        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        bool povMoved( const OIS::JoyStickEvent &e, int pov );
        bool axisMoved( const OIS::JoyStickEvent &e, int axis );
        bool sliderMoved( const OIS::JoyStickEvent &e, int sliderID );
        bool buttonPressed( const OIS::JoyStickEvent &e, int button );
        bool buttonReleased ( const OIS::JoyStickEvent &e, int button);

        CEGUI::MouseButton convertMouseButton(const OIS::MouseButtonID id);
        CEGUI::MouseButton convertJoystickButton(int id);

        bool frameStarted (const Ogre::FrameEvent &e);
        bool frameEnded (const Ogre::FrameEvent &e);

        bool hide(const CEGUI::EventArgs &e);

        bool handleBackMenuClick(const CEGUI::EventArgs&);

        /// This method loads ranking layout
        void rankingShow();
        /// This method initializes some CEGUI windows and subscribes to some events
        void setupWindows();
        /// This method initializes the animations' state
        void resetAnimations();

    private:
      	Ogre::Root* _root;
      	Ogre::SceneManager* _sceneMgr;
      	Ogre::Viewport* _viewport;
      	Ogre::Camera* _camera;

        OIS::Keyboard *_keyboard;
        OIS::Mouse *_mouse;
        bool _exit;

        /// CEGUI Windows are the visual part
        CEGUI::Window* _records;
        CEGUI::Window* d_topBarLabel;
        CEGUI::Window* d_botBarLabel;

        /// It is required to check the best scores
      	RankingManager *_rankingManager;

        const float RANGE_JOYSTICK = 32767;
        enum Constant {
            BUTTON_A = 2,
            BUTTON_B = 1
        };

        /// Instance that allows to do animations
        CEGUI::AnimationInstance* d_topBarAnimInst;
        CEGUI::AnimationInstance* d_botBarAnimInst;
        CEGUI::AnimationInstance* d_buttonBack;
        CEGUI::AnimationInstance* d_fistPlace;
        CEGUI::AnimationInstance* d_secondPlace;
        CEGUI::AnimationInstance* d_thirdPlace;
        CEGUI::AnimationInstance* d_size;
};

#endif
