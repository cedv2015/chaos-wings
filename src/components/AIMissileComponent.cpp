#include "AIMissileComponent.h"

AIMissileComponent::AIMissileComponent (std::shared_ptr<GameObject> gameObject, std::shared_ptr<PhysicsComponent> physicsCmp) : AIComponent(gameObject) ,_objective(nullptr),  _physicsCmp(physicsCmp), _cameraMgr(nullptr) {
    _cameraMgr = CameraManager::getSingletonPtr();
    _objective = _cameraMgr->getRandomPlayer();
    _velocity = Ogre::Vector3(0, 0, 0.5);
}

AIMissileComponent::~AIMissileComponent () {
    _objective = nullptr;
    _physicsCmp = nullptr;
    _cameraMgr = nullptr;
}

void AIMissileComponent::update (float delta) {
    if (!_cameraMgr->isPlayerAlive(_objective)) {
        _objective = _cameraMgr->getRandomPlayer();
    }
    Ogre::Vector3 objective_position;
    if (_objective == nullptr) {
        objective_position = Ogre::Vector3::ZERO;
    }
    else {
        objective_position = _objective->getPosition();
    }
    Ogre::Vector3 own_position = _physicsCmp->getPosition();
    if (own_position.z < objective_position.z) {
        Ogre::Vector2 distance;
        distance.x = objective_position.x - own_position.x;
        distance.y = objective_position.y - own_position.y;
        float total_distance = distance.squaredLength();
        total_distance = total_distance > 3 ? 3 : total_distance;
        distance.normalise();
        distance *= total_distance * ACCELERATION;
        _velocity.x += distance.x;
        _velocity.y += distance.y;
        float velocity_length = _velocity.length();
        if (velocity_length > MAX_VELOCITY) {
            _velocity /= velocity_length;
            _velocity *= MAX_VELOCITY;
        }
    }
    _velocity.z = 5;
    own_position += _velocity * delta;
    btTransform transform = _physicsCmp->getTransformation();
    transform.setOrigin(btVector3(own_position.x, own_position.y, own_position.z));
    btQuaternion rotation(-_velocity.y * 0.05, _velocity.x * 0.05, 0, 1);
    transform.setRotation(rotation);
    _physicsCmp->setTransformation(transform);
}

const float AIMissileComponent::ACCELERATION = 0.5;
const float AIMissileComponent::MAX_VELOCITY = 6;
