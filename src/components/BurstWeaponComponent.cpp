#include "BurstWeaponComponent.h"

void BurstWeaponComponent::update (float delta) {
    if (_animatorCmp->getState() == RigidBodyAnimationComponent::State::ATTACKING) {
        _next_shoot -= delta;
        if (_next_shoot <= 0) {
            if (_shots_remaining == _number_of_shots) {
                _angle_burst = (rand() % 360);
            }
            _next_shoot = _time_between_shots;
            Ogre::Vector3 direction = _animatorCmp->getPosition();
            direction.z += 20;
            float distance_to_center = BURST_WIDTH - ((float)_shots_remaining / (float)_number_of_shots * BURST_WIDTH * 2);
            float x = Ogre::Math::Cos(Ogre::Degree(_angle_burst)) * distance_to_center;
            float y = Ogre::Math::Sin(Ogre::Degree(_angle_burst)) * distance_to_center;
            direction.x += x;
            direction.y += y;
            _gameObjectCreator->createEnemyBullet(_bullet_type, _animatorCmp->getPosition() + _animatorCmp->getOrientation() * _relative_position, direction);
            if (_shots_remaining == 0) {
                _shots_remaining = _number_of_shots;
                _next_shoot = (rand() % (_max_freq - _min_freq)) + _min_freq;
            }
            else {
                --_shots_remaining;
            }
        }
    }
}

const float BurstWeaponComponent::BURST_WIDTH = 10.0;
