#include "CEGUIComponent.h"
#include "GameObject.h"
#include "StatusComponent.h"
#include "EnemyLoader.h"
#include "InputComponent.h"

CEGUIComponent::CEGUIComponent(std::shared_ptr<GameObject> gameObject, int id, int type) : Component(gameObject, Component::Type::CEGUI), _id(id), _type(type), _finish(false), _invulnerabilityWindow(nullptr){

    switch(_type) {
        case Type::PLAYER:
              _hud = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("Player"+std::to_string(id)+".layout");
              CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_hud);
              _windowScore = _hud->getChild("Player"+std::to_string(id)+"Container/Player"+std::to_string(id)+"ScoreLabel");
              _windowHUD = _hud->getChild("Player"+std::to_string(id)+"Container");
              _lifeWindow = _hud->getChild("Player"+std::to_string(id)+"Container/Life"+std::to_string(id));
              _invulnerabilityWindow = _hud->getChild("Player"+std::to_string(id)+"Container/Invulnerability");
              _size = _lifeWindow->getSize();
              _sizeInvulnerability = _invulnerabilityWindow->getSize();
              _position = _lifeWindow->getPosition();
              _positionInvulnerability = _invulnerabilityWindow->getPosition();
            break;
        case Type::FINALBOSS1:
              _hud = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("finalBoss.layout");
              CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_hud);
              _windowScore = _hud->getChild("FinalBossContainer/FinalBossScoreLabel");
              _windowHUD = _hud->getChild("FinalBossContainer");
              _lifeWindow = _hud->getChild("FinalBossContainer/Life");
              _size = _lifeWindow->getSize();
              _position = _lifeWindow->getPosition();
            break;
        case Type::PHASE:
              _hud = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("Phase"+std::to_string(id)+".layout");
              CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_hud);
              _windowScore = _hud->getChild("Phase"+std::to_string(id)+"Container/Phase"+std::to_string(id)+"ScoreLabel");
              _windowHUD = _hud->getChild("Phase"+std::to_string(id)+"Container/Way");
              _lifeWindow = _hud->getChild("Phase"+std::to_string(id)+"Container/Caza");
              _size = _windowHUD->getSize();
              _position = _lifeWindow->getPosition();
              CEGUI::AnimationManager& animMgr = CEGUI::AnimationManager::getSingleton();

              CEGUI::Animation* lifeAnim = animMgr.getAnimation("RevBackgroundAlpha");
              _lifeAnimInstance = animMgr.instantiateAnimation(lifeAnim);
              _lifeAnimInstance->setTargetWindow(_hud->getChild("Phase"+std::to_string(id)+"Container"));
            break;
    }

}
void CEGUIComponent::update(float delta) {
    CEGUI::System::getSingleton().injectTimePulse(delta);
    if(_type!=Type::PHASE){
        std::shared_ptr<StatusComponent> status = std::static_pointer_cast<StatusComponent>(_gameObject->getComponent(Component::Type::STATUS));
        int max_life = status->getMaxLife();
        int life = status->getLife();
        CEGUI::USize size = _lifeWindow->getSize();
        CEGUI::UVector2 position = _lifeWindow->getPosition();
        float original_size = size.d_width.d_scale;
        float diff;
        if(size.d_width.d_scale!=(_size.d_width.d_scale/max_life)*life){
            size.d_width.d_scale =(_size.d_width.d_scale/max_life)*life;
            _lifeWindow->setSize(size);
            diff = std::abs(original_size-size.d_width.d_scale);
            switch(_type){
                case Type::PLAYER:
                    switch (_id) {
                      case 1:
                          position.d_x.d_scale-= diff/2;
                      break;
                      case 2:
                          position.d_x.d_scale+= diff/2;
                      break;
                    }
                    break;
                case Type::FINALBOSS1:
                    position.d_x.d_scale-= diff/2;
                    break;
            }
            CEGUI::AnimationManager& animMgr = CEGUI::AnimationManager::getSingleton();

            CEGUI::Animation* lifeAnim = animMgr.getAnimation("LifeBar");
            _lifeAnimInstance = animMgr.instantiateAnimation(lifeAnim);
            _lifeAnimInstance->setTargetWindow(_lifeWindow);
            _lifeWindow->setPosition(position);
        }
        if(_type == Type::PLAYER){
            int time_max_invulnerability = (int) std::static_pointer_cast<InputComponent>(_gameObject->getComponent(Component::Type::INPUT))->getTimeInvulnerabilityCooldown();
            float time  = std::static_pointer_cast<InputComponent>(_gameObject->getComponent(Component::Type::INPUT))->getTimeToNextInvulnerability();
            if((int)time == 0)
                _windowScore->setText("GO!");
            else
                _windowScore->setText(std::to_string((int)time));
            CEGUI::USize sizeInv = _invulnerabilityWindow->getSize();
            sizeInv.d_height.d_scale =(_sizeInvulnerability.d_height.d_scale/time_max_invulnerability)*(time_max_invulnerability-(int)time);
            _invulnerabilityWindow->setSize(sizeInv);
        }

    }else{
      int percentaje = EnemyLoader::getSingletonPtr()->getPercentajeUntilBoss();
      if(percentaje == -1){
          if(!_finish){
              _lifeAnimInstance->start();
              _finish = true;
          }
      }else{
        CEGUI::UVector2 position = _lifeWindow->getPosition();
        position.d_x.d_scale = _position.d_x.d_scale + (_size.d_width.d_scale/100)*percentaje;
        _lifeWindow->setPosition(position);
      }

    }

}
