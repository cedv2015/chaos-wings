#include "InputComponent.h"
#include "PhysicsComponent.h"
#include "PhysicsManager.h"
#include <cmath>

void InputComponent::update (float delta) {
    _time_to_next_invulnerability - delta < 0 ? _time_to_next_invulnerability = 0 : _time_to_next_invulnerability -= delta;
    if (!_eyehole_node) {
        return;
    }
    Ogre::Vector3 eyehole_position = _eyehole_node->getPosition();
    Ogre::Vector3 player_position = _physicsComponent->getPosition();
    btTransform player_transformation = _physicsComponent->getTransformation();
    btQuaternion rotation(0, 0, 0, 1);
    float distance_x = eyehole_position.x - player_position.x;
    float distance_y = eyehole_position.y - player_position.y;
    // The distance is near to zero
    if (distance_x > - MINIMUM_DISTANCE && distance_x < MINIMUM_DISTANCE && distance_y > - MINIMUM_DISTANCE && distance_y < MINIMUM_DISTANCE) {
        player_transformation.setRotation(rotation);
        _physicsComponent->setTransformation(player_transformation);
    }
    else {
        float total_distance = std::sqrt(std::pow(distance_x, 2) + std::pow(distance_y, 2));
        float speed = total_distance > MAXIMUM_SPEED ? MAXIMUM_SPEED : total_distance;
        player_position.x += distance_x * speed * delta / total_distance;
        player_position.y += distance_y * speed * delta / total_distance;
        player_transformation.setOrigin(btVector3(player_position.x, player_position.y, player_position.z));
        rotation += btQuaternion(distance_y * 0.05, - distance_x * 0.05, - distance_x * 0.05, 1);
        checkRotation(rotation);
        player_transformation.setRotation(rotation);
    }
    _time_since_last_shot += delta;
    if (_button1_pressed) {
        if (_time_since_last_shot >= TIME_BETWEEN_SHOTS) {
            Ogre::Vector3 direction;
            Ogre::Vector3 camera_position = _camera->getPosition();
            btVector3 btFrom(camera_position.x, camera_position.y, camera_position.z);
            Ogre::Vector3 vec3To = ((eyehole_position - camera_position) * 40) + camera_position;
            btVector3 btTo(vec3To.x, vec3To.y, vec3To.z);
            btCollisionWorld::ClosestRayResultCallback res(btFrom, btTo);
            res.m_collisionFilterGroup = PhysicsComponent::Type::PLAYER_BULLET;
            res.m_collisionFilterMask = PhysicsComponent::player_bullet_collides_with;
            PhysicsManager::getSingletonPtr()->getWorld()->getBulletDynamicsWorld()->rayTest(btFrom, btTo, res);
            if (res.hasHit()) {
                direction = Ogre::Vector3(res.m_hitPointWorld.getX(), res.m_hitPointWorld.getY(), res.m_hitPointWorld.getZ());
            }
            else {
                direction = _eyehole_node->getPosition();
                direction.z -= 5;
            }
            _gameObjectCreator->createPlayerBullet(player_position, direction);
            _time_since_last_shot = 0;
        }
    }
    if (_statusCmp) {
        float time_invulnerable = _statusCmp->getTimeInvulnerable();
        if (time_invulnerable > 0) {
            time_invulnerable /= TIME_INVULNERABLE;
            time_invulnerable = 1 - time_invulnerable;
            rotation *= btQuaternion(0, 0, time_invulnerable * 3.14 * 2);
            player_transformation.setRotation(rotation);
            _physicsComponent->setAffectedByCollisions(false);
        }
        else {
            _physicsComponent->setAffectedByCollisions(true);
        }
    }
    _physicsComponent->setTransformation(player_transformation);
}

void InputComponent::checkRotation(btQuaternion& rotation) {
    if (rotation.getX() > ROTATIONLIMIT) {
        rotation.setX(ROTATIONLIMIT);
    }
    else if (rotation.getX() < - ROTATIONLIMIT) {
        rotation.setX(- ROTATIONLIMIT);
    }
    if (rotation.getY() > ROTATIONLIMIT) {
        rotation.setY(ROTATIONLIMIT);
    }
    else if (rotation.getY() < - ROTATIONLIMIT) {
        rotation.setY(- ROTATIONLIMIT);
    }
    if (rotation.getZ() > ROTATIONLIMIT) {
        rotation.setZ(ROTATIONLIMIT);
    }
    else if (rotation.getZ() < - ROTATIONLIMIT) {
        rotation.setZ(- ROTATIONLIMIT);
    }
}

void InputComponent::visibilityEyehole(bool visibility){
    _eyehole_node->setVisible(visibility);
}

void InputComponent::secondButtonReleased () {
    if (_statusCmp) {
        if (_time_to_next_invulnerability <= 0) {
            _statusCmp->setTimeInvulnerable(TIME_INVULNERABLE);
            _time_to_next_invulnerability = TIME_INVULNERABILITY_COOLDOWN;
        }
    }
}

const float InputComponent::HORIZONTAL_LIMIT = 10;
const float InputComponent::VERTICAL_LIMIT = 6;
const float InputComponent::MINIMUM_DISTANCE = 0.01;
const float InputComponent::MAXIMUM_SPEED = 10;
const float InputComponent::ACCELERATION = 2;
const float InputComponent::TIME_BETWEEN_SHOTS = 0.2;
const float InputComponent::RANGE_JOYSTICK = 32767;
const float InputComponent::ROTATIONLIMIT = 1.0f;
const float InputComponent::TIME_INVULNERABILITY_COOLDOWN = 10.0;
const float InputComponent::TIME_INVULNERABLE = 2.0;
