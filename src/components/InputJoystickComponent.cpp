#include "InputJoystickComponent.h"

InputJoystickComponent::InputJoystickComponent (std::shared_ptr<GameObject> gameObject, int player_number, std::shared_ptr<PhysicsComponent> physicsComponent) : InputComponent(gameObject, player_number, physicsComponent), _sensivity(3280), _dead_zone(5000) {
    InputManager *inputMgr = InputManager::getSingletonPtr();
    inputMgr->addJoystickListener(this, "InputJoystickComponent");
};

bool InputJoystickComponent::povMoved(const OIS::JoyStickEvent &e, int pov) {
    return true;
}

bool InputJoystickComponent::axisMoved(const OIS::JoyStickEvent &e, int axis) {
    if (_eyehole_node) {
        switch(axis){
            case 0:
                _x_position = e.state.mAxes[axis].abs;
                //_eyehole_node->translate(e.state.mAxes[axis].abs * _sensivity,  0, 0);
                break;
            case 1:
                _y_position = e.state.mAxes[axis].abs;
                //_eyehole_node->translate(0, -e.state.mAxes[axis].abs * _sensivity, 0);
                break;
            default:
                return true;
        }
    }
    return true;
}
bool InputJoystickComponent::sliderMoved(const OIS::JoyStickEvent &e, int sliderID) {

    return true;
}
bool InputJoystickComponent::buttonPressed(const OIS::JoyStickEvent &e, int button) {
    switch (button) {
        case JOYSTICK_BUTTON_1:
            _button1_pressed = true;
            break;
        case JOYSTICK_BUTTON_2:
            _button2_pressed = true;
            break;
        default:
            break;
    }
    return true;

}

bool InputJoystickComponent::buttonReleased(const OIS::JoyStickEvent &e, int button) {
    switch (button) {
      case JOYSTICK_BUTTON_1:
          _button1_pressed = false;
          break;
      case JOYSTICK_BUTTON_2:
          InputComponent::secondButtonReleased();
          _button2_pressed = false;
          break;
      default:
          break;
    }
   return true;

}

void InputJoystickComponent::update(float delta) {
    float x_movement = 0;
    if (_x_position > _dead_zone || _x_position < -_dead_zone) {
        if (x_movement > 0) {
            x_movement -= _dead_zone;
        }
        else {
            x_movement += _dead_zone;
        }
        x_movement = _x_position * delta / _sensivity;
    }
    float y_movement = 0;
    if (_y_position > _dead_zone || _y_position < -_dead_zone) {
        if (y_movement > 0) {
            y_movement -= _dead_zone;
        }
        else {
            y_movement += _dead_zone;
        }
        y_movement = _y_position * delta / _sensivity;
    }
    float x = _eyehole_node->getPosition().x + x_movement;
    float y = _eyehole_node->getPosition().y - y_movement;
    if (x > HORIZONTAL_LIMIT) {
        x = HORIZONTAL_LIMIT;
    }
    else if (x < - HORIZONTAL_LIMIT) {
        x = - HORIZONTAL_LIMIT;
    }
    if (y > VERTICAL_LIMIT) {
        y = VERTICAL_LIMIT;
    }
    else if (y < - VERTICAL_LIMIT) {
        y = -VERTICAL_LIMIT;
    }
    _eyehole_node->setPosition(x, y, _eyehole_node->getPosition().z);
    InputComponent::update(delta);
}
