#include "InputMouseComponent.h"
#include "GameObjectCreator.h"

InputMouseComponent::InputMouseComponent (std::shared_ptr<GameObject> gameObject, int player_number, std::shared_ptr<PhysicsComponent> physicsComponent) : InputComponent(gameObject, player_number, physicsComponent), _sensivity(0.005) {
    InputManager *inputMgr = InputManager::getSingletonPtr();
    inputMgr->addMouseListener(this, "InputMouseComponent");
};


bool InputMouseComponent::mouseMoved (const OIS::MouseEvent &e) {
    if (_eyehole_node) {
        float x = _eyehole_node->getPosition().x + e.state.X.rel * _sensivity;
        float y = _eyehole_node->getPosition().y - e.state.Y.rel * _sensivity;
        if (x > HORIZONTAL_LIMIT) {
            x = HORIZONTAL_LIMIT;
        }
        else if (x < - HORIZONTAL_LIMIT) {
            x = - HORIZONTAL_LIMIT;
        }
        if (y > VERTICAL_LIMIT) {
            y = VERTICAL_LIMIT;
        }
        else if (y < - VERTICAL_LIMIT) {
            y = -VERTICAL_LIMIT;
        }
        _eyehole_node->setPosition(x, y, _eyehole_node->getPosition().z);
    }
    return true;
}

bool InputMouseComponent::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    switch (id) {
        case OIS::MB_Left:
            _button1_pressed = true;
            break;
        case OIS::MB_Right:
            _button2_pressed = true;
            break;
        default:
            break;
    }
    return true;
}

bool InputMouseComponent::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    switch (id) {
        case OIS::MB_Left:
            _button1_pressed = false;
            break;
        case OIS::MB_Right:
            InputComponent::secondButtonReleased();
            _button2_pressed = false;
            break;
        default:
            break;
    }
    return true;
}

