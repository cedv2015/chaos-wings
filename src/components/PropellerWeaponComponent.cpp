#include "PropellerWeaponComponent.h"

PropellerWeaponComponent::PropellerWeaponComponent (std::shared_ptr<GameObject> gameObject, std::shared_ptr<RigidBodyAnimationComponent> animatorCmp) : WeaponComponent(gameObject, animatorCmp, 0, 0, WeaponComponent::BulletType::NORMAL), _state(State::FINISHED), _time(0), _gameObjectCrt(nullptr) {
    _gameObjectCrt = GameObjectCreator::getSingletonPtr();
}

PropellerWeaponComponent::~PropellerWeaponComponent () {
    _active_propellers.clear();
    _particles.clear();
}

void PropellerWeaponComponent::update (float delta) {
    // If _active_propellers is empty, reload it with new values
    if (_active_propellers.empty()) {
        int number_propellers = rand() % 2 + 3;
        std::vector<int> free;
        for (int i = 0; i < 6; ++i) {
            free.push_back(i);
        }
        while (number_propellers >= 0) {
            int position = rand() % free.size();
            _active_propellers.push_back(free[position]);
            _particles[free[position]]->switchToParticle(ParticlesComponent::Type::FINAL_BOSS_PROPELLER_CHARGING);
            --number_propellers;
            free.erase(free.begin() + position);
        }
    }

    _time += delta;
    if (_state == State::CHARGING) {
        if (_time > CHARGING_TIME) {
            _state = State::SHOOTING;
            for (unsigned int i = 0; i < _active_propellers.size(); ++i) {
                _particles[_active_propellers[i]]->switchToParticle(ParticlesComponent::Type::FINAL_BOSS_PROPELLER_SHOOTING);
            }
            _time = 0;
        }
    }
    else if (_state == State::SHOOTING) {
        if (_time_last_shot <= 0) {
            for (unsigned int i = 0; i < _active_propellers.size(); ++i) {
                _gameObjectCreator->createPropellerBullet(_particles[_active_propellers[i]]->getPosition(), _particles[_active_propellers[i]]->getDirection() * _particles[_active_propellers[i]]->getMinVelocity());
            }
            _time_last_shot = TIME_BETWEEN_SHOTS;
        }
        else {
            _time_last_shot -= delta;
        }
        if (_time > SHOOTING_TIME) {
            _state = State::FINISHED;
            for (unsigned int i = 0; i < _active_propellers.size(); ++i) {
                _particles[_active_propellers[i]]->switchToParticle(ParticlesComponent::Type::FINAL_BOSS_PROPELLER_NORMAL);
            }
        }
    }
}

const float PropellerWeaponComponent::CHARGING_TIME = 2;
const float PropellerWeaponComponent::SHOOTING_TIME = 2;
const float PropellerWeaponComponent::TIME_BETWEEN_SHOTS = 2;
