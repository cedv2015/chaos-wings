#include "SoundComponent.h"
#include "SoundFXManager.h"

SoundComponent::SoundComponent (std::shared_ptr<GameObject> gameObject, int type, int loop) : Component(gameObject, Component::Type::SOUND), _loop(loop), _channel(0) {
    _appear.setNull();
    _disappear.setNull();
    _attack.setNull();

    SoundFXManager *soundFXMgr = SoundFXManager::getSingletonPtr();
    int random;
    switch(type){
        case SoundComponent::Type::PLAYER:
            _disappear = soundFXMgr->load("dead.wav");
            break;
        case SoundComponent::Type::ENEMY:
            random = 1+rand()%5;
            soundFXMgr->load("enemy_disappear"+std::to_string(random)+".wav")->play(_loop);
            break;
        case SoundComponent::Type::LASER_WALL:
            soundFXMgr->load("laser_wall.wav")->play(_loop);
            break;
        case SoundComponent::Type::PART:
            soundFXMgr->load("explosion.wav")->play(_loop);
            break;
        case SoundComponent::Type::BOSS1:
            _appear = soundFXMgr->load("motor.wav");
            _channel = _appear->play(_loop);
            _disappear = soundFXMgr->load("explosion_multi.wav");
            break;
        case SoundComponent::Type::BOSS2:
            soundFXMgr->load("boss2.wav")->play(_loop);
            _disappear = soundFXMgr->load("explosion_multi_boss2.wav");
            break;
        case SoundComponent::Type::PLAYER_BULLET:
            soundFXMgr->load("bullet.wav")->play(_loop);
            break;
        case SoundComponent::Type::LASER:
            //Error a veces cuando intentan reproducirse muchas veces a la vez
            random = 1+rand()%2;
            soundFXMgr->load("laser"+std::to_string(random)+".wav")->play(_loop);
        break;
        case SoundComponent::Type::MISSILE:
            random = 1+rand()%4;
            soundFXMgr->load("missile"+std::to_string(random)+".wav")->play(_loop);
            break;


    }
}

SoundComponent::~SoundComponent () {

}

void SoundComponent::update(float deltaT){}

void SoundComponent::disappear(){
    if (!_appear.isNull()) {
        if (_channel != -1) {
            _appear->stop(_channel);
        }
    }
    if (!_disappear.isNull()) {
        _disappear->play(0);
    }
}
