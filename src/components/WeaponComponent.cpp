#include "WeaponComponent.h"

void WeaponComponent::update(float delta) {
    if (_animatorCmp->getState() == RigidBodyAnimationComponent::State::ATTACKING) {
        _next_shoot -= delta;
        if (_next_shoot <= 0) {
            Ogre::Vector3 position = _animatorCmp->getPosition() + _animatorCmp->getOrientation() * _relative_position;
            _gameObjectCreator->createEnemyBullet(_bullet_type, position);
            _next_shoot = (rand() % (_max_freq - _min_freq)) + _min_freq;
        }
    }
}
