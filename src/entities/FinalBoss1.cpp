#include "FinalBoss1.h"
#include "NodeComponent.h"
#include "PhysicsComponent.h"
#include "StatusComponent.h"
#include "BurstWeaponComponent.h"
#include "PropellerWeaponComponent.h"
#include "ParticlesComponent.h"
#include "CEGUIComponent.h"
#include "TrackManager.h"

FinalBoss1::FinalBoss1 () : GameObject(), _state(State::WEAPON1), _time_in_state(0), _time_limit(TIME_WEAPON_1), _dead(false) {

}

FinalBoss1::~FinalBoss1 () {
    _animationCmp = nullptr;
    _soundCmp = nullptr;
    _weapons1.clear();
    _weapons2.clear();
    _weapons3.clear();
}

void FinalBoss1::initComponents () {
    std::shared_ptr<NodeComponent> nodeCmp = std::make_shared<NodeComponent>(shared_from_this(), "FinalBoss1", _id);
    std::shared_ptr<PhysicsComponent> physicsCmp = std::make_shared<PhysicsComponent>(shared_from_this(), nodeCmp->getNode(), nodeCmp->getEntity(), std::string("FinalBoss1") + std::to_string(_id), PhysicsComponent::Type::ENEMY, PhysicsComponent::Shape::CONVEX, true, Ogre::Vector3(300, 0, 3), Ogre::Vector3::ZERO);
    _animationCmp = std::make_shared<RigidBodyAnimationComponent>(shared_from_this(), physicsCmp, nodeCmp->getEntity(), true, true);
    std::shared_ptr<StatusComponent> statusCmp = std::make_shared<StatusComponent>(shared_from_this(), 100, StatusComponent::Type::FINALBOSS1);
    statusCmp->setTimeInvulnerable(13.6);
    std::shared_ptr<CEGUIComponent> ceguiCmp = std::make_shared<CEGUIComponent>(shared_from_this(), _id, CEGUIComponent::Type::FINALBOSS1);
    _soundCmp = std::make_shared<SoundComponent>(shared_from_this(), SoundComponent::Type::BOSS1, -1);
    physicsCmp->setStatusComponent(statusCmp);
    addComponent(_animationCmp);
    addComponent(physicsCmp);
    addComponent(nodeCmp);
    addComponent(statusCmp);
    addComponent(ceguiCmp);
    addComponent(_soundCmp);

    for (int i = 0; i < 2; ++i) {
        std::shared_ptr<BurstWeaponComponent> burstCmp = std::make_shared<BurstWeaponComponent>(shared_from_this(), _animationCmp, 2, 4, 10, 0.1, Ogre::Vector3(-10.20222 + 10.20222 * 2 * i, -0.25834, 5.67614));
        _weapons1.push_back(burstCmp);
    }
    for (int i = 0; i < 2; ++i) {
        std::shared_ptr<WeaponComponent> weaponCmp = std::make_shared<WeaponComponent>(shared_from_this(), _animationCmp, 2, 4, WeaponComponent::BulletType::MISSILE, Ogre::Vector3(-1.31385 + 1.31385 * 2 * i, 0.21676, 6.61622));
        _weapons2.push_back(weaponCmp);
    }
    std::shared_ptr<PropellerWeaponComponent> weapon = std::make_shared<PropellerWeaponComponent>(shared_from_this(), _animationCmp);
    _weapons3.push_back(weapon);

    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 3; ++j) {
            Ogre::Vector3 position;
            switch (j + i * 3) {
                case 0:
                    position = Ogre::Vector3(-5.36474, 1.627872, 6.75001);
                    break;
                case 1:
                    position = Ogre::Vector3(0, 2.37553, 7.1279);
                    break;
                case 2:
                    position = Ogre::Vector3(5.36474, 1.627872, 6.75001);
                    break;
                case 3:
                    position = Ogre::Vector3(-5.36474, -2.15561, 6.75001);
                    break;
                case 4:
                    position = Ogre::Vector3(0, -2.5711, 7.1279);
                    break;
                case 5:
                    position = Ogre::Vector3(5.36474, -2.15561, 6.75001);
                    break;
                default:
                    break;
            }
            std::shared_ptr<ParticlesComponent> particleCmp = std::make_shared<ParticlesComponent>(shared_from_this(), physicsCmp->getPosition(), ParticlesComponent::Type::FINAL_BOSS_PROPELLER_NORMAL, nodeCmp->getNode(), position, Ogre::Vector3(0, 0, 1));
            addComponent(particleCmp);
            std::static_pointer_cast<PropellerWeaponComponent>(_weapons3[0])->addParticleComponent(particleCmp);
        }
    }

    TrackManager::getSingletonPtr()->fadeToSong("Phase3_song.wav");

    ++_id;
}

void FinalBoss1::removeComponents () {
    _animationCmp = nullptr;
    _soundCmp = nullptr;
    _weapons1.clear();
    _weapons2.clear();
    _weapons3.clear();
    GameObject::removeComponents();
}

void FinalBoss1::update (float delta) {
    GameObject::update(delta);
    if (_animationCmp->getState() == RigidBodyAnimationComponent::State::ATTACKING) {
        if (_dead) {
            _animationCmp->addToInitialPosition(Ogre::Vector3(0, - delta * 2, 0));
            if (_animationCmp->getPosition().y < - 12) {
                eliminate();
            }
            return;
        }
        _time_in_state += delta;
        switch (_state) {
            case State::WEAPON1:
                for (unsigned int i = 0; i < _weapons1.size(); ++i) {
                    _weapons1[i]->update(delta);
                }
                break;
            case State::WEAPON2:
                for (unsigned int i = 0; i < _weapons2.size(); ++i) {
                    _weapons2[i]->update(delta);
                }
                break;
            case State::WEAPON3:
                for (unsigned int i = 0; i < _weapons3.size(); ++i) {
                    _weapons3[i]->update(delta);
                }
                break;
            default:
                break;
        }
        // State machine
        if (_time_in_state > _time_limit) {
            int next_state = rand() % 2;
            switch (next_state) {
                case 0:
                    if (_state == WEAPON2 || _state == WEAPON3) {
                        _state = WEAPON1;
                        _time_limit = TIME_WEAPON_1;
                        for (unsigned int i = 0; i < _weapons1.size(); ++i) {
                            _weapons1[i]->resetCounters();
                        }
                    }
                    else {
                        _state = WEAPON2;
                        _time_limit = TIME_WEAPON_2;
                        for (unsigned int i = 0; i < _weapons2.size(); ++i) {
                            _weapons2[i]->resetCounters();
                        }
                    }
                    break;
                case 1:
                    if (_state == WEAPON1 || _state == WEAPON2) {
                        _state = WEAPON3;
                        _time_limit = TIME_WEAPON_3;
                        for (unsigned int i = 0; i < _weapons3.size(); ++i) {
                            _weapons3[i]->resetCounters();
                        }
                    }
                    else {
                        _state = WEAPON2;
                        _time_limit = TIME_WEAPON_2;
                        for (unsigned int i = 0; i < _weapons2.size(); ++i) {
                            _weapons2[i]->resetCounters();
                        }
                    }
                    break;
                default:
                    break;
            }
            _time_in_state = 0;
        }
    }
}

void FinalBoss1::finish () {
    if (!_dead) {
        _dead = true;
        std::shared_ptr<ParticlesComponent> particleCmp = std::make_shared<ParticlesComponent>(shared_from_this(), _animationCmp->getPosition(), ParticlesComponent::Type::FINAL_BOSS_1_EXPLOSION, std::static_pointer_cast<NodeComponent>(getComponent(Component::Type::NODE))->getNode());
        addComponent(particleCmp);
        _soundCmp->disappear();
    }
}

void FinalBoss1::eliminate () {
    GameObject::finish();
}

int FinalBoss1::_id = 0;
