#include "FinalBoss2.h"
#include "NodeComponent.h"
#include "PhysicsComponent.h"
#include "StatusComponent.h"
#include "ParticlesComponent.h"
#include "CEGUIComponent.h"
#include "TrackManager.h"

FinalBoss2::FinalBoss2 () : GameObject(), _state(State::WEAPON1), _time_in_state(0), _time_limit(TIME_WEAPON_1), _active(false), _has_collided_with_water(false), _dead(false) {

}

FinalBoss2::~FinalBoss2 () {
    _animationCmp = nullptr;
    _soundCmp = nullptr;
    _weapons1.clear();
    _weapons2.clear();
    _weapons3.clear();
}

void FinalBoss2::initComponents () {
    std::shared_ptr<NodeComponent> nodeCmp = std::make_shared<NodeComponent>(shared_from_this(), "FinalBoss2", _id);
    std::shared_ptr<PhysicsComponent> physicsCmp = std::make_shared<PhysicsComponent>(shared_from_this(), nodeCmp->getNode(), nodeCmp->getEntity(), std::string("FinalBoss2") + std::to_string(_id), PhysicsComponent::Type::ENEMY, PhysicsComponent::Shape::CONVEX, true, Ogre::Vector3(300, 0, 0), Ogre::Vector3::ZERO);
    _animationCmp = std::make_shared<RigidBodyAnimationComponent>(shared_from_this(), physicsCmp, nodeCmp->getEntity(), true, true);
    std::shared_ptr<StatusComponent> statusCmp = std::make_shared<StatusComponent>(shared_from_this(), 100, StatusComponent::Type::FINALBOSS2);
    statusCmp->setTimeInvulnerable(8.2);
    std::shared_ptr<CEGUIComponent> ceguiCmp = std::make_shared<CEGUIComponent>(shared_from_this(), _id, CEGUIComponent::Type::FINALBOSS1);
    _soundCmp = std::make_shared<SoundComponent>(shared_from_this(), SoundComponent::Type::BOSS2);
    physicsCmp->setStatusComponent(statusCmp);
    addComponent(_animationCmp);
    addComponent(physicsCmp);
    addComponent(nodeCmp);
    addComponent(statusCmp);
    addComponent(ceguiCmp);
    addComponent(_soundCmp);

    // Lasers
    for (int i = 0; i < 2; ++i) {
        std::shared_ptr<WeaponComponent> weaponCmp = std::make_shared<WeaponComponent>(shared_from_this(), _animationCmp, 1, 3, WeaponComponent::BulletType::SHOTGUN, Ogre::Vector3(-2.33349 + 2.33349 * 2 * i, 3.21194, 7.47083));
        _weapons1.push_back(weaponCmp);
    }
    // Missiles
    for (int i = 0; i < 2; ++i) {
        std::shared_ptr<WeaponComponent> weaponCmp = std::make_shared<WeaponComponent>(shared_from_this(), _animationCmp, 2, 4, WeaponComponent::BulletType::MISSILE, Ogre::Vector3(-6.56219 + 6.56219 * 2 * i, 0.03665, 7.88245));
        _weapons2.push_back(weaponCmp);
    }
    // Laser wall
    std::shared_ptr<WeaponComponent> weaponCmp = std::make_shared<WeaponComponent>(shared_from_this(), _animationCmp, 3, 5, WeaponComponent::BulletType::WALL, Ogre::Vector3(0, 4.20046, 5.1733));
    _weapons3.push_back(weaponCmp);

    //TODO: add particles components (one for each propeller)
    for (int i = 0; i < 4; ++i) {
        Ogre::Vector3 position;
        switch (i) {
            case 0:
                position = Ogre::Vector3(-4.54105, 0.31439, 8.4346);
                break;
            case 1:
                position = Ogre::Vector3(-4.54105, -0.99119, 8.4346);
                break;
            case 2:
                position = Ogre::Vector3(4.54105, 0.31439, 8.4346);
                break;
            case 3:
                position = Ogre::Vector3(4.54105, -0.99119, 8.4346);
                break;
            default:
                break;
        }
        std::shared_ptr<ParticlesComponent> particleCmp = std::make_shared<ParticlesComponent>(shared_from_this(), physicsCmp->getPosition(), ParticlesComponent::Type::FINAL_BOSS_PROPELLER_NORMAL, nodeCmp->getNode(), position, Ogre::Vector3(0, 0, 1));
        addComponent(particleCmp);
    }

    std::shared_ptr<ParticlesComponent> particleCmp2 = std::make_shared<ParticlesComponent>(shared_from_this(), physicsCmp->getPosition(), ParticlesComponent::Type::FINAL_BOSS_2_RISING, nodeCmp->getNode(), Ogre::Vector3(-2.71361, 2.60278, -10.18038));
    std::shared_ptr<ParticlesComponent> particleCmp3 = std::make_shared<ParticlesComponent>(shared_from_this(), physicsCmp->getPosition(), ParticlesComponent::Type::FINAL_BOSS_2_RISING, nodeCmp->getNode(), Ogre::Vector3(2.71361, 2.60278, -10.18038));
    std::shared_ptr<ParticlesComponent> particleCmp4 = std::make_shared<ParticlesComponent>(shared_from_this(), physicsCmp->getPosition(), ParticlesComponent::Type::FINAL_BOSS_2_RISING, nodeCmp->getNode(), Ogre::Vector3(-0.49434, 5.14839, -0.4863));
    std::shared_ptr<ParticlesComponent> particleCmp5 = std::make_shared<ParticlesComponent>(shared_from_this(), physicsCmp->getPosition(), ParticlesComponent::Type::FINAL_BOSS_2_RISING, nodeCmp->getNode(), Ogre::Vector3(0.49434, 5.14839, -0.4863));
    addComponent(particleCmp2);
    addComponent(particleCmp3);
    addComponent(particleCmp4);
    addComponent(particleCmp5);

    TrackManager::getSingletonPtr()->fadeToSong("Phase3_song.wav");

    ++_id;
}

void FinalBoss2::removeComponents () {
    _animationCmp = nullptr;
    _soundCmp = nullptr;
    _weapons1.clear();
    _weapons2.clear();
    _weapons3.clear();
    GameObject::removeComponents();
}

void FinalBoss2::update (float delta) {
    GameObject::update(delta);
    if (!_has_collided_with_water) {
        float time = std::static_pointer_cast<StatusComponent>(getComponent(Component::Type::STATUS))->getTimeAlive();
        if (time > 6) {
            _has_collided_with_water = true;
            Ogre::Vector3 position = std::static_pointer_cast<PhysicsComponent>(getComponent(Component::Type::PHYSICS))->getPosition();
            position.y = -6.99;
            std::shared_ptr<ParticlesComponent> particleCmp1 = std::make_shared<ParticlesComponent>(shared_from_this(), position, ParticlesComponent::Type::FINAL_BOSS_2_COLLISION_WATER1, nullptr);
            addComponent(particleCmp1);
            std::shared_ptr<ParticlesComponent> particleCmp2 = std::make_shared<ParticlesComponent>(shared_from_this(), position, ParticlesComponent::Type::FINAL_BOSS_2_COLLISION_WATER2, nullptr);
            addComponent(particleCmp2);
        }
    }
    if (!_active) {
        float time = std::static_pointer_cast<StatusComponent>(getComponent(Component::Type::STATUS))->getTimeAlive();
        if (time > 3) {
            _active = true;
            Ogre::Vector3 position = std::static_pointer_cast<PhysicsComponent>(getComponent(Component::Type::PHYSICS))->getPosition();
            position.y = -6.99;
            std::shared_ptr<ParticlesComponent> particleCmp6 = std::make_shared<ParticlesComponent>(shared_from_this(), position, ParticlesComponent::Type::FINAL_BOSS_2_SPEED, nullptr);
            addComponent(particleCmp6);
        }
    }
    if (_animationCmp->getState() == RigidBodyAnimationComponent::State::ATTACKING) {
        if (_dead) {
            _animationCmp->addToInitialPosition(Ogre::Vector3(0, - delta * 2, 0));
            if (_animationCmp->getPosition().y < - 10) {
                eliminate();
            }
            return;
        }
        _time_in_state += delta;
        switch (_state) {
            case State::WEAPON1:
                for (unsigned int i = 0; i < _weapons1.size(); ++i) {
                    _weapons1[i]->update(delta);
                }
                break;
            case State::WEAPON2:
                for (unsigned int i = 0; i < _weapons2.size(); ++i) {
                    _weapons2[i]->update(delta);
                }
                break;
            case State::WEAPON3:
                for (unsigned int i = 0; i < _weapons3.size(); ++i) {
                    _weapons3[i]->update(delta);
                }
                break;
            default:
                break;
        }
        // State machine
        if (_time_in_state > _time_limit) {
            int next_state = rand() % 2;
            switch (next_state) {
                case 0:
                    if (_state == WEAPON2 || _state == WEAPON3) {
                        _state = WEAPON1;
                        _time_limit = TIME_WEAPON_1;
                        for (unsigned int i = 0; i < _weapons1.size(); ++i) {
                            _weapons1[i]->resetCounters();
                        }
                    }
                    else {
                        _state = WEAPON2;
                        _time_limit = TIME_WEAPON_2;
                        for (unsigned int i = 0; i < _weapons2.size(); ++i) {
                            _weapons2[i]->resetCounters();
                        }
                    }
                    break;
                case 1:
                    if (_state == WEAPON1 || _state == WEAPON2) {
                        _state = WEAPON3;
                        _time_limit = TIME_WEAPON_3;
                        for (unsigned int i = 0; i < _weapons3.size(); ++i) {
                            _weapons3[i]->resetCounters();
                        }
                    }
                    else {
                        _state = WEAPON2;
                        _time_limit = TIME_WEAPON_2;
                        for (unsigned int i = 0; i < _weapons2.size(); ++i) {
                            _weapons2[i]->resetCounters();
                        }
                    }
                    break;
                default:
                    break;
            }
            _time_in_state = 0;
        }
        if (_dead) {
            _animationCmp->addToInitialPosition(Ogre::Vector3(0, - delta * 3, 0));
            if (_animationCmp->getPosition().y < - 10) {
                eliminate();
            }
        }
    }
}

void FinalBoss2::finish () {
    if (!_dead) {
        _dead = true;
        std::shared_ptr<ParticlesComponent> particleCmp = std::make_shared<ParticlesComponent>(shared_from_this(), _animationCmp->getPosition(), ParticlesComponent::Type::FINAL_BOSS_2_EXPLOSION, std::static_pointer_cast<NodeComponent>(getComponent(Component::Type::NODE))->getNode());
        addComponent(particleCmp);
        _soundCmp->disappear();
    }
}

void FinalBoss2::eliminate () {
    GameObject::finish();
}

int FinalBoss2::_id = 0;
