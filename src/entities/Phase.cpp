#include "Phase.h"
#include <memory>
#include "NodeComponent.h"
#include "ParticlesComponent.h"
#include "PhysicsComponent.h"
#include "CEGUIComponent.h"

void Phase::initComponents () {
    std::shared_ptr<NodeComponent> nodeComponent;
    std::shared_ptr<ParticlesComponent> particlesCmp;
    std::shared_ptr<PhysicsComponent> physicsCmp;
    std::shared_ptr<CEGUIComponent> ceguiCmp;
    switch(_id){
        case 1:
            nodeComponent = std::make_shared<NodeComponent>(shared_from_this(), "Scenario1", 2, false);
            particlesCmp = std::make_shared<ParticlesComponent>(shared_from_this(), Ogre::Vector3::ZERO, ParticlesComponent::Type::SPEEDPHASE1);
            physicsCmp = std::make_shared<PhysicsComponent>(shared_from_this(), nodeComponent->getNode(), nodeComponent->getEntity(), std::string("Phase"), PhysicsComponent::Type::PHASE, PhysicsComponent::Shape::STATIC_PLANE, false, Ogre::Vector3(0, -7, 0));
            ceguiCmp = std::make_shared<CEGUIComponent>(shared_from_this(), 1, CEGUIComponent::Type::PHASE);
            break;
        case 2:
            nodeComponent = std::make_shared<NodeComponent>(shared_from_this(), "Scenario2", 2, false);
            particlesCmp = std::make_shared<ParticlesComponent>(shared_from_this(), Ogre::Vector3::ZERO, ParticlesComponent::Type::SPEEDPHASE2);
            physicsCmp = std::make_shared<PhysicsComponent>(shared_from_this(), nodeComponent->getNode(), nodeComponent->getEntity(), std::string("Phase"), PhysicsComponent::Type::PHASE, PhysicsComponent::Shape::STATIC_PLANE, false, Ogre::Vector3(0, -7, 0));
            ceguiCmp = std::make_shared<CEGUIComponent>(shared_from_this(), 2, CEGUIComponent::Type::PHASE);

            break;
    }
    ceguiCmp->visibilityHUD(false);
    _components.push_back(nodeComponent);
    _components.push_back(physicsCmp);
    _components.push_back(particlesCmp);
    _components.push_back(ceguiCmp);
}
