#include "GameManager.h"
#include "MainState.h"
#include "MenuState.h"
#include "PlayState.h"
#include "PauseState.h"
#include "GameOverState.h"
#include "InstructionsState.h"
#include "RankingState.h"
#include "CreditsState.h"
#include "EndGameState.h"
#include "DialogueState.h"

int main () {
    GameManager *game = new GameManager;

    new MainState;
    new MenuState;
    new PlayState;
    new PauseState;
    new InstructionsState;
    new RankingState;
    new GameOverState;
    new CreditsState;
    new EndGameState;
    new DialogueState;

    try {
        game->start(MainState::getSingletonPtr());
    } catch (Ogre::Exception &e) {
        std::cout << e.getFullDescription() << std::endl;
    }

    delete game;

    return 0;
}
