#include "CameraManager.h"

template<> CameraManager* Ogre::Singleton<CameraManager>::msSingleton = nullptr;

CameraManager::CameraManager() : _number_of_players(0), _type_of_camera(0), _time(0) {
    _camera = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager")->getCamera("MainCamera");
}

CameraManager::~CameraManager() {
    _camera = nullptr;
   _players.clear();
}

CameraManager* CameraManager::getSingletonPtr () {
    return msSingleton;
}

CameraManager& CameraManager::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

void CameraManager::update(float delta) {
    if (_type_of_camera == 0) {
        Ogre::Vector3 final_position;
        Ogre::Vector3 camera_position = _camera->getPosition();
        float player_distance;
        // simple case, only 1 player; check the player position, and move the camera accordingly
        if (_number_of_players == 1) {
            final_position = _players.begin()->second->getPosition();
            player_distance = 0;
        }
        // 2 _players; check if the distance between them is too big and the
        // camera has to be moved backwards; move the camera to the intermediate
        // point between the two _players
        else if (_number_of_players == 2) {
            Ogre::Vector3 player1_position = _players.begin()->second->getPosition();
            Ogre::Vector3 player2_position = (++_players.begin())->second->getPosition();
            final_position = (player1_position + player2_position) / 2;
            // move camera in Z axis
            float player_distance_x = player1_position.x - player2_position.x;
            float player_distance_y = player1_position.y - player2_position.y;
            player_distance = std::sqrt(std::pow(player_distance_x, 2) + std::pow(player_distance_y, 2)) / 3;
            player_distance *= Z_MULTIPLIER;
            final_position.y += player_distance / 2.5;
        }
        // if there are no _players, the camera wont move
        else {
            return;
        }
        // we dont want to change the camera just behind the player node, but near from it
        // _camera->setPosition(camera_position.x, camera_position.y, player_distance);
        final_position /= 2;
        final_position.z *= 2;
        float distance_x = final_position.x - camera_position.x;
        float distance_y = final_position.y - camera_position.y;
        float distance_z = player_distance - camera_position.z;
        // if the camera is near to the final position, dont move it
        if (distance_x < - MINIMUM_DISTANCE || distance_x > MINIMUM_DISTANCE || distance_y < - MINIMUM_DISTANCE || distance_y > MINIMUM_DISTANCE) {
            float total_distance = std::sqrt(std::pow(distance_x, 2) + std::pow(distance_y, 2)) / 3;
            camera_position.x += distance_x * delta / total_distance;
            camera_position.y += distance_y * delta / total_distance;
            _camera->setPosition(camera_position);
        }
        if (distance_z < - MINIMUM_DISTANCE || distance_z > MINIMUM_DISTANCE) {
            camera_position.z += distance_z * delta;
            _camera->setPosition(camera_position);
        }
        _camera->setOrientation(Ogre::Quaternion(1, 0, 0, 0));
    }
    else {
        if(_players.size()>0){
            _time += delta / 2;
            Ogre::Vector3 player_position = _players.begin()->second->getPosition();
            float radius = 8;
            Ogre::Vector3 camera_position(player_position.x + Ogre::Math::Cos(_time) * radius, player_position.y, player_position.z + Ogre::Math::Sin(_time) * radius);
            _camera->setPosition(camera_position);
            _camera->lookAt(player_position);
        }
    }
}

void CameraManager::addPlayer(Ogre::SceneNode* player, int player_number) {
    ++_number_of_players;
    _players.insert(std::make_pair(player_number, player));
}

void CameraManager::removePlayer (int player_number) {
    --_number_of_players;
    for (auto it = _players.begin(); it != _players.end(); ++it) {
        if (it->first == player_number) {
            _players.erase(it);
            return;
        }
    }
}

Ogre::SceneNode* CameraManager::getRandomPlayer() {
    if (_players.size() > 1) {
        int random = rand() % 2;
        if (random == 0) {
            return _players.begin()->second;
        }
        else {
            return (++_players.begin())->second;
        }
    }
    else if (_players.size() > 0) {
        return _players.begin()->second;
    }
    else {
        return nullptr;
    }
}

bool CameraManager::isPlayerAlive (Ogre::SceneNode *node) {
    for (auto it = _players.begin(); it != _players.end(); ++it) {
        if (it->second == node) {
            return true;
        }
    }
    return false;
}

void CameraManager::switchState () {
    if (_type_of_camera == 0) {
        _type_of_camera = 1;
    }
    else {
        _type_of_camera = 0;
    }
}

const float CameraManager::MINIMUM_DISTANCE = 0.01;
const float CameraManager::Z_MULTIPLIER = 1.7;
