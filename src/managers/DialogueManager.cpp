#include "DialogueManager.h"
#include "DialogueState.h"
#include "OsUtils.h"


template <> DialogueManager* Ogre::Singleton<DialogueManager>::msSingleton = nullptr;

DialogueManager::DialogueManager (): _hud(nullptr), _exitAnim(false), _next_dialogue(false), _finish_dialogue(false), _message(false), _speed(false), _dialogue_cont(1), _phase(1), _moveOut(0), d_timeSinceStart(0.0f) {}

DialogueManager* DialogueManager::getSingletonPtr () {
    return msSingleton;
}

DialogueManager& DialogueManager::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}

void DialogueManager::newIntervention(int phase, int dialogue_cont){
    _phase = phase;
    _dialogue_cont = dialogue_cont;
    _moveOut = 0;
    loadDialogue();
    chargeDialogue();
    d_botBarAnimInst->start();
}

void DialogueManager::chargeDialogue(){
    initCEGUI();
    _next_dialogue = true;
    _speed = false;
    d_timeSinceStart = 0.0;
    VELOCITY = 0.08f;
    setDialogue(0.0);
}
void DialogueManager::update(float delta) {

    d_timeSinceStart += delta;
    if(_message)
        updateText(delta);

}

void DialogueManager::hide(){
    if(_hud)
        _hud->setVisible(false);
}

void DialogueManager::loadDialogue() {

    _data.clear();
    std::string separator = "";
    int os = OsUtils::getOs();
    if (os == OsUtils::OS::LINUX || os == OsUtils::OS::MAC) {
        separator = "/";
    }
    else if (os == OsUtils::OS::WINDOWS) {
        separator = "\\";
    }
    std::string file_name = std::string("misc") + separator + std::string("phase") + std::to_string(_phase) + std::string("-") + std::to_string(_dialogue_cont)+ std::string(".txt");

    std::ifstream file(file_name);
    std::string line;
    if (file.is_open()) {
        std::string item;
        while (getline(file, line)) {
            std::stringstream iss;
            iss << line;
            while (std::getline (iss, item, DELIM)) {
                _data.push_back(item);
            }
        }
        file.close();
    }else{
        _finish_dialogue = true;
    }
}

void DialogueManager::initCEGUI(){

    if(_hud==nullptr){
        _hud = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("DialogueMenu.layout");
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_hud);
        _botBarLabel = _hud->getChild("BotBar/BotBarLabel");
        _botNameLabel = _hud->getChild("BotBar/NameLabel");
        _botBar = _hud->getChild("BotBar");
        setupAnimations();
    }else
        _hud->show();
        _hud->setVisible(true);
}

void DialogueManager::setupAnimations(){

  CEGUI::AnimationManager& animMgr = CEGUI::AnimationManager::getSingleton();

  CEGUI::Animation* botBarAnim = animMgr.getAnimation("BotBarDialogoMoveInAnimation");
  d_botBarAnimInst = animMgr.instantiateAnimation(botBarAnim);
  d_botBarAnimInst->setTargetWindow(_botBar);

  CEGUI::Animation* botBarOutAnim = animMgr.getAnimation("BotBarDialogoMoveOutAnimation");
  d_botBarOutAnimInst = animMgr.instantiateAnimation(botBarOutAnim);
  d_botBarOutAnimInst->setTargetWindow(_botBar);
}

void DialogueManager::setDialogue(float delta)
{
    int id;
    std::string name;
    if(_next_dialogue && !_finish_dialogue){
        if (_data.size() > 0) {
            id = std::stoi(_data[0]);
            name = _data[1];
            _text = _data[2];
            _data.erase(_data.begin(), _data.begin() + 3);
            _next_dialogue = false;
            _botNameLabel->setText(name);
            switch(id){
                case 1:
                    _hud->getChild("BotBar/ImagePhoto1")->setVisible(true);
                    _hud->getChild("BotBar/ImagePhoto2")->setVisible(false);
                break;
                case 2:
                    _hud->getChild("BotBar/ImagePhoto1")->setVisible(false);
                    _hud->getChild("BotBar/ImagePhoto2")->setVisible(true);
            }

        }else{
              _botNameLabel->setText("");
              _hud->getChild("BotBar/ImagePhoto1")->setVisible(false);
              _hud->getChild("BotBar/ImagePhoto2")->setVisible(false);
              _text = "";
              _moveOut++;
              if(_moveOut==1){
                  d_botBarOutAnimInst->start();
                  DialogueState::getSingletonPtr()->popState();
              }
        }
    }
    if(_finish_dialogue)
        _botBar->setVisible(false);

    _message = true;
}

void DialogueManager::updateText(float delta){

    CEGUI::String firstPart, secondPart;
    int limit;
    if(_text.length()>(unsigned)_limit_letter){
        limit = _limit_letter;
        while(_text.at(limit) != ' '){
              limit++;
        }
      firstPart = _text.substr(0,limit);
      secondPart = _text.substr(limit,_text.length());
    }else{
        firstPart = _text;
        secondPart = "";
    }

    if(!_speed)
        s_secondStartDelay = firstPart.size()/13.0f;

    CEGUI::String finalText;

    int firstPartTypeProgress = static_cast<int>((d_timeSinceStart - s_firstStartDelay) / VELOCITY);
    if(firstPartTypeProgress > 0)
        finalText += firstPart.substr(0, std::min<unsigned int>(firstPart.length(), firstPartTypeProgress));

    int secondPartTypeProgress = static_cast<int>((d_timeSinceStart - s_secondStartDelay) / VELOCITY);
    if(secondPartTypeProgress > 0)
        finalText += "\n" + secondPart.substr(0, std::min<unsigned int>(secondPart.length(), secondPartTypeProgress));

    if(finalText.length()>_text.length())
        _message = false;

    _botBarLabel->setText(finalText);
}

void DialogueManager::resetDialogue(){
    _dialogue_cont = 0;
    if(_hud)
        _hud->setVisible(false);
}


void DialogueManager::passMessage(){
    VELOCITY = 0.01f;
    s_secondStartDelay = 0.0f;
    _speed = true;

};
