#include "EnemyLoader.h"
#include "PlayState.h"
#include "OsUtils.h"

template<> EnemyLoader* Ogre::Singleton<EnemyLoader>::msSingleton = nullptr;

EnemyLoader::EnemyLoader () : _gameObjCreator(nullptr), _time(0), _next_enemy(0), _phase_number(0) {
    _gameObjCreator = GameObjectCreator::getSingletonPtr ();
}

EnemyLoader::~EnemyLoader () {
    _gameObjCreator = nullptr;
}

EnemyLoader* EnemyLoader::getSingletonPtr () {
    return msSingleton;
}

EnemyLoader& EnemyLoader::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

void EnemyLoader::loadPhase (int number) {
    _time = 0;
    _phase_number = number;
    _data.clear();
    std::string separator = "";
    int os = OsUtils::getOs();
    if (os == OsUtils::OS::LINUX || os == OsUtils::OS::MAC) {
        separator = "/";
    }
    else if (os == OsUtils::OS::WINDOWS) {
        separator = "\\";
    }
    std::string file_name = std::string("misc") + separator + std::string("phase") + std::to_string(number) + std::string(".phs");

    std::ifstream file(file_name);
    std::string line;
    if (file.is_open()) {
        std::string item;
        while (getline(file, line)) {
            std::stringstream iss;
            iss << line;
            while (std::getline (iss, item, DELIM)) {
                _data.push_back(item);
            }
        }
        file.close();
    }

    _next_enemy = std::stod(_data[0]);
}

void EnemyLoader::update (float delta) {
    _time += delta;
    if (_data.size() > 0) {
        while (_next_enemy < _time && _data.size() > 0) {
            int type = std::stoi(_data[1]);
            Ogre::Vector3 position(std::stod(_data[2]), std::stod(_data[3]), std::stod(_data[4]));
            _gameObjCreator->createEnemy(type, position);
            _data.erase(_data.begin(), _data.begin() + 5);
            _next_enemy = std::stod(_data[0]);
        }
    }
}

int EnemyLoader::getPercentajeUntilBoss () {
    if (_data.size() > 0) {
        float time = std::stod(_data[_data.size() - 5]);
        int percentaje = (int)((_time / time) * 100);
        return percentaje;
    }
    return -1;
}

void EnemyLoader::checkEnemiesState() {
    if (_data.size() == 0)
        PlayState::getSingletonPtr()->finishPhase();
}
