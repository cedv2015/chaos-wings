#include "GameManager.h"
#include "GameState.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include "ParticlesManager.h"
#include "GameObjectManager.h"
#include "GameObjectCreator.h"
#include "RankingManager.h"
#include "EnemyLoader.h"
#include "DialogueManager.h"

template<> GameManager* Ogre::Singleton<GameManager>::msSingleton = nullptr;

GameManager::GameManager () : _root(nullptr), _sceneManager(nullptr), _renderWindow(nullptr), _inputMgr(nullptr), _physicsMgr(nullptr) {
    _root = new Ogre::Root();

    loadResources();

    if (!configure()) {
        return;
    }

    _inputMgr = new InputManager;
    _inputMgr->initialise(_renderWindow);

    _inputMgr->addKeyListener(this, "GameManager");
    _inputMgr->addMouseListener(this, "GameManager");
    _inputMgr->addJoystickListener(this, "GameManager");

    OGRE_NEW TrackManager;
    OGRE_NEW SoundFXManager;
    OGRE_NEW ParticlesManager;
    OGRE_NEW GameObjectManager;
    OGRE_NEW GameObjectCreator;
    OGRE_NEW RankingManager;
    OGRE_NEW EnemyLoader;
    OGRE_NEW DialogueManager;

    _root->addFrameListener(this);

    initSDL();

    srand(time(NULL));
}

GameManager* GameManager::getSingletonPtr () {
    return msSingleton;
}

GameManager& GameManager::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

GameManager::~GameManager () {
    while (!_states.empty()) {
        _states.top()->exit();
        _states.pop();
    }

    if (_physicsMgr) {
        delete _physicsMgr;
    }

    if (_inputMgr) {
        delete _inputMgr;
    }

    TrackManager *trackManager = TrackManager::getSingletonPtr();
    if (trackManager) {
        delete trackManager;
    }

    SoundFXManager *soundManager = SoundFXManager::getSingletonPtr();
    if (soundManager) {
        delete soundManager;
    }

    ParticlesManager *particlesMgr = ParticlesManager::getSingletonPtr();
    if (particlesMgr) {
        delete particlesMgr;
    }

    if (_root) {
        delete _root;
    }
}

void GameManager::start (GameState *state) {
    CEGUI::OgreRenderer::bootstrapSystem();

    _root = Ogre::Root::getSingletonPtr();

    _sceneManager = _root->getSceneManager("SceneManager");
    _sceneManager->setShadowTechnique(Ogre::SHADOWTYPE_TEXTURE_MODULATIVE);
    _sceneManager->setShadowTextureCount(30);
    _sceneManager->setShadowTextureSize(512);

    _physicsMgr = new PhysicsManager;

    CEGUI::Scheme::setDefaultResourceGroup("Schemes");
    CEGUI::ImageManager::setImagesetDefaultResourceGroup("Imagesets");
    CEGUI::Font::setDefaultResourceGroup("Fonts");
    CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
    CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");

    CEGUI::SchemeManager::getSingleton().createFromFile("GameMenu.scheme");
    CEGUI::SchemeManager::getSingleton().createFromFile("Generic.scheme");
    CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("Jura-13");
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("GameMenuImages/MouseCursor");

    CEGUI::Window* sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow", "Sheet");
    //CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(sheet);

    //CEGUI::Window* sheet = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("GameMenu.layout");
    CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(sheet);
    changeState(state);

    _root->startRendering();
}

void GameManager::changeState(GameState *state) {
    if (!_states.empty()) {
        _states.top()->exit();
        _states.pop();
    }

    _states.push(state);
    _states.top()->enter();
}

void GameManager::pushState(GameState *state) {
    if (!_states.empty()) {
        _states.top()->pause();
    }
    _states.push(state);
    _states.top()->enter();
}

void GameManager::popState () {
    if (!_states.empty()) {
        _states.top()->exit();
        _states.pop();
    }

    if (!_states.empty()) {
        _states.top()->resume();
    }
}

void GameManager::loadResources() {
    Ogre::ConfigFile cf;
    cf.load("resources.cfg");

    Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator();
    Ogre::String sectionstr, typestr, datastr;
    while (sI.hasMoreElements()) {
        sectionstr = sI.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap *settings = sI.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator i;
        for (i = settings->begin(); i != settings->end(); i++) {
            typestr = i->first;
            datastr = i->second;
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation(datastr, typestr, sectionstr);
        }
    }
}

bool GameManager::configure() {
    if (!_root->restoreConfig()) {
        if (!_root->showConfigDialog()) {
            return false;
        }
    }

    _renderWindow = _root->initialise(true, "Final");

    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

    return true;
}

bool GameManager::frameStarted (const Ogre::FrameEvent& evt) {
    _inputMgr->capture();
    return _states.top()->frameStarted(evt);
}

bool GameManager::frameEnded (const Ogre::FrameEvent& evt) {
    return _states.top()->frameEnded(evt);
}

bool GameManager::keyPressed (const OIS::KeyEvent& e) {
    _states.top()->keyPressed(e);
    return true;
}

bool GameManager::keyReleased (const OIS::KeyEvent& e) {
    _states.top()->keyReleased(e);
    return true;
}

bool GameManager::mouseMoved (const OIS::MouseEvent& e) {
    _states.top()->mouseMoved(e);
    return true;
}

bool GameManager::mousePressed (const OIS::MouseEvent& e, const OIS::MouseButtonID id) {
    _states.top()->mousePressed(e, id);
    return true;
}

bool GameManager::mouseReleased (const OIS::MouseEvent& e, const OIS::MouseButtonID id) {
    _states.top()->mouseReleased(e, id);
    return true;
}

bool GameManager::povMoved(const OIS::JoyStickEvent &e, int pov) {
    _states.top()->povMoved(e, pov);
    return true;
}
bool GameManager::axisMoved(const OIS::JoyStickEvent &e, int axis) {
    _states.top()->axisMoved(e, axis);
    return true;
}
bool GameManager::sliderMoved(const OIS::JoyStickEvent &e, int sliderID) {
    _states.top()->sliderMoved(e, sliderID);
    return true;
}
bool GameManager::buttonPressed(const OIS::JoyStickEvent &e, int button) {
    _states.top()->buttonPressed(e, button);
    return true;

}
bool GameManager::buttonReleased(const OIS::JoyStickEvent &e, int button) {
    _states.top()->buttonReleased(e, button);
    return true;

}
bool GameManager::initSDL () {
    if (SDL_Init(SDL_INIT_AUDIO) < 0) {
        return false;
    }

    atexit (SDL_Quit);

    if (Mix_OpenAudio (MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 4096) < 0) {
        return false;
    }

    atexit (Mix_CloseAudio);

    return true;
}
