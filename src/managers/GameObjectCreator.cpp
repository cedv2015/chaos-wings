#include "GameObjectCreator.h"
#include "PlayerBullet.h"
#include "NodeComponent.h"
#include "PhysicsComponent.h"
#include "StatusComponent.h"
#include "RigidBodyAnimationComponent.h"
#include "WeaponComponent.h"
#include "BurstWeaponComponent.h"
#include "SoundComponent.h"
#include "ParticlesComponent.h"
#include "AIMissileComponent.h"
#include "FinalBoss1.h"
#include "FinalBoss2.h"
#include "InputComponent.h"

template<> GameObjectCreator* Ogre::Singleton<GameObjectCreator>::msSingleton = nullptr;

GameObjectCreator::GameObjectCreator () : _gameObjectMgr(nullptr), _cameraMgr(nullptr), _player_mouse(false), _player_joystick(false), _enemy_bullets(0), _enemy_1(0), _enemy_2(0), _enemy_3(0), _enemy_4(0), _enemy_missile(0), _part(0), _propeller_bullet(0), _sceneMgr(nullptr) {
    _gameObjectMgr = GameObjectManager::getSingletonPtr();
}

GameObjectCreator::~GameObjectCreator () {
    _actual_phase = nullptr;
}

GameObjectCreator* GameObjectCreator::getSingletonPtr () {
    return msSingleton;
}

GameObjectCreator& GameObjectCreator::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

std::shared_ptr<Phase> GameObjectCreator::createPhase(int id){
    _actual_phase = std::make_shared<Phase>(id);
    _actual_phase->initComponents ();
    _gameObjectMgr->addGameObject(_actual_phase);
    return _actual_phase;
}

std::shared_ptr<Player> GameObjectCreator::createPlayerMouse() {
    std::shared_ptr<Player> player = std::make_shared<Player>(1);
    player->initComponents();
    _gameObjectMgr->addGameObject(player);
    setPlayerMouse(true);
    return player;
}

std::shared_ptr<Player> GameObjectCreator::createPlayerJoystick() {
    std::shared_ptr<Player> player = std::make_shared<Player>(2);
    player->initComponents();
    _gameObjectMgr->addGameObject(player);
    setPlayerJoystick(true);
    return player;
}

void GameObjectCreator::createPlayerBullet(Ogre::Vector3 position, Ogre::Vector3 direction) {
    std::shared_ptr<PlayerBullet> bullet = std::make_shared<PlayerBullet>();
    bullet->initComponents(position, direction);
    _gameObjectMgr->addGameObject(bullet);
}

void GameObjectCreator::createNormalEnemyBullet(Ogre::Vector3 position, Ogre::Vector3 direction, bool sound) {
    std::shared_ptr<GameObject> enemy_bullet = std::make_shared<GameObject>();
    std::shared_ptr<NodeComponent> nodeCmp = std::make_shared<NodeComponent>(enemy_bullet, "Enemy_bullet", _enemy_bullets);
    Ogre::Quaternion orientation = Ogre::Vector3(0, 0, 1).getRotationTo((direction - position).normalisedCopy());
    std::shared_ptr<PhysicsComponent> physicsCmp = std::make_shared<PhysicsComponent>(enemy_bullet, nodeCmp->getNode(), nodeCmp->getEntity(), std::string("Enemy_bullet") + std::to_string(_enemy_bullets), PhysicsComponent::Type::ENEMY_BULLET, PhysicsComponent::Shape::STATIC_CONVEX, true, position, Ogre::Vector3::ZERO, orientation);
    Ogre::Vector3 velocity = ((direction - position).normalisedCopy() * ENEMY_BULLET_VELOCITY);
    physicsCmp->setLinearVelocity(velocity);
    std::shared_ptr<StatusComponent> statusCmp = std::make_shared<StatusComponent>(enemy_bullet, 1, StatusComponent::Type::BULLET);
    if(sound){
        std::shared_ptr<SoundComponent> soundCmp = std::make_shared<SoundComponent>(enemy_bullet, SoundComponent::Type::LASER);
        enemy_bullet->addComponent(soundCmp);
    }
    enemy_bullet->addComponent(physicsCmp);
    enemy_bullet->addComponent(nodeCmp);
    enemy_bullet->addComponent(statusCmp);
    ++_enemy_bullets;
    _gameObjectMgr->addGameObject(enemy_bullet);
}

void GameObjectCreator::createWallEnemyBullet(Ogre::Vector3 position, Ogre::Vector3 direction) {
    std::shared_ptr<GameObject> enemy_bullet = std::make_shared<GameObject>();
    std::shared_ptr<NodeComponent> nodeCmp = std::make_shared<NodeComponent>(enemy_bullet, "Enemy_bullet", _enemy_bullets);
    Ogre::Quaternion orientation = Ogre::Vector3(0, 0, 1).getRotationTo((direction - position).normalisedCopy());
    std::shared_ptr<PhysicsComponent> physicsCmp = std::make_shared<PhysicsComponent>(enemy_bullet, nodeCmp->getNode(), nodeCmp->getEntity(), std::string("Enemy_bullet") + std::to_string(_enemy_bullets), PhysicsComponent::Type::ENEMY_BULLET, PhysicsComponent::Shape::STATIC_CONVEX, true, position, Ogre::Vector3::ZERO, orientation);
    Ogre::Vector3 velocity = ((direction - position).normalisedCopy() * ENEMY_BULLET_VELOCITY);
    physicsCmp->setLinearVelocity(velocity / 2);
    std::shared_ptr<StatusComponent> statusCmp = std::make_shared<StatusComponent>(enemy_bullet, 1, StatusComponent::Type::BULLET);
    std::shared_ptr<SoundComponent> soundCmp = std::make_shared<SoundComponent>(enemy_bullet, SoundComponent::Type::LASER_WALL);
    enemy_bullet->addComponent(physicsCmp);
    enemy_bullet->addComponent(nodeCmp);
    enemy_bullet->addComponent(statusCmp);
    enemy_bullet->addComponent(soundCmp);
    ++_enemy_bullets;
    _gameObjectMgr->addGameObject(enemy_bullet);
}

void GameObjectCreator::createEnemy(int type, Ogre::Vector3 position) {
    switch (type) {
        case 1:
            position.x += 300;
            createEnemy1(position);
            break;
        case 2:
            position.x += 300;
            createEnemy2(position);
            break;
        case 3:
            position.x += 300;
            createEnemy3(position);
            break;
        case 4:
            position.x += 300;
            createEnemy4(position);
            break;
        case 5:
            position.x += 300;
            createFinalBoss1();
            break;
        case 6:
            position.x += 300;
            createFinalBoss2();
        default:
            break;
    }
}

void GameObjectCreator::createEnemyBullet (int type, Ogre::Vector3 position, Ogre::Vector3 direction) {
    if (!_cameraMgr) {
        _cameraMgr = CameraManager::getSingletonPtr();
    }
    if (direction == Ogre::Vector3::ZERO) {
        Ogre::SceneNode *player_node = _cameraMgr->getRandomPlayer ();
        if (player_node) {
            direction = player_node->getPosition();
        }
        else {
            std::cout << "No player" << std::endl;
        }
    }
    switch (type) {
        case WeaponComponent::BulletType::NORMAL:{
            createNormalEnemyBullet(position, direction, true);
        }break;
        case WeaponComponent::BulletType::SHOTGUN:{

            Ogre::Vector3 direction_up = direction;
            direction_up.y += 2;
            Ogre::Vector3 direction_down = direction;
            direction_down.y -= 2;
            Ogre::Vector3 direction_left = direction;
            direction_left.x -= 2;
            Ogre::Vector3 direction_right = direction;
            direction_right.x += 2;
            createNormalEnemyBullet(position, direction, true);
            createNormalEnemyBullet(position, direction_up, false);
            createNormalEnemyBullet(position, direction_down, false);
            createNormalEnemyBullet(position, direction_left, false);
            createNormalEnemyBullet(position, direction_right, false);
        }break;
        case WeaponComponent::BulletType::MISSILE:{
            createMissile(position);
        }break;
        case WeaponComponent::BulletType::WALL: {
            int free_position = rand() % int(MAX_WALL_BULLETS * MAX_WALL_BULLETS);
            while (free_position % 10 == 0) {
                free_position = rand() % int(MAX_WALL_BULLETS * MAX_WALL_BULLETS);
            }
            int free_position2 = free_position - 1;
            for (int i = 0; i < MAX_WALL_BULLETS; ++i) {
                for (int j = 0; j < MAX_WALL_BULLETS; ++j) {
                    if (i + j * MAX_WALL_BULLETS != free_position && i + j * MAX_WALL_BULLETS != free_position2) {
                        float x = - InputComponent::HORIZONTAL_LIMIT + 1 + ((InputComponent::HORIZONTAL_LIMIT - 1) / MAX_WALL_BULLETS * 2 * j);
                        float y = - InputComponent::VERTICAL_LIMIT + 1 + ((InputComponent::VERTICAL_LIMIT - 1) / MAX_WALL_BULLETS * 2 * i);
                        Ogre::Vector3 direction(x, y, -10);
                        createWallEnemyBullet(position, direction);
                    }
                }
            }
        }break;
        default:
            break;
    }
}

void GameObjectCreator::createFinalBoss1 () {
    std::shared_ptr<FinalBoss1> finalBoss1 = std::make_shared<FinalBoss1>();
    finalBoss1->initComponents();
    _gameObjectMgr->addGameObject(finalBoss1);
}

void GameObjectCreator::createFinalBoss2 () {
    std::shared_ptr<FinalBoss2> finalBoss2 = std::make_shared<FinalBoss2>();
    finalBoss2->initComponents();
    _gameObjectMgr->addGameObject(finalBoss2);
}

void GameObjectCreator::createPropellerBullet (Ogre::Vector3 position, Ogre::Vector3 direction) {
    if (!_sceneMgr) {
        _sceneMgr = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
    }
    std::shared_ptr<GameObject> bullet = std::make_shared<GameObject>();
    std::shared_ptr<PhysicsComponent> physicsCmp = std::make_shared<PhysicsComponent>(bullet, _sceneMgr->createSceneNode(std::string("PropellerBullet") + std::to_string(_propeller_bullet)), nullptr, std::string("PropellerBullet") + std::to_string(_propeller_bullet), PhysicsComponent::Type::ENEMY_BULLET, PhysicsComponent::Shape::SPHERE, true, position, Ogre::Vector3(3, 3, 3));
    physicsCmp->setLinearVelocity(Ogre::Vector3(direction));
    std::shared_ptr<StatusComponent> statusCmp = std::make_shared<StatusComponent>(bullet, 1, StatusComponent::Type::BULLET);
    physicsCmp->setStatusComponent(statusCmp);
    bullet->addComponent(physicsCmp);
    bullet->addComponent(statusCmp);
    _gameObjectMgr->addGameObject(bullet);
    ++_propeller_bullet;
}

void GameObjectCreator::createEnemy1(Ogre::Vector3 position) {
    //Single shot enemy, 6 life
    std::shared_ptr<GameObject> enemy = std::make_shared<GameObject>();
    std::shared_ptr<NodeComponent> nodeCmp = std::make_shared<NodeComponent>(enemy, "Enemy1", _enemy_1);
    std::shared_ptr<PhysicsComponent> physicsCmp = std::make_shared<PhysicsComponent>(enemy, nodeCmp->getNode(), nodeCmp->getEntity(), std::string("Enemy1") + std::to_string(_enemy_1), PhysicsComponent::Type::ENEMY, PhysicsComponent::Shape::CONVEX, true, position, Ogre::Vector3::ZERO);
    std::shared_ptr<RigidBodyAnimationComponent> rigidAnimationCmp = std::make_shared<RigidBodyAnimationComponent>(enemy, physicsCmp, nodeCmp->getEntity());
    std::shared_ptr<StatusComponent> statusCmp = std::make_shared<StatusComponent>(enemy, 6, StatusComponent::Type::ENEMY1);
    std::shared_ptr<WeaponComponent> weaponCmp = std::make_shared<WeaponComponent>(enemy, rigidAnimationCmp, 2, 4, WeaponComponent::BulletType::MISSILE);
    std::shared_ptr<SoundComponent> soundCmp = std::make_shared<SoundComponent>(enemy, SoundComponent::Type::ENEMY);
    physicsCmp->setStatusComponent(statusCmp);
    enemy->addComponent(rigidAnimationCmp);
    enemy->addComponent(weaponCmp);
    enemy->addComponent(physicsCmp);
    enemy->addComponent(nodeCmp);
    enemy->addComponent(statusCmp);
    enemy->addComponent(soundCmp);
    _gameObjectMgr->addGameObject(enemy);
    ++_enemy_1;
}

void GameObjectCreator::createEnemy2 (Ogre::Vector3 position) {
    // Burst shot enemy, 10 life
    std::shared_ptr<GameObject> enemy = std::make_shared<GameObject>();
    std::shared_ptr<NodeComponent> nodeCmp = std::make_shared<NodeComponent>(enemy, "Enemy2", _enemy_2);
    std::shared_ptr<PhysicsComponent> physicsCmp = std::make_shared<PhysicsComponent>(enemy, nodeCmp->getNode(), nodeCmp->getEntity(), std::string("Enemy2") + std::to_string(_enemy_2), PhysicsComponent::Type::ENEMY, PhysicsComponent::Shape::CONVEX, true, position, Ogre::Vector3::ZERO);
    std::shared_ptr<RigidBodyAnimationComponent> rigidAnimationCmp = std::make_shared<RigidBodyAnimationComponent>(enemy, physicsCmp, nodeCmp->getEntity());
    //TODO: change parameters of weapon and life
    std::shared_ptr<StatusComponent> statusCmp = std::make_shared<StatusComponent>(enemy, 10, StatusComponent::Type::ENEMY2);
    std::shared_ptr<BurstWeaponComponent> weaponCmp = std::make_shared<BurstWeaponComponent>(enemy, rigidAnimationCmp, 2, 4, 10, 0.1);
    std::shared_ptr<SoundComponent> soundCmp = std::make_shared<SoundComponent>(enemy, SoundComponent::Type::ENEMY);
    physicsCmp->setStatusComponent(statusCmp);
    enemy->addComponent(rigidAnimationCmp);
    enemy->addComponent(weaponCmp);
    enemy->addComponent(physicsCmp);
    enemy->addComponent(nodeCmp);
    enemy->addComponent(statusCmp);
    enemy->addComponent(soundCmp);
    _gameObjectMgr->addGameObject(enemy);

    ++_enemy_2;
}

void GameObjectCreator::createEnemy3 (Ogre::Vector3 position) {
    // Shotgun enemy, 9 life
    std::shared_ptr<GameObject> enemy = std::make_shared<GameObject>();
    std::shared_ptr<NodeComponent> nodeCmp = std::make_shared<NodeComponent>(enemy, "Enemy3", _enemy_3);
    std::shared_ptr<PhysicsComponent> physicsCmp = std::make_shared<PhysicsComponent>(enemy, nodeCmp->getNode(), nodeCmp->getEntity(), std::string("Enemy3") + std::to_string(_enemy_3), PhysicsComponent::Type::ENEMY, PhysicsComponent::Shape::CONVEX, true, position, Ogre::Vector3::ZERO);
    std::shared_ptr<RigidBodyAnimationComponent> rigidAnimationCmp = std::make_shared<RigidBodyAnimationComponent>(enemy, physicsCmp, nodeCmp->getEntity());
    //TODO: change parameters of weapon and life
    std::shared_ptr<StatusComponent> statusCmp = std::make_shared<StatusComponent>(enemy, 9, StatusComponent::Type::ENEMY3);
    std::shared_ptr<WeaponComponent> weaponCmp = std::make_shared<WeaponComponent>(enemy, rigidAnimationCmp, 3, 5, WeaponComponent::BulletType::SHOTGUN);
    std::shared_ptr<SoundComponent> soundCmp = std::make_shared<SoundComponent>(enemy, SoundComponent::Type::ENEMY);
    physicsCmp->setStatusComponent(statusCmp);
    enemy->addComponent(rigidAnimationCmp);
    enemy->addComponent(weaponCmp);
    enemy->addComponent(physicsCmp);
    enemy->addComponent(nodeCmp);
    enemy->addComponent(statusCmp);
    enemy->addComponent(soundCmp);
    _gameObjectMgr->addGameObject(enemy);

    ++_enemy_3;
}

void GameObjectCreator::createEnemy4 (Ogre::Vector3 position) {
    // Single shot enemy, 4 life
    std::shared_ptr<GameObject> enemy = std::make_shared<GameObject>();
    std::shared_ptr<NodeComponent> nodeCmp = std::make_shared<NodeComponent>(enemy, "Enemy4", _enemy_4);
    std::shared_ptr<PhysicsComponent> physicsCmp = std::make_shared<PhysicsComponent>(enemy, nodeCmp->getNode(), nodeCmp->getEntity(), std::string("Enemy4") + std::to_string(_enemy_4), PhysicsComponent::Type::ENEMY, PhysicsComponent::Shape::CONVEX, true, position, Ogre::Vector3::ZERO);
    std::shared_ptr<RigidBodyAnimationComponent> rigidAnimationCmp = std::make_shared<RigidBodyAnimationComponent>(enemy, physicsCmp, nodeCmp->getEntity());
    //TODO: change parameters of weapon and life
    std::shared_ptr<StatusComponent> statusCmp = std::make_shared<StatusComponent>(enemy, 5, StatusComponent::Type::ENEMY4);
    std::shared_ptr<WeaponComponent> weaponCmp = std::make_shared<WeaponComponent>(enemy, rigidAnimationCmp, 1, 2, WeaponComponent::BulletType::NORMAL);
    std::shared_ptr<SoundComponent> soundCmp = std::make_shared<SoundComponent>(enemy, SoundComponent::Type::ENEMY);
    physicsCmp->setStatusComponent(statusCmp);
    enemy->addComponent(rigidAnimationCmp);
    enemy->addComponent(weaponCmp);
    enemy->addComponent(physicsCmp);
    enemy->addComponent(nodeCmp);
    enemy->addComponent(statusCmp);
    enemy->addComponent(soundCmp);
    _gameObjectMgr->addGameObject(enemy);

    ++_enemy_4;
}

void GameObjectCreator::createPartsEnemy (Ogre::Vector3 position, int type) {
    for (int i = 0; i < NUMBER_ENEMY_PARTS; ++i) {
        std::shared_ptr<GameObject> part = std::make_shared<GameObject>();
        std::string part_name = std::string("Enemy") + std::to_string(type) + std::string("Part") + std::to_string(i + 1);
        std::shared_ptr<NodeComponent> nodeCmp = std::make_shared<NodeComponent>(part, part_name, _part);
        std::shared_ptr<PhysicsComponent> physicsCmp = std::make_shared<PhysicsComponent>(part, nodeCmp->getNode(), nodeCmp->getEntity(), part_name + std::to_string(_part), PhysicsComponent::Type::ENEMY_PART, PhysicsComponent::Shape::STATIC_CONVEX, false, position);
        std::shared_ptr<SoundComponent> soundCmp = std::make_shared<SoundComponent>(part, SoundComponent::Type::PART);
        physicsCmp->setGravity(Ogre::Vector3(0, -9.8, 0));
        physicsCmp->setLinearVelocity((Ogre::Vector3(rand() % 10 - 5, rand() % 10 , rand() % 10 - 5).normalisedCopy()) * 10);
        physicsCmp->setAngularVelocity((Ogre::Vector3(rand() % 10 - 5, rand() % 10 - 5, rand() % 10 - 5).normalisedCopy()) * 3);
        //std::shared_ptr<ParticlesComponent> partCmp = std::make_shared<ParticlesComponent>(part, position, ParticlesComponent::Type::ENEMY_PART, nodeCmp->getNode());
        part->addComponent(physicsCmp);
        part->addComponent(nodeCmp);
        part->addComponent(soundCmp);
        //part->addComponent(partCmp);
        _gameObjectMgr->addGameObject(part);
        ++_part;
    }
}

void GameObjectCreator::createPartsPlayer (Ogre::Vector3 position, int type) {
    for (int i = 1; i < NUMBER_PLAYER_PARTS; ++i) {
        std::shared_ptr<GameObject> part = std::make_shared<GameObject>();
        std::string part_name = std::string("Player") + std::to_string(type) + std::string("Part") + std::to_string(i + 1);
        std::shared_ptr<NodeComponent> nodeCmp = std::make_shared<NodeComponent>(part, part_name, _part);
        std::shared_ptr<PhysicsComponent> physicsCmp = std::make_shared<PhysicsComponent>(part, nodeCmp->getNode(), nodeCmp->getEntity(), part_name + std::to_string(_part), PhysicsComponent::Type::ENEMY_PART, PhysicsComponent::Shape::STATIC_CONVEX, false, position);
        std::shared_ptr<SoundComponent> soundCmp = std::make_shared<SoundComponent>(part, SoundComponent::Type::PART);
        physicsCmp->setGravity(Ogre::Vector3(0, -9.8, 0));
        physicsCmp->setLinearVelocity((Ogre::Vector3(rand() % 10 - 5, rand() % 10 , rand() % 10 - 5).normalisedCopy()) * 10);
        physicsCmp->setAngularVelocity((Ogre::Vector3(rand() % 10 - 5, rand() % 10 - 5, rand() % 10 - 5).normalisedCopy()) * 3);
        //std::shared_ptr<ParticlesComponent> partCmp = std::make_shared<ParticlesComponent>(part, position, ParticlesComponent::Type::ENEMY_PART, nodeCmp->getNode());
        part->addComponent(physicsCmp);
        part->addComponent(nodeCmp);
        part->addComponent(soundCmp);
        //part->addComponent(partCmp);
        _gameObjectMgr->addGameObject(part);
        ++_part;
    }
}

void GameObjectCreator::createMissile (Ogre::Vector3 position) {
    std::shared_ptr<GameObject> enemy = std::make_shared<GameObject>();
    std::shared_ptr<NodeComponent> nodeCmp = std::make_shared<NodeComponent>(enemy, "Missile", _enemy_missile);
    std::shared_ptr<PhysicsComponent> physicsCmp = std::make_shared<PhysicsComponent>(enemy, nodeCmp->getNode(), nodeCmp->getEntity(), std::string("Missile") + std::to_string(_enemy_missile), PhysicsComponent::Type::ENEMY, PhysicsComponent::Shape::STATIC_CONVEX, true, position, Ogre::Vector3::ZERO);
    std::shared_ptr<AIMissileComponent> aiCmp = std::make_shared<AIMissileComponent>(enemy, physicsCmp);
    std::shared_ptr<StatusComponent> statusCmp = std::make_shared<StatusComponent>(enemy, 1, StatusComponent::Type::ENEMYMISSILE);
    std::shared_ptr<ParticlesComponent> partCmp = std::make_shared<ParticlesComponent>(enemy, position, ParticlesComponent::Type::MISSILE, nodeCmp->getNode(), Ogre::Vector3(0, 0, -0.55), Ogre::Vector3(0, 1, 0));
    std::shared_ptr<SoundComponent> soundCmp = std::make_shared<SoundComponent>(enemy, SoundComponent::Type::MISSILE);
    physicsCmp->setStatusComponent(statusCmp);
    enemy->addComponent(aiCmp);
    enemy->addComponent(physicsCmp);
    enemy->addComponent(nodeCmp);
    enemy->addComponent(statusCmp);
    enemy->addComponent(partCmp);
    enemy->addComponent(soundCmp);
    _gameObjectMgr->addGameObject(enemy);

    ++_enemy_missile;
}

const float GameObjectCreator::MAX_WALL_BULLETS = 10;
const float GameObjectCreator::ENEMY_BULLET_VELOCITY = 15;
