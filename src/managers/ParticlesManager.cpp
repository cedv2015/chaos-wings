#include "ParticlesManager.h"
#include "ParticlesComponent.h"

template <> ParticlesManager* Ogre::Singleton<ParticlesManager>::msSingleton = nullptr;

ParticlesManager::ParticlesManager () : _id_counter(0) {
}

ParticlesManager::~ParticlesManager () {
    _particles.clear();
}

void ParticlesManager::update (float delta) {
    int size = _particles.size();
    for (int i = 0; i < size; ++i) {
        _particles[i]->update(delta);
        if (_particles[i]->isFinished()) {
            _particles.erase(_particles.begin() + i);
            --i; --size;
        }
    }
}

std::shared_ptr<MyParticleSystem> ParticlesManager::createParticleSystem(Ogre::Vector3 position, int type) {
    std::string name;
    switch (type) {
        case ParticlesComponent::Type::SPEEDPHASE1:
            name = std::string("Speed_phase_1");
            break;
        case ParticlesComponent::Type::SPEEDPHASE2:
            name = std::string("Speed_phase_2");
            break;
        case ParticlesComponent::Type::WATERBULLET:
            name = std::string("WaterBullet");
            break;
        case ParticlesComponent::Type::SNOWBULLET:
            name = std::string("SnowBullet");
            break;
        case ParticlesComponent::Type::PLAYERPROPULSION:
            name = std::string("PlayerPropeller");
            break;
        case ParticlesComponent::Type::PLAYERPROPULSION2:
            name = std::string("PlayerPropeller2");
            break;
        case ParticlesComponent::Type::WINGS_FRICTION:
            name = std::string("Wings_friction");
            break;
        case ParticlesComponent::Type::PLAYER_LASER:
            name = std::string("Laser");
            break;
        case ParticlesComponent::Type::BULLET_COLLISION:
            name = std::string("Bullet_collision");
            break;
        case ParticlesComponent::Type::MISSILE:
            name = std::string("Missile");
            break;
        case ParticlesComponent::Type::ENEMY_PART:
            name = std::string("EnemyPart");
            break;
        case ParticlesComponent::Type::FINAL_BOSS_PROPELLER_NORMAL:
            name = std::string("Final_boss_propeller_normal");
            break;
        case ParticlesComponent::Type::FINAL_BOSS_PROPELLER_CHARGING:
            name = std::string("Final_boss_propeller_charging");
            break;
        case ParticlesComponent::Type::FINAL_BOSS_PROPELLER_SHOOTING:
            name = std::string("Final_boss_propeller_shooting");
            break;
        case ParticlesComponent::Type::FINAL_BOSS_1_EXPLOSION:
            name = std::string("Boss_1_explosion");
            break;
        case ParticlesComponent::Type::ENEMY_DEATH:
            name = std::string("Enemy_explosion");
            break;
        case ParticlesComponent::Type::FINAL_BOSS_2_RISING:
            name = std::string("Boss_2_rising");
            break;
        case ParticlesComponent::Type::FINAL_BOSS_2_SPEED:
            name = std::string("Boss_2_speed");
            break;
        case ParticlesComponent::Type::FINAL_BOSS_2_COLLISION_WATER1:
            name = std::string("Boss_2_collision_water1");
            break;
        case ParticlesComponent::Type::FINAL_BOSS_2_COLLISION_WATER2:
            name = std::string("Boss_2_collision_water2");
            break;
        case ParticlesComponent::Type::FINAL_BOSS_2_EXPLOSION:
            name = std::string("Boss_2_explosion");
            break;
        /*case Particles::GRENADEEXPLOSION:
            name = std::string("Grenade");
            break;
            */
        default:
            break;
    }
    std::shared_ptr<MyParticleSystem> particle = std::make_shared<MyParticleSystem>(position, name);
    _particles.push_back(particle);
    return particle;
}

ParticlesManager* ParticlesManager::getSingletonPtr () {
    return msSingleton;
}

ParticlesManager& ParticlesManager::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

void ParticlesManager::destroyAllParticles() {
    _particles.clear();
}
