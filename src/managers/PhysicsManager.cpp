#include "PhysicsManager.h"
#include "PhysicsComponent.h"

template <> PhysicsManager* Ogre::Singleton<PhysicsManager>::msSingleton = nullptr;

PhysicsManager::PhysicsManager () : _root(nullptr), _sceneMgr(nullptr), _world(nullptr), _debugDrawer(nullptr), _debug(false) {
    _root = Ogre::Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");

    // Creacion del modulo de debug visual de Bullet
    _debugDrawer = new OgreBulletCollisions::DebugDrawer();
    _debugDrawer->setDrawWireframe(true);
    Ogre::SceneNode *debugNode = _sceneMgr->getRootSceneNode()->createChildSceneNode("debugNode", Ogre::Vector3::ZERO);
    debugNode->attachObject(static_cast <Ogre::SimpleRenderable *>(_debugDrawer));

    // Creacion del mundo (definicion de limites y gravedad)
    Ogre::AxisAlignedBox worldBounds = Ogre::AxisAlignedBox (
        Ogre::Vector3(-10000, -10000,-10000),
        Ogre::Vector3(10000, 10000, 10000));
    Ogre::Vector3 gravity = Ogre::Vector3(0, 0, 0);

    _world = new OgreBulletDynamics::DynamicsWorld(_sceneMgr, worldBounds, gravity);
    _world->setDebugDrawer(_debugDrawer);
    _world->setShowDebugShapes(_debug);
}

PhysicsManager::~PhysicsManager () {
    if (_debugDrawer) {
        delete _debugDrawer;
        _world->setDebugDrawer(nullptr);
    }

    if(_world) {
        delete _world;
    }
}

PhysicsManager* PhysicsManager::getSingletonPtr () {
    return msSingleton;
}

PhysicsManager& PhysicsManager::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}

void PhysicsManager::update(float delta) {
    _world->stepSimulation(delta);
    checkCollisions();
}

void PhysicsManager::switchDebug () {
    _debug = !_debug;
    _world->setShowDebugShapes(_debug);
}

void PhysicsManager::checkCollisions() {
    btCollisionWorld *bulletWorld = _world->getBulletCollisionWorld();
    int numManifolds = bulletWorld->getDispatcher()->getNumManifolds();
    for (int i = 0; i < numManifolds; i++) {
        btPersistentManifold *contactManifold = bulletWorld->getDispatcher()->getManifoldByIndexInternal(i);
        int numContacts = contactManifold->getNumContacts();
        if (numContacts > 0) {
            btCollisionObject *obA = (btCollisionObject*)(contactManifold->getBody0());
            btCollisionObject *obB = (btCollisionObject*)(contactManifold->getBody1());
            PhysicsComponent *cmpA = nullptr;
            PhysicsComponent *cmpB = nullptr;
            void *userPtrA = obA->getUserPointer();
            void *userPtrB = obB->getUserPointer();
            if (userPtrA) {
                cmpA = static_cast<PhysicsComponent*>(userPtrA);
            }

            if (userPtrB) {
                cmpB = static_cast<PhysicsComponent*>(userPtrB);
            }

            if (cmpA) {
                cmpA->onCollision(cmpB);
            }

            if (cmpB) {
                cmpB->onCollision(cmpA);
            }
        }
    }
}
