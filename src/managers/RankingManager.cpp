#include "RankingManager.h"
#include "StatusComponent.h"
#include "OsUtils.h"

template <> RankingManager* Ogre::Singleton<RankingManager>::msSingleton = nullptr;

RankingManager* RankingManager::getSingletonPtr () {
    return msSingleton;
}

RankingManager& RankingManager::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}

std::vector<std::string> RankingManager::getRanking () {
// checks if the vector contains something, if not it calls "loadRankings"
    if (_rankings.size() == 0) {
        this->loadRankings();
    }
    return _rankings;
}

bool RankingManager::checkRanking () {
// checks if the score is better than the actual 10th position
    if (_rankings.size() == 0) {
        this->loadRankings();
    }
    if (_score > std::stoi(_rankings[5])) {
        return true;
    }
    return false;
}

void RankingManager::setRanking (std::string name) {
// changes the position one by one from the last to the position of the actual score
    if (_rankings.size() == 0) {
        this->loadRankings();
    }
    if (!this->checkRanking()) {
        return;
    }
    int position = 4;
    int i;
    for (i = 3; i > 0; i--) {
        if (_score < stoi(_rankings[i * 2 - 1])) {
            break;
        }
        position--;
    }
    for (i = 3; i > position; i--) {
        _rankings[i * 2 - 1] = _rankings[i * 2 - 3];
        _rankings[i * 2 - 2] = _rankings[i * 2 - 4];
    }
    _rankings[position * 2 - 2] = name;
    _rankings[position * 2 - 1] = std::to_string(_score);
    this->saveRankings ();
    std::cout << "Ranking saved" << std::endl;
}

void RankingManager::saveRankings () {
// evens contain the name of the player
// odds contain the number of score
    std::string separator = "";
    int os = OsUtils::getOs();
    if (os == OsUtils::OS::LINUX || os == OsUtils::OS::MAC) {
        separator = "/";
    }
    else if (os == OsUtils::OS::WINDOWS) {
        separator = "\\";
    }
    std::string filename = std::string("misc") + separator + std::string("ranking.txt");
    std::ofstream file (filename);
    if (file.is_open()) {
        for (int i = 0; i<6; i++) {
            file << _rankings[i] << std::endl;
        }
        file.close();
    }
}

void RankingManager::loadRankings () {
// evens contain the name of the player
// odds contain the number of score
    std::string separator = "";
    int os = OsUtils::getOs();
    if (os == OsUtils::OS::LINUX || os == OsUtils::OS::MAC) {
        separator = "/";
    }
    else if (os == OsUtils::OS::WINDOWS) {
        separator = "\\";
    }
    std::string filename = std::string("misc") + separator + std::string("ranking.txt");
    std::string line;
    std::ifstream file (filename);
    if (file.is_open()) {
        while (getline(file, line)) {
            _rankings.push_back(line);
        }
        file.close();
    }
}


void RankingManager::addScore(int type){
    switch (type) {
      case StatusComponent::Type::ENEMY1:
          _score+=SCORE_ENEMY1;
          break;
      case StatusComponent::Type::ENEMY2:
          _score+=SCORE_ENEMY2;
          break;
      case StatusComponent::Type::ENEMY3:
          _score+=SCORE_ENEMY3;
          break;
      case StatusComponent::Type::ENEMY4:
          _score+=SCORE_ENEMY4;
          break;
      case StatusComponent::Type::ENEMYMISSILE:
          _score+=SCORE_ENEMY_MISSILE;
          break;
      case StatusComponent::Type::FINALBOSS1:
          _score+=SCORE_FINAL_BOSS1;
          break;
      case StatusComponent::Type::FINALBOSS2:
          _score+=SCORE_FINAL_BOSS2;
          break;
      default:
          break;
    }
}
