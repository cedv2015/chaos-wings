#include <TrackManager.h>

template<> TrackManager* Ogre::Singleton<TrackManager>::msSingleton = 0;

TrackManager::TrackManager() : _time_fade(0), _last_fade(0) {
  // Tipo de recurso.
  mResourceType = "Track";
  // Prioridad.
  mLoadOrder = 30.f;
  // Registro del gestor en el sistema.
  Ogre::ResourceGroupManager::getSingleton()._registerResourceManager(mResourceType, this);
  _actual_song.setNull();
  _target_song.setNull();
}

TrackManager::~TrackManager() {
  // Eliminación del registro.
  Ogre::ResourceGroupManager::getSingleton()._unregisterResourceManager(mResourceType);
}

void TrackManager::playTrack (const Ogre::String& name) {
    if (!_actual_song.isNull()) {
        if (_actual_song->isPlaying()) {
            _actual_song->stop();
        }
        _actual_song.setNull();
        _target_song.setNull();
    }
    _actual_song = load(name);
    _actual_song->play(-1);
}

TrackPtr TrackManager::load(const Ogre::String& name, const Ogre::String& group) {
  // Crea o recupera el recurso
  TrackPtr trackPtr = createOrRetrieve(name, group, false, 0, 0).first.staticCast<Track>();

  // Carga explícita del recurso.
  trackPtr->load();

  return trackPtr;
}

TrackManager& TrackManager::getSingleton() {
  assert(msSingleton);
  return (*msSingleton);
}

TrackManager* TrackManager::getSingletonPtr() {
  assert(msSingleton);
  return msSingleton;
}

void TrackManager::fadeToSong (const Ogre::String &name, float seg) {
    return;
    if (_actual_song.isNull()) {
        _actual_song = load(name);
        _actual_song->play(-1);
    }
    else {
        _target_song = load(name);
        _time_fade = seg;
        _actual_song->fadeOut(int((seg) * 1000));
        _last_fade = 0;
    }
}

void TrackManager::update (float delta) {
    _last_fade += delta;
    if (!_target_song.isNull()) {
        if (_last_fade > _time_fade + 0.8) {
            _actual_song->stop();
            _actual_song = _target_song;
            _actual_song->play(-1);
            _target_song.setNull();
        }
    }
}

void TrackManager::setHighVolume () {
    if (!_actual_song.isNull()) {
        _actual_song->setHighVolume();
    }
}

void TrackManager::setLowVolume () {
    if (!_actual_song.isNull()) {
        _actual_song->setLowVolume();
    }
}

// Creación de un nuevo recurso.
// No se llama a load().
Ogre::Resource* TrackManager::createImpl(const Ogre::String& resource_name,
					 Ogre::ResourceHandle handle,
					 const Ogre::String& resource_group,
					 bool isManual,
					 Ogre::ManualResourceLoader* loader,
					 const Ogre::NameValuePairList* createParams) {
  return new Track(this, resource_name, handle, resource_group, isManual, loader);
}
