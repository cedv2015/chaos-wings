#include "CreditsState.h"
#include "OsUtils.h"

template <> CreditsState* Ogre::Singleton<CreditsState>::msSingleton = nullptr;

CreditsState::CreditsState () :  _root(nullptr), _sceneMgr(nullptr), _camera(nullptr), _credits(nullptr), _keyboard(nullptr), _mouse(nullptr) ,_exit(false){}

CreditsState& CreditsState::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}
CreditsState* CreditsState::getSingletonPtr () {
    return msSingleton;
}

void CreditsState::enter () {
    std::cout << "Entered in CreditsState" << std::endl;
    _root = Ogre::Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");
  	_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().show( );

    if(_credits == NULL){
        _credits = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("credits.layout");
        _credits->getChild("ButtonBack")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&CreditsState::menu, this));
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_credits);
        setupAnimations();
    }else
        _credits->show();
    loadCredits();

}

void CreditsState::exit () {
    _credits->hide();
}
void CreditsState::pause() {}
void CreditsState::resume() {}

void CreditsState::keyPressed (const OIS::KeyEvent &e) {}
void CreditsState::keyReleased (const OIS::KeyEvent &e) {
    switch (e.key) {
        case OIS::KC_ESCAPE:
            _exit = true;
            break;
        default:
            break;
    }
}

CEGUI::MouseButton CreditsState::convertMouseButton(OIS::MouseButtonID id)
{
    CEGUI::MouseButton ceguiId;
    switch(id)
    {
        case OIS::MB_Left:
            ceguiId = CEGUI::LeftButton;
            break;
        case OIS::MB_Right:
            ceguiId = CEGUI::RightButton;
            break;
        case OIS::MB_Middle:
            ceguiId = CEGUI::MiddleButton;
            break;
        default:
            ceguiId = CEGUI::LeftButton;
    }
    return ceguiId;
}

void CreditsState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(e.state.X.abs, e.state.Y.abs);
}
void CreditsState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}
void CreditsState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

bool CreditsState::frameStarted (const Ogre::FrameEvent &e) {
    CEGUI::System::getSingleton().injectTimePulse(e.timeSinceLastFrame);
    return !_exit;
}
bool CreditsState::frameEnded (const Ogre::FrameEvent &e) {
    return !_exit;
}


bool CreditsState::povMoved(const OIS::JoyStickEvent &e, int pov) {return true;}
bool CreditsState::axisMoved(const OIS::JoyStickEvent &e, int axis) {return true;}
bool CreditsState::sliderMoved(const OIS::JoyStickEvent &e, int sliderID) {return true;}
bool CreditsState::buttonPressed(const OIS::JoyStickEvent &e, int button) {return true;}
bool CreditsState::buttonReleased(const OIS::JoyStickEvent &e, int button) {return true;}

bool CreditsState::menu(const CEGUI::EventArgs &e)
{
    this->popState();
    _credits->hide();
    changeState(MenuState::getSingletonPtr());
    return true;
}

void CreditsState::loadCredits() {

    _data.clear();
    std::string separator = "";
    int os = OsUtils::getOs();
    if (os == OsUtils::OS::LINUX || os == OsUtils::OS::MAC) {
        separator = "/";
    }
    else if (os == OsUtils::OS::WINDOWS) {
        separator = "\\";
    }
    std::string file_name = std::string("misc") + separator + std::string("credits.txt");

    std::ifstream file(file_name);
    std::string line;
    if (file.is_open()) {
        std::string item;
        while (getline(file, line)) {
            std::stringstream iss;
            iss << line;
            while (std::getline (iss, item, '\n')) {
                _data.push_back(item);
            }
        }
        file.close();
    }
    CEGUI::String finalText ="";
    for(int i=0; (unsigned)i<_data.size(); i++){

          finalText += _data[i]+"\n";
    }
    _credits->getChild("creditsLabel")->setText(finalText);
    _creditsAnimInst->start();

}

void CreditsState::setupAnimations(){

  CEGUI::AnimationManager& animMgr = CEGUI::AnimationManager::getSingleton();

  CEGUI::Animation* creditsAnim = animMgr.getAnimation("CreditsAnimation");
  _creditsAnimInst = animMgr.instantiateAnimation(creditsAnim);
  _creditsAnimInst->setTargetWindow(_credits->getChild("creditsLabel"));
}
