#include "DialogueState.h"
#include "PlayState.h"
#include "SoundFXManager.h"
#include "GameObjectManager.h"

template<> DialogueState* Ogre::Singleton<DialogueState>::msSingleton = nullptr;


DialogueState::DialogueState () : _root(nullptr), _sceneMgr(nullptr), _camera(nullptr), _node(nullptr), _entity(nullptr), _exit(false), _phase(0), _dialogue(0), _clic(0), _dialogueMgr(nullptr) {
    _dialogueMgr = DialogueManager::getSingletonPtr();
}

DialogueState::~DialogueState () {

}

DialogueState* DialogueState::getSingletonPtr () {
    return msSingleton;
}

DialogueState& DialogueState::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}

void DialogueState::enter () {
    SoundFXManager::getSingletonPtr()->load("alert.wav")->play(0);
    std::cout << "Entered in DialogueState" << std::endl;
    _root = Ogre::Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");
    _dialogueMgr->newIntervention(_phase,_dialogue);

    GameObjectManager::getSingletonPtr()->hideHUD();

    PlayState::getSingletonPtr()->visibilityHUDPhase(false);
    _trackMgr = TrackManager::getSingletonPtr();
}

void DialogueState::exit () {
    GameObjectManager::getSingletonPtr()->showHUD();
    PlayState::getSingletonPtr()->finishDialogue();
    PlayState::getSingletonPtr()->visibilityHUD(true);
}

void DialogueState::pause () {

}

void DialogueState::resume () {

}

void DialogueState::keyPressed (const OIS::KeyEvent &e) {
    switch (e.key) {
        default:
            break;
    }
}

void DialogueState::keyReleased (const OIS::KeyEvent &e) {
    switch (e.key) {
        case OIS::KC_ESCAPE:
            _exit = true;
            break;
        default:
            break;
    }
}

void DialogueState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(e.state.X.abs, e.state.Y.abs);
}

void DialogueState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    return;
};
void DialogueState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    switch (id) {
      case OIS::MB_Left:
          _clic++;
          if((_clic%2)==0)
              _dialogueMgr->chargeDialogue();
          else
              _dialogueMgr->passMessage();
          break;
      default:
          break;
    }
};

bool DialogueState::povMoved(const OIS::JoyStickEvent &e, int pov) {return true;}
bool DialogueState::axisMoved(const OIS::JoyStickEvent &e, int axis) {return true;}
bool DialogueState::sliderMoved(const OIS::JoyStickEvent &e, int sliderID) {return true;}
bool DialogueState::buttonPressed(const OIS::JoyStickEvent &e, int button) {
  switch (button) {
      case JOYSTICK_BUTTON_1:
          _clic++;
          if((_clic%2)==0)
              _dialogueMgr->chargeDialogue();
          else
              _dialogueMgr->passMessage();
          break;
      default:
          break;
  }
  return true;
}
bool DialogueState::buttonReleased(const OIS::JoyStickEvent &e, int button) {return true;}

bool DialogueState::frameStarted (const Ogre::FrameEvent &evt) {
    Ogre::Real delta = evt.timeSinceLastFrame;
    CEGUI::System::getSingleton().injectTimePulse(evt.timeSinceLastFrame);
    _dialogueMgr->update(delta);
    _trackMgr->update(delta);

    return !_exit;
};
bool DialogueState::frameEnded (const Ogre::FrameEvent &evt) {
    return !_exit;
};
