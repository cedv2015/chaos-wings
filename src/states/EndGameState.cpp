#include "EndGameState.h"
#include "GameObjectManager.h"
#include "DialogueManager.h"

template <> EndGameState* Ogre::Singleton<EndGameState>::msSingleton = nullptr;

EndGameState::EndGameState () : _root(nullptr), _sceneMgr(nullptr), _camera(nullptr), _keyboard(nullptr), _mouse(nullptr), _exit(false), _eGame(false){
    _rankingMgr = RankingManager::getSingletonPtr();
}

EndGameState* EndGameState::getSingletonPtr () {
    return msSingleton;
}

EndGameState& EndGameState::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}

void EndGameState::enter () {

    std::cout << "Entered in EndGameState" << std::endl;
    _root = Ogre::Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");

    _physicsMgr = PhysicsManager::getSingletonPtr();
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().show( );
    d_timeSinceStart = 0.0;
    if(_rankingMgr->checkRanking()){
        addRanking();
        _eGame = false;
    }else{
        endGame();
        _eGame = true;
    }

}

void EndGameState::exit() {
    if(_endGame)
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->removeChild(_endGame);
}

void EndGameState::pause () {}

void EndGameState::resume () {}

bool EndGameState::frameStarted (const Ogre::FrameEvent &evt) {
    Ogre::Real delta = evt.timeSinceLastFrame;
    CEGUI::System::getSingleton().injectTimePulse(delta);
    _physicsMgr->update(delta);
    GameObjectManager::getSingletonPtr()->update(delta);
    d_timeSinceStart += delta;
    if(_eGame)
        updateText(delta);
    return !_exit;
}


bool EndGameState::frameEnded (const Ogre::FrameEvent &evt) {
    return !_exit;
}

void EndGameState::keyPressed (const OIS::KeyEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyDown(static_cast<CEGUI::Key::Scan>(e.key));
    CEGUI::System::getSingleton().getDefaultGUIContext().injectChar(e.text);
}

void EndGameState::keyReleased (const OIS::KeyEvent &e) {
    switch (e.key) {
        case OIS::KC_ESCAPE:
            _exit = true;
            break;
        default:
            break;
    }
}

void EndGameState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(e.state.X.abs, e.state.Y.abs);
}
void EndGameState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}
void EndGameState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}


CEGUI::MouseButton EndGameState::convertMouseButton(OIS::MouseButtonID id)
{
    CEGUI::MouseButton ceguiId;
    switch(id)
    {
        case OIS::MB_Left:
            ceguiId = CEGUI::LeftButton;
            break;
        case OIS::MB_Right:
            ceguiId = CEGUI::RightButton;
            break;
        case OIS::MB_Middle:
            ceguiId = CEGUI::MiddleButton;
            break;
        default:
            ceguiId = CEGUI::LeftButton;
    }
    return ceguiId;
}

bool EndGameState::povMoved(const OIS::JoyStickEvent &e, int pov) {return true;}
bool EndGameState::axisMoved(const OIS::JoyStickEvent &e, int axis) {return true;}
bool EndGameState::sliderMoved(const OIS::JoyStickEvent &e, int sliderID) {return true;}
bool EndGameState::buttonPressed(const OIS::JoyStickEvent &e, int button) {return true;}
bool EndGameState::buttonReleased(const OIS::JoyStickEvent &e, int button) {return true;}

bool EndGameState::close(const CEGUI::EventArgs &e)
{
    this->popState();
    _endGame->hide();
    PlayState::getSingletonPtr()->setMenu(true);
    return true;

}

void EndGameState::updateText(float delta){

    CEGUI::String firstPart = "MISSION COMPLETED";
    CEGUI::String secondPart = "You Win";


    CEGUI::String finalText;

    int firstPartTypeProgress = static_cast<int>((d_timeSinceStart - 1.5) / 0.1f);
    if(firstPartTypeProgress > 0)
        finalText += firstPart.substr(0, std::min<unsigned int>(firstPart.length(), firstPartTypeProgress));

    int secondPartTypeProgress = static_cast<int>((d_timeSinceStart - 3.0) / 0.12f);
    if(secondPartTypeProgress > 0)
        finalText += "\n" + secondPart.substr(0, std::min<unsigned int>(secondPart.length(), secondPartTypeProgress));

        finalText += "[font='Jura-50']";

        static double blinkStartDelay = 3.6f;
        double blinkPeriod = 0.8;
        double blinkTime = std::modf(static_cast<double>(d_timeSinceStart), &blinkPeriod);
        if(blinkTime > 0.55 || d_timeSinceStart < blinkStartDelay)
            finalText += "[colour='00000000']";

        finalText += (".");
      _endGame->getChild("EndGameLabel")->setText(finalText);
}

void EndGameState::setupAnimations(){

  CEGUI::AnimationManager& animMgr = CEGUI::AnimationManager::getSingleton();

  CEGUI::Animation* backgroundAnim = animMgr.getAnimation("BackgroundAlpha");
  _backgroundAnimInst = animMgr.instantiateAnimation(backgroundAnim);
  _backgroundAnimInst->setTargetWindow(_endGame->getChild("Resume"));
}

void EndGameState::addRanking(){
    _endGame = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("addRanking.layout");
    CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_endGame);
    _endGame->getChild("Panel/Button")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&EndGameState::accept, this));
}

bool EndGameState::accept(const CEGUI::EventArgs &e)
{
    CEGUI::Window* name_record = _endGame->getChild("Panel/NewName");
    std::string name = name_record->getText().c_str();
    name_record->setText("");
    _rankingMgr->setRanking(name);
    _endGame->hide();
    CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->removeChild(_endGame);
    _eGame = true;
    endGame();
    return true;

}

void EndGameState::endGame(){
    _endGame = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("endGame.layout");
    CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_endGame);
    _endGame->getChild("ButtonBack")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&EndGameState::close, this));
    setupAnimations();
    _backgroundAnimInst->start();
    _endGame->getChild("ScoreLabel")->setText("Score: "+std::to_string(_rankingMgr->getScore()));
}
