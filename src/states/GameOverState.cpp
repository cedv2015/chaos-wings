#include "GameOverState.h"
#include "GameObjectManager.h"
#include "DialogueManager.h"

#define SENSITIVITY 0.2

template <> GameOverState* Ogre::Singleton<GameOverState>::msSingleton = nullptr;

GameOverState::GameOverState () : _root(nullptr), _sceneMgr(nullptr), _camera(nullptr), _keyboard(nullptr), _mouse(nullptr), _exit(false), _gOver(false), _physicsMgr(nullptr), _rankingMgr(nullptr), _ranking(nullptr), _exitButton(nullptr), _backgroundAnimInst(nullptr), d_timeSinceStart(0.0){
    _rankingMgr = RankingManager::getSingletonPtr();
}

GameOverState* GameOverState::getSingletonPtr () {
    return msSingleton;
}

GameOverState& GameOverState::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}

void GameOverState::enter () {

    std::cout << "Entered in GameOverState" << std::endl;
    _root = Ogre::Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");

    _physicsMgr = PhysicsManager::getSingletonPtr();
    DialogueManager::getSingletonPtr()->hide();

    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().show( );
    d_timeSinceStart = 0.0;

    if(_rankingMgr->checkRanking()){
        addRanking();
        _gOver = false;
    }else{
        gameOver();
        _gOver = true;
    }

}

void GameOverState::exit() {
    if(_ranking)
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->removeChild(_ranking);
}

void GameOverState::pause () {}

void GameOverState::resume () {}

bool GameOverState::frameStarted (const Ogre::FrameEvent &evt) {
    Ogre::Real delta = evt.timeSinceLastFrame;
    CEGUI::System::getSingleton().injectTimePulse(delta);
    _physicsMgr->update(delta);
    GameObjectManager::getSingletonPtr()->update(delta);
    d_timeSinceStart += delta;
    if(_gOver)
        updateText(delta);
    return !_exit;
}


bool GameOverState::frameEnded (const Ogre::FrameEvent &evt) {
    return !_exit;
}

void GameOverState::keyPressed(const OIS::KeyEvent& evt)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyDown(static_cast<CEGUI::Key::Scan>(evt.key));
  CEGUI::System::getSingleton().getDefaultGUIContext().injectChar(evt.text);

}

void GameOverState::keyReleased(const OIS::KeyEvent& evt)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyUp(static_cast<CEGUI::Key::Scan>(evt.key));

}

void GameOverState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(e.state.X.abs, e.state.Y.abs);
}
void GameOverState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}
void GameOverState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}


CEGUI::MouseButton GameOverState::convertMouseButton(OIS::MouseButtonID id)
{
    CEGUI::MouseButton ceguiId;
    switch(id)
    {
        case OIS::MB_Left:
            ceguiId = CEGUI::LeftButton;
            break;
        case OIS::MB_Right:
            ceguiId = CEGUI::RightButton;
            break;
        case OIS::MB_Middle:
            ceguiId = CEGUI::MiddleButton;
            break;
        default:
            ceguiId = CEGUI::LeftButton;
    }
    return ceguiId;
}

void GameOverState::addRanking(){
    _ranking = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("addRanking.layout");
    CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_ranking);
    _ranking->getChild("Panel/Button")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&GameOverState::accept, this));
}

bool GameOverState::hide(const CEGUI::EventArgs &e)
{
    _ranking->hide();
    this->popState();
    changeState(MenuState::getSingletonPtr());
    TrackManager::getSingletonPtr()->fadeToSong("Menu_song.wav");
    return true;

}

bool GameOverState::accept(const CEGUI::EventArgs &e)
{
    CEGUI::Window* name_record = _ranking->getChild("Panel/NewName");
    std::string name = name_record->getText().c_str();
    name_record->setText("");
    _rankingMgr->setRanking(name);
    _ranking->hide();
    this->popState();
    changeState(MenuState::getSingletonPtr());
    TrackManager::getSingletonPtr()->fadeToSong("Menu_song.wav");
    return true;

}
void GameOverState::gameOver(){
    _ranking = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("gameover.layout");
    CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_ranking);
    _ranking->getChild("ButtonBack")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&GameOverState::close, this));
    setupAnimations();

    _backgroundAnimInst->start();
    //_ranking->getChild("ScoreLabel")->setText("Score: "+std::to_string(_rankingMgr->getScore()));
}
void GameOverState::setupAnimations(){

  CEGUI::AnimationManager& animMgr = CEGUI::AnimationManager::getSingleton();

  CEGUI::Animation* backgroundAnim = animMgr.getAnimation("BackgroundAlpha");
  _backgroundAnimInst = animMgr.instantiateAnimation(backgroundAnim);
  _backgroundAnimInst->setTargetWindow(_ranking->getChild("GOBackground"));
}

bool GameOverState::close(const CEGUI::EventArgs &e)
{
    _ranking->hide();
    this->popState();
    changeState(MenuState::getSingletonPtr());
    TrackManager::getSingletonPtr()->fadeToSong("Menu_song.wav");
    return true;

}

void GameOverState::updateText(float delta){

    CEGUI::String firstPart = "MISSION FAILED";
    CEGUI::String secondPart = "Game Over";


    CEGUI::String finalText;

    int firstPartTypeProgress = static_cast<int>((d_timeSinceStart - 2.5) / 0.1f);
    if(firstPartTypeProgress > 0)
        finalText += firstPart.substr(0, std::min<unsigned int>(firstPart.length(), firstPartTypeProgress));

    int secondPartTypeProgress = static_cast<int>((d_timeSinceStart - 4.5) / 0.12f);
    if(secondPartTypeProgress > 0)
        finalText += "\n" + secondPart.substr(0, std::min<unsigned int>(secondPart.length(), secondPartTypeProgress));

        finalText += "[font='Jura-50']";

        static double blinkStartDelay = 3.6f;
        double blinkPeriod = 0.8;
        double blinkTime = std::modf(static_cast<double>(d_timeSinceStart), &blinkPeriod);
        if(blinkTime > 0.55 || d_timeSinceStart < blinkStartDelay)
            finalText += "[colour='00000000']";

        finalText += (".");
      _ranking->getChild("GOLabel")->setText(finalText);
}


bool GameOverState::povMoved(const OIS::JoyStickEvent &e, int pov) {return true;}
bool GameOverState::axisMoved(const OIS::JoyStickEvent &e, int axis) {return true;}
bool GameOverState::sliderMoved(const OIS::JoyStickEvent &e, int sliderID) {return true;}
bool GameOverState::buttonPressed(const OIS::JoyStickEvent &e, int button) {return true;}
bool GameOverState::buttonReleased(const OIS::JoyStickEvent &e, int button) {return true;}
