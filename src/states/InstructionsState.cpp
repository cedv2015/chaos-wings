#include "InstructionsState.h"
#include "PlayState.h"
#include "MenuState.h"
#include "GameManager.h"
#include "OsUtils.h"

template<> InstructionsState* Ogre::Singleton<InstructionsState>::msSingleton = nullptr;

InstructionsState::InstructionsState () : _root(nullptr),  _sceneMgr(nullptr), _camera(nullptr), _keyboard(nullptr), _mouse(nullptr), _exit(false), _instructions(nullptr), inst(0){
    _list_instructions.push_back("Basic Information");
    _list_instructions.push_back("Configuration");
}

InstructionsState* InstructionsState::getSingletonPtr () {
    return msSingleton;
}

InstructionsState& InstructionsState::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}

void InstructionsState::enter () {
    std::cout << "Entered in InstructionsState" << std::endl;
  	_root = Ogre::Root::getSingletonPtr();
  	_sceneMgr = _root->getSceneManager("SceneManager");
  	_camera = _sceneMgr->getCamera("MainCamera");
  	_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    loadInstructions();
    instructionsShow();
}

void InstructionsState::exit () {
  	_sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
    _instructions->setVisible(false);
}

void InstructionsState::pause () {}

void InstructionsState::resume () {}

bool InstructionsState::frameStarted (const Ogre::FrameEvent &evt) {
    CEGUI::System::getSingleton().injectTimePulse(evt.timeSinceLastFrame);
    return !_exit;
}

bool InstructionsState::frameEnded (const Ogre::FrameEvent &evt) {
    return !_exit;
}

void InstructionsState::keyPressed (const OIS::KeyEvent &e) {
    switch (e.key) {
        default:
            break;
    }
}

void InstructionsState::keyReleased (const OIS::KeyEvent &e) {
    switch (e.key) {
        case OIS::KC_ESCAPE:
            _exit = true;
            break;
        case OIS::KC_P:
            changeState(PlayState::getSingletonPtr());
            break;
        default:
            break;
    }
}

void InstructionsState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(e.state.X.abs, e.state.Y.abs);
}
void InstructionsState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}
void InstructionsState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

CEGUI::MouseButton InstructionsState::convertMouseButton(OIS::MouseButtonID id)
{
  CEGUI::MouseButton ceguiId;
  switch(id)
    {
    case OIS::MB_Left:
      ceguiId = CEGUI::LeftButton;
      break;
    case OIS::MB_Right:
      ceguiId = CEGUI::RightButton;
      break;
    case OIS::MB_Middle:
      ceguiId = CEGUI::MiddleButton;
      break;
    default:
      ceguiId = CEGUI::LeftButton;
    }
  return ceguiId;
}

void InstructionsState::instructionsShow()
{
    if(_instructions == nullptr){
        _instructions = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("Instructions.layout");
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_instructions);

        setupWindows();
        resetAnimations();

    }else{
        _instructions->show();
        resetAnimations();
    }
}

void InstructionsState::resetAnimations(){
    d_topBarAnimInst->start();
    d_botBarAnimInst->start();
    d_topBarLabel->setText("Instructions");
    d_mouseIsHoveringNavi = false;
    d_navigationLabelPartialBlendOutInst->stop();
    d_navigationLabelBlendInAnimInst->start();
    d_naviButtonRightMoveOutInst->stop();
    d_naviButtonLeftMoveOutInst->stop();
    d_naviBotMoveOutInst->stop();
    d_naviButtonRightMoveInInst->start();
    d_naviButtonLeftMoveInInst->start();
    d_naviBotMoveInInst->start();
    d_buttonBack->start();
    _instructions->getChild("PanelContainer")->setVisible(false);

}

bool InstructionsState::hide(const CEGUI::EventArgs &e)
{
    _instructions->hide();
    changeState(InstructionsState::getSingletonPtr());
    return true;

}

CEGUI::MouseButton InstructionsState::convertJoystickButton(int id)
{
    CEGUI::MouseButton ceguiId;
    switch(id)
    {
        case BUTTON_A:
            ceguiId = CEGUI::RightButton;
            break;
        case BUTTON_B:
            ceguiId = CEGUI::LeftButton;
            break;
        default:
            break;
    }
    return ceguiId;
}

bool InstructionsState::povMoved(const OIS::JoyStickEvent &e, int pov) {
    CEGUI::Vector2f mousePos = CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().getPosition();
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(mousePos.d_x-0.1,0);
    return true;
}
bool InstructionsState::axisMoved(const OIS::JoyStickEvent &e, int axis) {
    PlayState::getSingletonPtr()->setControllMouse(false);
    Ogre::RenderWindow* render = GameManager::getSingletonPtr()->getRenderWindow();
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition((e.state.mAxes[0].abs+RANGE_JOYSTICK)/(RANGE_JOYSTICK-(-RANGE_JOYSTICK))*render->getWidth()+1, (e.state.mAxes[1].abs+RANGE_JOYSTICK)/(RANGE_JOYSTICK-(-RANGE_JOYSTICK))*render->getHeight()+1);

    return true;
}
bool InstructionsState::sliderMoved(const OIS::JoyStickEvent &e, int sliderID) {return true;}
bool InstructionsState::buttonPressed(const OIS::JoyStickEvent &e, int button) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertJoystickButton(button));
    return true;
}
bool InstructionsState::buttonReleased(const OIS::JoyStickEvent &e, int button) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertJoystickButton(button));
    return true;
}

void InstructionsState::setupWindows()
{
    d_botBarLabel = _instructions->getChild("BotBar/BotBarLabel");
    d_topBarLabel = _instructions->getChild("TopBar/TopBarLabel");

    d_naviLabel = _instructions->getChild("BotNavigationContainer/NaviCenterContainer/NavigationLabel");

    _posR = _instructions->getChild("BotNavigationContainer/RightArrow")->getPosition();
    _posL = _instructions->getChild("BotNavigationContainer/LeftArrow")->getPosition();
    _posRA = _instructions->getChild("BotNavigationContainer/RightArrowArea")->getPosition();
    _posLA = _instructions->getChild("BotNavigationContainer/LeftArrowArea")->getPosition();
    _posLabel = _instructions->getChild("BotNavigationContainer/NaviCenterContainer/NavigationLabel")->getPosition();

    d_botNaviLeftArrowArea = _instructions->getChild("BotNavigationContainer/LeftArrowArea");
    d_botNaviRightArrowArea = _instructions->getChild("BotNavigationContainer/RightArrowArea");
    d_botNaviLeftArrowArea->subscribeEvent(CEGUI::Window::EventMouseEntersArea, CEGUI::Event::Subscriber(&InstructionsState::handleMouseEntersLeftArrowArea, this));
    d_botNaviLeftArrowArea->subscribeEvent(CEGUI::Window::EventMouseLeavesArea, CEGUI::Event::Subscriber(&InstructionsState::handleMouseLeavesLeftArrowArea, this));

    d_botNaviRightArrowArea->subscribeEvent(CEGUI::Window::EventMouseEntersArea, CEGUI::Event::Subscriber(&InstructionsState::handleMouseEntersRightArrowArea, this));
    d_botNaviRightArrowArea->subscribeEvent(CEGUI::Window::EventMouseLeavesArea, CEGUI::Event::Subscriber(&InstructionsState::handleMouseLeavesRightArrowArea, this));

    d_botNaviContainer = _instructions->getChild("BotNavigationContainer");
    d_botNaviCenter = _instructions->getChild("BotNavigationContainer/NaviCenterContainer");

    d_navigationTravelIcon = _instructions->getChild("BotNavigationContainer/NaviCenterContainer/NavigationIcon");
    d_navigationSelectionIcon = _instructions->getChild("BotNavigationContainer/NaviCenterContainer/NaviBotSelectionIcon");

    setupNaviIconAnimationEventHandlers();

    CEGUI::AnimationManager& animMgr = CEGUI::AnimationManager::getSingleton();


    CEGUI::Animation* topBarAnim = animMgr.getAnimation("TopBarMoveInAnimation");
    d_topBarAnimInst = animMgr.instantiateAnimation(topBarAnim);
    CEGUI::Window* topBarWindow = _instructions->getChild("TopBar");
    d_topBarAnimInst->setTargetWindow(topBarWindow);

    CEGUI::Animation* botBarAnim = animMgr.getAnimation("BotBarMoveInAnimation");
    d_botBarAnimInst = animMgr.instantiateAnimation(botBarAnim);
    CEGUI::Window* botBarWindow = _instructions->getChild("BotBar");
    d_botBarAnimInst->setTargetWindow(botBarWindow);

    CEGUI::Animation* naviButtonRightMoveInAnim = animMgr.getAnimation("NaviButtonInstructionsRightMoveIn");
    d_naviButtonRightMoveInInst = animMgr.instantiateAnimation(naviButtonRightMoveInAnim);
    CEGUI::Animation* naviButtonRightMoveOutAnim = animMgr.getAnimation("NaviButtonInstructionsRightMoveOut");
    d_naviButtonRightMoveOutInst = animMgr.instantiateAnimation(naviButtonRightMoveOutAnim);
    CEGUI::Window* window = _instructions->getChild("BotNavigationContainer/RightArrow");
    d_naviButtonRightMoveInInst->setTargetWindow(window);
    d_naviButtonRightMoveOutInst->setTargetWindow(window);

    window->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&InstructionsState::handleRightArrowClick, this));

    CEGUI::Animation* naviButtonLeftMoveInAnim = animMgr.getAnimation("NaviButtonInstructionsLeftMoveIn");
    d_naviButtonLeftMoveInInst = animMgr.instantiateAnimation(naviButtonLeftMoveInAnim);
    CEGUI::Animation* naviButtonLeftMoveOutAnim = animMgr.getAnimation("NaviButtonInstructionsLeftMoveOut");
    d_naviButtonLeftMoveOutInst = animMgr.instantiateAnimation(naviButtonLeftMoveOutAnim);
    window = _instructions->getChild("BotNavigationContainer/LeftArrow");
    d_naviButtonLeftMoveInInst->setTargetWindow(window);
    d_naviButtonLeftMoveOutInst->setTargetWindow(window);
    window->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&InstructionsState::handleLeftArrowClick, this));

    CEGUI::Animation* naviBotMoveInAnim = animMgr.getAnimation("NaviBotCenterInstructionsMoveIn");
    d_naviBotMoveInInst = animMgr.instantiateAnimation(naviBotMoveInAnim);
    CEGUI::Animation* naviBotMoveOutAnim = animMgr.getAnimation("NaviBotCenterInstructionsMoveOut");
    d_naviBotMoveOutInst = animMgr.instantiateAnimation(naviBotMoveOutAnim);
    window = _instructions->getChild("BotNavigationContainer/NaviCenterContainer");
    d_naviBotMoveInInst->setTargetWindow(window);
    d_naviBotMoveOutInst->setTargetWindow(window);

    CEGUI::Animation* blendInAnim = animMgr.getAnimation("BlendIn");
    d_startButtonBlendInAnimInst = animMgr.instantiateAnimation(blendInAnim);
    d_startButtonBlendInAnimInst->setTargetWindow(d_startButtonClickArea);

    CEGUI::Animation* partialBlendOutAnim = animMgr.getAnimation("PartialBlendOut");

    window = _instructions->getChild("BotNavigationContainer");
    d_naviPartialBlendOutInst = animMgr.instantiateAnimation(partialBlendOutAnim);
    d_naviPartialBlendOutInst->setTargetWindow(window);
    d_naviBlendInInst = animMgr.instantiateAnimation(blendInAnim);
    d_naviBlendInInst->setTargetWindow(window);


    window = _instructions->getChild("BotNavigationContainer/NaviCenterContainer/NavigationLabel");
    window->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&InstructionsState::handleNaviLabelClick, this));
    d_navigationLabelPartialBlendOutInst = animMgr.instantiateAnimation(partialBlendOutAnim);
    d_navigationLabelPartialBlendOutInst->setTargetWindow(window);
    d_navigationLabelBlendInAnimInst = animMgr.instantiateAnimation(blendInAnim);
    d_navigationLabelBlendInAnimInst->setTargetWindow(window);


    CEGUI::Animation* travelRotateInAnim = animMgr.getAnimation("TravelRotateIn");
    CEGUI::AnimationInstance* travelRotateInInst = animMgr.instantiateAnimation(travelRotateInAnim);
    travelRotateInInst->setTargetWindow(d_navigationTravelIcon);
    CEGUI::Animation* travelRotateOutAnim = animMgr.getAnimation("TravelRotateOut");
    CEGUI::AnimationInstance* travelRotateOutInst = animMgr.instantiateAnimation(travelRotateOutAnim);
    travelRotateOutInst->setTargetWindow(d_navigationTravelIcon);

    CEGUI::Animation* blendOutAnim = animMgr.getAnimation("FullBlendOut");
    d_botBarLabelBlendOutInst = animMgr.instantiateAnimation(blendOutAnim);
    d_botBarLabelBlendOutInst->setTargetWindow(d_botBarLabel);

    CEGUI::Animation* loopRotateRightAnim = animMgr.getAnimation("LoopRotateRight");
    CEGUI::Animation* stopRotateAnim = animMgr.getAnimation("StopRotate");
    CEGUI::AnimationInstance* loopRotateRightAnimInst = animMgr.instantiateAnimation(loopRotateRightAnim);
    loopRotateRightAnimInst->setTargetWindow(d_navigationSelectionIcon);
    CEGUI::AnimationInstance* loopRotateLeftAnimInst = animMgr.instantiateAnimation(stopRotateAnim);
    loopRotateLeftAnimInst->setTargetWindow(d_navigationSelectionIcon);

    window = _instructions->getChild("BotNavigationContainer/RightArrow");
    CEGUI::Animation* naviArrowRightTwitch = animMgr.getAnimation("NaviArrowInstructionsRightTwitch");
    CEGUI::AnimationInstance* naviArrowRightTwitchInst = animMgr.instantiateAnimation(naviArrowRightTwitch);
    naviArrowRightTwitchInst->setTargetWindow(window);
    CEGUI::Animation* naviArrowRightReturn = animMgr.getAnimation("NaviArrowInstructionsRightReturn");
    CEGUI::AnimationInstance* naviArrowRightReturnInst = animMgr.instantiateAnimation(naviArrowRightReturn);
    naviArrowRightReturnInst->setTargetWindow(window);

    window = _instructions->getChild("BotNavigationContainer/LeftArrow");
    CEGUI::Animation* naviArrowLeftTwitch = animMgr.getAnimation("NaviArrowInstructionsLeftTwitch");
    CEGUI::AnimationInstance* naviArrowLeftTwitchInst = animMgr.instantiateAnimation(naviArrowLeftTwitch);
    naviArrowLeftTwitchInst->setTargetWindow(window);
    CEGUI::Animation* naviArrowLeftReturn = animMgr.getAnimation("NaviArrowInstructionsLeftReturn");
    CEGUI::AnimationInstance* naviArrowLeftReturnInst = animMgr.instantiateAnimation(naviArrowLeftReturn);
    naviArrowLeftReturnInst->setTargetWindow(window);

    CEGUI::Animation* alphaButtonAnim = animMgr.getAnimation("InsideBlendIn");

    window = _instructions->getChild("ButtonBack");
    d_buttonBack = animMgr.instantiateAnimation(alphaButtonAnim);
    d_buttonBack->setTargetWindow(window);
    window->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&InstructionsState::handleBackMenuClick, this));



}

bool InstructionsState::handleMouseEntersLeftArrowArea(const CEGUI::EventArgs&)
{
    CEGUI::EventArgs fireArgs;
    _instructions->getChild("BotNavigationContainer/LeftArrow")->fireEvent("StartTwitching", fireArgs);

    return false;
}

bool InstructionsState::handleMouseLeavesLeftArrowArea(const CEGUI::EventArgs&)
{
    CEGUI::EventArgs fireArgs;
    _instructions->getChild("BotNavigationContainer/LeftArrow")->fireEvent("EndTwitching", fireArgs);

    return false;
}

bool InstructionsState::handleMouseEntersRightArrowArea(const CEGUI::EventArgs&)
{
    CEGUI::EventArgs fireArgs;
    _instructions->getChild("BotNavigationContainer/RightArrow")->fireEvent("StartTwitching", fireArgs);

    return false;
}

bool InstructionsState::handleMouseLeavesRightArrowArea(const CEGUI::EventArgs&)
{
    CEGUI::EventArgs fireArgs;
    _instructions->getChild("BotNavigationContainer/RightArrow")->fireEvent("EndTwitching", fireArgs);

    return false;
}

bool InstructionsState::handleLeftArrowClick(const CEGUI::EventArgs&)
{
    inst--;
    d_naviLabel->setText(_list_instructions[inst%_list_instructions.size()]);
    CEGUI::Window *panel_label = _instructions->getChild("PanelContainer/PanelLabel");
    panel_label->setText(_data[inst%_list_instructions.size()]);

    return false;
}

bool InstructionsState::handleRightArrowClick(const CEGUI::EventArgs&)
{
    inst++;
    d_naviLabel->setText(_list_instructions[inst%_list_instructions.size()]);
    CEGUI::Window *panel_label = _instructions->getChild("PanelContainer/PanelLabel");
    panel_label->setText(_data[inst%_list_instructions.size()]);

    return false;
}
bool InstructionsState::handleNaviLabelClick(const CEGUI::EventArgs&)
{
    d_naviButtonRightMoveOutInst->start();
    d_naviButtonLeftMoveOutInst->start();
    d_naviBotMoveOutInst->start();
    CEGUI::Window *panel = _instructions->getChild("PanelContainer");
    CEGUI::Window *panel_label = _instructions->getChild("PanelContainer/PanelLabel");
    if(!panel->isVisible()){
        panel->setVisible(true);
        CEGUI::Animation* alphaButtonAnim = CEGUI::AnimationManager::getSingleton().getAnimation("FadeIn");
        CEGUI::AnimationInstance* fade = CEGUI::AnimationManager::getSingleton().instantiateAnimation(alphaButtonAnim);
        fade->setTargetWindow(panel);
        fade->start();
    }
    panel_label->setText(_data[inst%_list_instructions.size()]);

    return false;
}
bool InstructionsState::handleBackMenuClick(const CEGUI::EventArgs&)
{
    d_naviButtonRightMoveOutInst->stop();
    d_naviButtonLeftMoveOutInst->stop();
    d_naviBotMoveOutInst->stop();
    _instructions->getChild("BotNavigationContainer/NaviCenterContainer/NavigationLabel")->setPosition(_posLabel);
    _instructions->getChild("BotNavigationContainer/LeftArrow")->setPosition(_posL);
    _instructions->getChild("BotNavigationContainer/RightArrow")->setPosition(_posR);
    _instructions->getChild("BotNavigationContainer/RightArrowArea")->setPosition(_posRA);
    _instructions->getChild("BotNavigationContainer/LeftArrowArea")->setPosition(_posLA);
    inst=0;
    d_naviLabel->setText(_list_instructions[inst%_list_instructions.size()]);
    CEGUI::Window *panel_label = _instructions->getChild("PanelContainer/PanelLabel");
    panel_label->setText(_data[inst%_list_instructions.size()]);
    changeState(MenuState::getSingletonPtr());

    return false;
}



void InstructionsState::setupNaviIconAnimationEventHandlers()
{
    d_botNaviLeftArrowArea->subscribeEvent(CEGUI::Window::EventMouseEntersArea, CEGUI::Event::Subscriber(&InstructionsState::handleCheckIfNaviIconAnimationNeedsChange, this));
    d_botNaviLeftArrowArea->subscribeEvent(CEGUI::Window::EventMouseLeavesArea, CEGUI::Event::Subscriber(&InstructionsState::handleCheckIfNaviIconAnimationNeedsChange, this));
    d_botNaviRightArrowArea->subscribeEvent(CEGUI::Window::EventMouseEntersArea, CEGUI::Event::Subscriber(&InstructionsState::handleCheckIfNaviIconAnimationNeedsChange, this));
    d_botNaviRightArrowArea->subscribeEvent(CEGUI::Window::EventMouseLeavesArea, CEGUI::Event::Subscriber(&InstructionsState::handleCheckIfNaviIconAnimationNeedsChange, this));
    CEGUI::Window* window;
    window = _instructions->getChild("BotNavigationContainer/NaviCenterContainer/NavigationLabel");
    window->subscribeEvent(CEGUI::Window::EventMouseEntersArea, CEGUI::Event::Subscriber(&InstructionsState::handleCheckIfNaviIconAnimationNeedsChange, this));
    window->subscribeEvent(CEGUI::Window::EventMouseLeavesArea, CEGUI::Event::Subscriber(&InstructionsState::handleCheckIfNaviIconAnimationNeedsChange, this));
    window = _instructions->getChild("BotNavigationContainer/NaviCenterContainer/NavigationIcon");
    window->subscribeEvent(CEGUI::Window::EventMouseEntersArea, CEGUI::Event::Subscriber(&InstructionsState::handleCheckIfNaviIconAnimationNeedsChange, this));
    window->subscribeEvent(CEGUI::Window::EventMouseLeavesArea, CEGUI::Event::Subscriber(&InstructionsState::handleCheckIfNaviIconAnimationNeedsChange, this));
    window = _instructions->getChild("BotNavigationContainer/NaviCenterContainer/NaviBotSelectionIcon");
    window->subscribeEvent(CEGUI::Window::EventMouseEntersArea, CEGUI::Event::Subscriber(&InstructionsState::handleCheckIfNaviIconAnimationNeedsChange, this));
    window->subscribeEvent(CEGUI::Window::EventMouseLeavesArea, CEGUI::Event::Subscriber(&InstructionsState::handleCheckIfNaviIconAnimationNeedsChange, this));
}

bool InstructionsState::handleCheckIfNaviIconAnimationNeedsChange(const CEGUI::EventArgs&)
{
    bool mouseIsHovering = false;
    CEGUI::Window* window;

    window = _instructions->getChild("BotNavigationContainer/NaviCenterContainer/NavigationLabel");
    mouseIsHovering |= window->isMouseContainedInArea();
    window = _instructions->getChild("BotNavigationContainer/NaviCenterContainer/NavigationIcon");
    mouseIsHovering |= window->isMouseContainedInArea();
    window = _instructions->getChild("BotNavigationContainer/NaviCenterContainer/NaviBotSelectionIcon");
    mouseIsHovering |= window->isMouseContainedInArea();
    mouseIsHovering |= d_botNaviRightArrowArea->isMouseContainedInArea();
    mouseIsHovering |= d_botNaviLeftArrowArea->isMouseContainedInArea();

    //We fire an event to trigger the animation depending on if the mouse hovers a critical
    //window or not. Additionally we perform a check to not fire an event for an animation that is already running
    if(d_mouseIsHoveringNavi != mouseIsHovering)
    {
        d_mouseIsHoveringNavi = mouseIsHovering;
        CEGUI::EventArgs args;

        if(mouseIsHovering)
        {
            d_navigationSelectionIcon->fireEvent("StartRotate", args);
            d_navigationTravelIcon->fireEvent("StartRotate", args);
        }
        else
        {
            d_navigationSelectionIcon->fireEvent("StopRotate", args);
            d_navigationTravelIcon->fireEvent("StopRotate", args);
        }
    }

    return false;
}

void InstructionsState::loadInstructions() {
    _data.clear();
    std::string separator = "";
    int os = OsUtils::getOs();
    if (os == OsUtils::OS::LINUX || os == OsUtils::OS::MAC) {
        separator = "/";
    }
    else if (os == OsUtils::OS::WINDOWS) {
        separator = "\\";
    }
    std::string file_name = std::string("misc") + separator + std::string("instructions.txt");

    std::ifstream file(file_name);
    std::string line;
    std::vector<std::string> data;
    if (file.is_open()) {
        std::string item;
        while (getline(file, line)) {
            std::stringstream iss;
            iss << line;
            while (std::getline (iss, item, '\n')) {
                data.push_back(item);
            }
        }
        file.close();
    }
    CEGUI::String finalText ="";
    for(int i=0; (unsigned)i<data.size(); i++){

          finalText += data[i]+"\n";
    }
    std::stringstream string;
    std::string item;
    string << finalText;
    while (std::getline (string, item, ':')) {
                _data.push_back(item);
    }

}
