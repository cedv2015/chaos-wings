/***************************************************************************
*   Copyright (C) 2004 - 2012 Paul D Turner & The CEGUI Development Team
*
*   Permission is hereby granted, free of charge, to any person obtaining
*   a copy of this software and associated documentation files (the
*   "Software"), to deal in the Software without restriction, including
*   without limitation the rights to use, copy, modify, merge, publish,
*   distribute, sublicense, and/or sell copies of the Software, and to
*   permit persons to whom the Software is furnished to do so, subject to
*   the following conditions:
*
*   The above copyright notice and this permission notice shall be
*   included in all copies or substantial portions of the Software.
*
*   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
*   EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
*   MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
*   IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
*   OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
*   ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
*   OTHER DEALINGS IN THE SOFTWARE.
***************************************************************************/

#include "MenuState.h"
#include "PlayState.h"
#include "InstructionsState.h"
#include "RankingState.h"
#include "CreditsState.h"
#include "GameManager.h"

#include <cmath>
#include <algorithm>

#define _USE_MATH_DEFINES
#include <math.h>

#ifndef M_PI
#define M_PI		3.14159265358979323846
#endif


template<> MenuState* Ogre::Singleton<MenuState>::msSingleton = nullptr;

MenuState::MenuState () :  _root(nullptr), _sceneMgr(nullptr), _camera(nullptr), _keyboard(nullptr), _mouse(nullptr), _exit(false),  d_startButtonClicked(false), _layout(nullptr), i(1), _trackMgr(nullptr) {
    _list_phases.push_back("Phase 1");
    _list_phases.push_back("Phase 2");
    _list_phases.push_back("Phase 3");
}

MenuState* MenuState::getSingletonPtr () {
    return msSingleton;
}

MenuState& MenuState::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}

void MenuState::enter () {
    std::cout << "Entered in MenuState" << std::endl;
    _root = Ogre::Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");
    _trackMgr = TrackManager::getSingletonPtr();
    menu();
    entering();
}

void MenuState::exit () {
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
    _layout->setVisible(false);
}

void MenuState::pause () {}

void MenuState::resume () {}

bool MenuState::frameStarted (const Ogre::FrameEvent &evt) {
    float delta = evt.timeSinceLastFrame;
    CEGUI::System::getSingleton().injectTimePulse(delta);
    update(delta);
    _trackMgr->update(delta);
    return !_exit;
}

bool MenuState::frameEnded (const Ogre::FrameEvent &evt) {
    return !_exit;
}

void MenuState::keyPressed(const OIS::KeyEvent& evt)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyDown(static_cast<CEGUI::Key::Scan>(evt.key));
  CEGUI::System::getSingleton().getDefaultGUIContext().injectChar(evt.text);

}

void MenuState::keyReleased(const OIS::KeyEvent& evt)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyUp(static_cast<CEGUI::Key::Scan>(evt.key));
  switch (evt.key) {
      case OIS::KC_ESCAPE:
          _exit = true;
          break;
      case OIS::KC_P:
          changeState(PlayState::getSingletonPtr());
          break;
      case OIS::KC_I:
          changeState(InstructionsState::getSingletonPtr());
          break;
      case OIS::KC_R:
          changeState(RankingState::getSingletonPtr());
          break;
      default:
          break;
  }

}

void MenuState::mouseMoved(const OIS::MouseEvent& e)
{
    PlayState::getSingletonPtr()->setControllMouse(true);
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(e.state.X.abs, e.state.Y.abs);
}

void MenuState::mousePressed(const OIS::MouseEvent& e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}

void MenuState::mouseReleased(const OIS::MouseEvent& e, OIS::MouseButtonID id)
{
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

CEGUI::MouseButton MenuState::convertMouseButton(OIS::MouseButtonID id)
{
    CEGUI::MouseButton ceguiId;
    switch(id)
    {
        case OIS::MB_Left:
            ceguiId = CEGUI::LeftButton;
            break;
        case OIS::MB_Right:
            ceguiId = CEGUI::RightButton;
            break;
        case OIS::MB_Middle:
            ceguiId = CEGUI::MiddleButton;
            break;
        default:
            ceguiId = CEGUI::LeftButton;
    }
    return ceguiId;
}

CEGUI::MouseButton MenuState::convertJoystickButton(int id)
{
    CEGUI::MouseButton ceguiId;
    switch(id)
    {
        case BUTTON_A:
            ceguiId = CEGUI::RightButton;
            break;
        case BUTTON_B:
            ceguiId = CEGUI::LeftButton;
            break;
        default:
            break;
    }
    return ceguiId;
}

bool MenuState::povMoved(const OIS::JoyStickEvent &e, int pov) {
    CEGUI::Vector2f mousePos = CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().getPosition();
    /*switch(e.state.mPOV[pov].direction){
        case OIS::Pov::West:
            CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(mousePos.d_x-0.1,0)
        case OIS::Pov::East:
            CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(mousePos.d_x+0.1,0)
        case OIS::Pov::North:
            CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(0, mousePos.d_y+0.1,0)
        case OIS::Pov::South:
            CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(0, mousePos.d_y-0.1,0)
    }*/
    //std::cout << mousePos.d_x << std::endl;
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(mousePos.d_x-0.1,0);
    return true;
}
bool MenuState::axisMoved(const OIS::JoyStickEvent &e, int axis) {
    PlayState::getSingletonPtr()->setControllMouse(false);
    Ogre::RenderWindow* render = GameManager::getSingletonPtr()->getRenderWindow();
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition((e.state.mAxes[0].abs+RANGE_JOYSTICK)/(RANGE_JOYSTICK-(-RANGE_JOYSTICK))*render->getWidth()+1, (e.state.mAxes[1].abs+RANGE_JOYSTICK)/(RANGE_JOYSTICK-(-RANGE_JOYSTICK))*render->getHeight()+1);

    return true;
}
bool MenuState::sliderMoved(const OIS::JoyStickEvent &e, int sliderID) {return true;}
bool MenuState::buttonPressed(const OIS::JoyStickEvent &e, int button) {
    // e.state.buttonDown(button)
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertJoystickButton(button));
    return true;
}
bool MenuState::buttonReleased(const OIS::JoyStickEvent &e, int button) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertJoystickButton(button));
    return true;
}


void MenuState::menu(){

    if(_layout == nullptr){
        _layout = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("GameMenu.layout");
        CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_layout);

        setupWindows();
        setupAnimations();

        d_navigationIsEnabled = false;
        d_mouseIsHoveringNavi = false;

        d_startButtonClickArea->setAlpha(0.0f);
        d_startButtonBlendInAnimInst->start();

        enableNavigationBarElements();

        d_navigationTravelIcon->setEnabled(true);

        d_timeSinceLoginAccepted = 0.0f;
        d_currentWriteFocus = WF_TopBar;

        d_botNaviContainer->setEnabled(true);
    }else{
        _layout->show();

        d_navigationIsEnabled = false;
        d_mouseIsHoveringNavi = false;

        d_startButtonClickArea->setAlpha(0.0f);
        d_startButtonBlendInAnimInst->start();

        enableNavigationBarElements();

        d_navigationTravelIcon->setEnabled(true);

        d_timeSinceLoginAccepted = 0.0f;
        d_currentWriteFocus = WF_TopBar;

        d_botNaviContainer->setEnabled(true);
    }

}

void MenuState::setupWindows()
{
    d_botBarLabel = _layout->getChild("BotBar/BotBarLabel");
    d_topBarLabel = _layout->getChild("TopBar/TopBarLabel");


    d_naviLabel = _layout->getChild("BotNavigationContainer/NaviCenterContainer/NavigationLabel");

    setupNaviArrowWindows();

    setupButtonClickHandlers();

    d_botNaviContainer = _layout->getChild("BotNavigationContainer");
    d_botNaviCenter = _layout->getChild("BotNavigationContainer/NaviCenterContainer");

    d_startButtonClickArea = _layout->getChild("InnerPartContainer/InsideStartClickArea");
    d_startButtonClickArea->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&MenuState::handleInnerPartStartClickAreaClick, this));

    d_navigationTravelIcon = _layout->getChild("BotNavigationContainer/NaviCenterContainer/NavigationIcon");
    d_navigationSelectionIcon = _layout->getChild("BotNavigationContainer/NaviCenterContainer/NaviBotSelectionIcon");

    d_navigationSelectionIcon->subscribeEvent(CEGUI::AnimationInstance::EventAnimationStarted, CEGUI::Event::Subscriber(&MenuState::handleNaviSelectionIconAnimStart, this));

    setupNaviIconAnimationEventHandlers();

    setupInnerButtonsSubOptionsLabels();

}

void MenuState::entering()
{
    d_navigationTravelIcon->setEnabled(false);

    d_timeSinceStart = 2.0f;

    d_interactiveElementsWereInitiallyBlendedOut = true;
    d_interactivePlanetElementsAreEnabled = true;
    d_startButtonClicked = true;

    d_botBarLabel->setAlpha(1.0f);
    d_botNaviContainer->setAlpha(1.0f);

    d_centerButtonsBlendInInst->setPosition(d_centerButtonsBlendInInst->getDefinition()->getDuration());
    d_centerButtonsBlendInInst->apply();

    d_currentWriteFocus = WF_BotBar;

    //Reset labels and visibility
    d_topBarLabel->setText("");
    d_botBarLabel->setText("");


    _layout->getChild("InnerButtonsContainer/PopupLinesInstructions")->setVisible(false);
    _layout->getChild("InnerButtonsContainer/PopupLinesRanking")->setVisible(false);
    _layout->getChild("InnerButtonsContainer/PopupLinesCharacters")->setVisible(false);
    _layout->getChild("InnerButtonsContainer/PopupLinesQuit")->setVisible(false);

    resetAnimations();

    d_startButtonClickArea->setVisible(false);

    //Start the animations
    startEntranceAnimations();

    makeAllSelectionIconsInvisible();
}

void MenuState::startEntranceAnimations()
{
    d_topBarAnimInst->start();
    d_botBarAnimInst->start();
    d_insideBlendInAnimInst->start();
    d_insideImage3RotateInInst->start();
    d_insideImage4RotateInInst->start();
    d_insideImageRingsContainerSizeInInst->start();
    d_buttonFadeInAnimInst2->start();
    d_buttonFadeInAnimInst3->start();
    d_buttonFadeInAnimInst4->start();
    d_buttonFadeInAnimInst5->start();
    d_naviButtonRightMoveInInst->start();
    d_naviButtonLeftMoveInInst->start();
    d_naviBotMoveInInst->start();
}

void MenuState::resetAnimations()
{
    //Set animations to end and apply their changes to the windows
    //For every animation instance that uses source properties
    //so the values will be taken correctly after starting them again
    d_buttonFadeInAnimInst2->setPosition(d_buttonFadeInAnimInst2->getDefinition()->getDuration());
    d_buttonFadeInAnimInst2->apply();
    d_buttonFadeInAnimInst3->setPosition(d_buttonFadeInAnimInst3->getDefinition()->getDuration());
    d_buttonFadeInAnimInst3->apply();
    d_buttonFadeInAnimInst4->setPosition(d_buttonFadeInAnimInst4->getDefinition()->getDuration());
    d_buttonFadeInAnimInst4->apply();
    d_buttonFadeInAnimInst5->setPosition(d_buttonFadeInAnimInst5->getDefinition()->getDuration());
    d_buttonFadeInAnimInst5->apply();
    d_naviButtonRightMoveInInst->setPosition(d_naviButtonRightMoveInInst->getDefinition()->getDuration());
    d_naviButtonRightMoveInInst->apply();
    d_naviButtonLeftMoveInInst->setPosition(d_naviButtonLeftMoveInInst->getDefinition()->getDuration());
    d_naviButtonLeftMoveInInst->apply();
    d_naviBotMoveInInst->setPosition(d_naviBotMoveInInst->getDefinition()->getDuration());
    d_naviBotMoveInInst->apply();
}

void MenuState::update(float timeSinceLastUpdate)
{
    d_timeSinceStart += timeSinceLastUpdate;


    d_timeSinceLoginAccepted += timeSinceLastUpdate;

    updateLoginWelcomeText();


    if(d_timeSinceStart >= s_secondStartDelay && !d_interactiveElementsWereInitiallyBlendedOut)
    {
        disableInteractivePlanetElements();
        disableNavigationBarElements();
    }

    _layout->getChild("BotNavigationContainer")->setVisible(false);
}

void MenuState::disableInteractivePlanetElements()
{
    d_centerButtonsBlendInInst->pause();
    d_centerButtonsPartialBlendOutInst->start();


    d_interactiveElementsWereInitiallyBlendedOut = true;
}

void MenuState::disableNavigationBarElements()
{
    d_naviBlendInInst->pause();
    d_naviPartialBlendOutInst->start();

    d_navigationLabelBlendInAnimInst->stop();
    d_navigationLabelPartialBlendOutInst->start();

    d_navigationIsEnabled = false;
}

void MenuState::updateIntroText()
{
    static const CEGUI::String firstPart = "Connection established...";
    static const CEGUI::String secondPart = "Warning! User Authentication required!";

    CEGUI::String finalText;

    int firstPartTypeProgress = static_cast<int>((d_timeSinceStart - s_firstStartDelay) / 0.08f);
    if(firstPartTypeProgress > 0)
        finalText += firstPart.substr(0, std::min<unsigned int>(firstPart.length(), firstPartTypeProgress));

    int secondPartTypeProgress = static_cast<int>((d_timeSinceStart - s_secondStartDelay) / 0.08f);
    if(secondPartTypeProgress > 0)
        finalText += "\n" + secondPart.substr(0, std::min<unsigned int>(secondPart.length(), secondPartTypeProgress));

    finalText += "[font='Jura-13']";

    static double blinkStartDelay = 3.6f;
    double blinkPeriod = 0.8;
    double blinkTime = std::modf(static_cast<double>(d_timeSinceStart), &blinkPeriod);
    if(blinkTime > 0.55 || d_timeSinceStart < blinkStartDelay || d_currentWriteFocus != WF_BotBar)
        finalText += "[colour='00000000']";

    finalText += ("_");

    d_botBarLabel->setText(finalText);
}

void MenuState::updateLoginWelcomeText()
{
    if(d_timeSinceLoginAccepted <= 0.0f)
        return;

    static const CEGUI::String firstPart = "Welcome to Chaos Wings";
    CEGUI::String displayText = firstPart + d_userName;
    CEGUI::String finalText;

    int progress = static_cast<int>(d_timeSinceLoginAccepted / 0.08f);
    if(progress > 0)
        finalText += displayText.substr(0, std::min<unsigned int>(displayText.length(), progress));

    finalText += "[font='Jura-13']";

    double blinkPeriod = 0.8;
    double blinkTime = std::modf(static_cast<double>(d_timeSinceStart), &blinkPeriod);
    if(blinkTime > 0.55 || d_currentWriteFocus != WF_TopBar)
        finalText += "[colour='00000000']";

    finalText += ("_");

    d_topBarLabel->setText(finalText);
}

void MenuState::setupAnimations()
{
    CEGUI::AnimationManager& animMgr = CEGUI::AnimationManager::getSingleton();
    animMgr.loadAnimationsFromXML("GameMenu.anims");

    CEGUI::Animation* startButtAnim = animMgr.getAnimation("StartButtonPulsating");
    CEGUI::AnimationInstance* startButtAnimInstance = animMgr.instantiateAnimation(startButtAnim);
    CEGUI::Window* startButtWindow = _layout->getChild("InnerPartContainer/InsideStartClickArea/StartButtonImage");
    startButtAnimInstance->setTargetWindow(startButtWindow);
    startButtAnimInstance->start();

    CEGUI::Animation* insideImg1Anim = animMgr.getAnimation("InsideImage1Pulsating");
    CEGUI::AnimationInstance* insideImg1AnimInst = animMgr.instantiateAnimation(insideImg1Anim);
    CEGUI::Window* insideImg1 = _layout->getChild("InnerPartContainer/InsideImage1");
    insideImg1AnimInst->setTargetWindow(insideImg1);
    insideImg1AnimInst->start();

    CEGUI::Animation* topBarAnim = animMgr.getAnimation("TopBarMoveInAnimation");
    d_topBarAnimInst = animMgr.instantiateAnimation(topBarAnim);
    CEGUI::Window* topBarWindow = _layout->getChild("TopBar");
    d_topBarAnimInst->setTargetWindow(topBarWindow);

    CEGUI::Animation* botBarAnim = animMgr.getAnimation("BotBarMoveInAnimation");
    d_botBarAnimInst = animMgr.instantiateAnimation(botBarAnim);
    CEGUI::Window* botBarWindow = _layout->getChild("BotBar");
    d_botBarAnimInst->setTargetWindow(botBarWindow);

    CEGUI::Animation* insideBlendInAnim = animMgr.getAnimation("InsideBlendIn");
    d_insideBlendInAnimInst = animMgr.instantiateAnimation(insideBlendInAnim);
    CEGUI::Window* innerPartContainer = _layout->getChild("InnerPartContainer");
    d_insideBlendInAnimInst->setTargetWindow(innerPartContainer);

    CEGUI::Animation* insideImage3RotateIn = animMgr.getAnimation("InsideImage3RotateIn");
    d_insideImage3RotateInInst = animMgr.instantiateAnimation(insideImage3RotateIn);
    CEGUI::Window* insideImage3 = _layout->getChild("InnerPartContainer/OuterRingsContainer/InsideImage3");
    d_insideImage3RotateInInst->setTargetWindow(insideImage3);

    CEGUI::Animation* insideImage4RotateIn = animMgr.getAnimation("InsideImage4RotateIn");
    d_insideImage4RotateInInst = animMgr.instantiateAnimation(insideImage4RotateIn);
    CEGUI::Window* insideImage4 = _layout->getChild("InnerPartContainer/OuterRingsContainer/InsideImage4");
    d_insideImage4RotateInInst->setTargetWindow(insideImage4);

    CEGUI::Animation* insideImageRingsContainerSizeIn = animMgr.getAnimation("RingsContainerSizeIn");
    d_insideImageRingsContainerSizeInInst = animMgr.instantiateAnimation(insideImageRingsContainerSizeIn);
    CEGUI::Window* insideImageContainer = _layout->getChild("InnerPartContainer/OuterRingsContainer");
    d_insideImageRingsContainerSizeInInst->setTargetWindow(insideImageContainer);

    CEGUI::Animation* buttonFadeInAnim = animMgr.getAnimation("ButtonFadeIn");
    d_buttonFadeInAnimInst2 = animMgr.instantiateAnimation(buttonFadeInAnim);
    CEGUI::Window* window = _layout->getChild("InnerButtonsContainer/ButtonRanking");
    d_buttonFadeInAnimInst2->setTargetWindow(window);
    d_buttonFadeInAnimInst3 = animMgr.instantiateAnimation(buttonFadeInAnim);
    window = _layout->getChild("InnerButtonsContainer/ButtonInstructions");
    d_buttonFadeInAnimInst3->setTargetWindow(window);
    d_buttonFadeInAnimInst4 = animMgr.instantiateAnimation(buttonFadeInAnim);
    window = _layout->getChild("InnerButtonsContainer/ButtonCharacters");
    d_buttonFadeInAnimInst4->setTargetWindow(window);
    d_buttonFadeInAnimInst5 = animMgr.instantiateAnimation(buttonFadeInAnim);
    window = _layout->getChild("InnerButtonsContainer/ButtonQuit");
    d_buttonFadeInAnimInst5->setTargetWindow(window);

    CEGUI::Animation* naviButtonRightMoveInAnim = animMgr.getAnimation("NaviButtonRightMoveIn");
    d_naviButtonRightMoveInInst = animMgr.instantiateAnimation(naviButtonRightMoveInAnim);
    window = _layout->getChild("BotNavigationContainer/RightArrow");
    d_naviButtonRightMoveInInst->setTargetWindow(window);


    window->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&MenuState::handleRightArrowClick, this));

    CEGUI::Animation* naviButtonLeftMoveInAnim = animMgr.getAnimation("NaviButtonLeftMoveIn");
    d_naviButtonLeftMoveInInst = animMgr.instantiateAnimation(naviButtonLeftMoveInAnim);
    window = _layout->getChild("BotNavigationContainer/LeftArrow");
    d_naviButtonLeftMoveInInst->setTargetWindow(window);


    window->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&MenuState::handleLeftArrowClick, this));

    CEGUI::Animation* naviBotMoveInAnim = animMgr.getAnimation("NaviBotCenterMoveIn");
    d_naviBotMoveInInst = animMgr.instantiateAnimation(naviBotMoveInAnim);
    window = _layout->getChild("BotNavigationContainer/NaviCenterContainer");
    d_naviBotMoveInInst->setTargetWindow(window);

    CEGUI::Animation* blendInAnim = animMgr.getAnimation("BlendIn");
    d_startButtonBlendInAnimInst = animMgr.instantiateAnimation(blendInAnim);
    d_startButtonBlendInAnimInst->setTargetWindow(d_startButtonClickArea);

    CEGUI::Animation* partialBlendOutAnim = animMgr.getAnimation("PartialBlendOut");

    window = _layout->getChild("BotNavigationContainer");
    d_naviPartialBlendOutInst = animMgr.instantiateAnimation(partialBlendOutAnim);
    d_naviPartialBlendOutInst->setTargetWindow(window);
    d_naviBlendInInst = animMgr.instantiateAnimation(blendInAnim);
    d_naviBlendInInst->setTargetWindow(window);
    window = _layout->getChild("InnerButtonsContainer");
    d_centerButtonsPartialBlendOutInst = animMgr.instantiateAnimation(partialBlendOutAnim);
    d_centerButtonsPartialBlendOutInst->setTargetWindow(window);
    d_centerButtonsBlendInInst = animMgr.instantiateAnimation(blendInAnim);
    d_centerButtonsBlendInInst->setTargetWindow(window);


    window = _layout->getChild("BotNavigationContainer/NaviCenterContainer/NavigationLabel");
    window->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&MenuState::handleNaviLabelClick, this));
    d_navigationLabelPartialBlendOutInst = animMgr.instantiateAnimation(partialBlendOutAnim);
    d_navigationLabelPartialBlendOutInst->setTargetWindow(window);
    d_navigationLabelBlendInAnimInst = animMgr.instantiateAnimation(blendInAnim);
    d_navigationLabelBlendInAnimInst->setTargetWindow(window);


    CEGUI::Animation* travelRotateInAnim = animMgr.getAnimation("TravelRotateIn");
    CEGUI::AnimationInstance* travelRotateInInst = animMgr.instantiateAnimation(travelRotateInAnim);
    travelRotateInInst->setTargetWindow(d_navigationTravelIcon);
    CEGUI::Animation* travelRotateOutAnim = animMgr.getAnimation("TravelRotateOut");
    CEGUI::AnimationInstance* travelRotateOutInst = animMgr.instantiateAnimation(travelRotateOutAnim);
    travelRotateOutInst->setTargetWindow(d_navigationTravelIcon);

    CEGUI::Animation* blendOutAnim = animMgr.getAnimation("FullBlendOut");
    d_botBarLabelBlendOutInst = animMgr.instantiateAnimation(blendOutAnim);
    d_botBarLabelBlendOutInst->setTargetWindow(d_botBarLabel);

    CEGUI::Animation* loopRotateRightAnim = animMgr.getAnimation("LoopRotateRight");
    CEGUI::Animation* stopRotateAnim = animMgr.getAnimation("StopRotate");
    CEGUI::AnimationInstance* loopRotateRightAnimInst = animMgr.instantiateAnimation(loopRotateRightAnim);
    loopRotateRightAnimInst->setTargetWindow(d_navigationSelectionIcon);
    CEGUI::AnimationInstance* loopRotateLeftAnimInst = animMgr.instantiateAnimation(stopRotateAnim);
    loopRotateLeftAnimInst->setTargetWindow(d_navigationSelectionIcon);

    window = _layout->getChild("BotNavigationContainer/RightArrow");
    CEGUI::Animation* naviArrowRightTwitch = animMgr.getAnimation("NaviArrowRightTwitch");
    CEGUI::AnimationInstance* naviArrowRightTwitchInst = animMgr.instantiateAnimation(naviArrowRightTwitch);
    naviArrowRightTwitchInst->setTargetWindow(window);
    CEGUI::Animation* naviArrowRightReturn = animMgr.getAnimation("NaviArrowRightReturn");
    CEGUI::AnimationInstance* naviArrowRightReturnInst = animMgr.instantiateAnimation(naviArrowRightReturn);
    naviArrowRightReturnInst->setTargetWindow(window);

    window = _layout->getChild("BotNavigationContainer/LeftArrow");

    CEGUI::Animation* naviArrowLeftTwitch = animMgr.getAnimation("NaviArrowLeftTwitch");
    CEGUI::AnimationInstance* naviArrowLeftTwitchInst = animMgr.instantiateAnimation(naviArrowLeftTwitch);
    naviArrowLeftTwitchInst->setTargetWindow(window);
    CEGUI::Animation* naviArrowLeftReturn = animMgr.getAnimation("NaviArrowLeftReturn");
    CEGUI::AnimationInstance* naviArrowLeftReturnInst = animMgr.instantiateAnimation(naviArrowLeftReturn);
    naviArrowLeftReturnInst->setTargetWindow(window);

    setupPopupLinesAnimations();

    setupSelectionIconAnimations();
}

void MenuState::setupPopupLinesAnimations()
{
    CEGUI::Window* window;
    CEGUI::AnimationManager& animMgr = CEGUI::AnimationManager::getSingleton();

    CEGUI::Animation* sizeGrowth = animMgr.getAnimation("SizeGrowth");
    window = _layout->getChild("InnerButtonsContainer/PopupLinesInstructions");
    d_popupLinesInstructionsAnimInst = animMgr.instantiateAnimation(sizeGrowth);
    d_popupLinesInstructionsAnimInst->setTarget(window);
    window = _layout->getChild("InnerButtonsContainer/PopupLinesRanking");
    d_popupLinesRankingAnimInst = animMgr.instantiateAnimation(sizeGrowth);
    d_popupLinesRankingAnimInst->setTarget(window);
    window = _layout->getChild("InnerButtonsContainer/PopupLinesCharacters");
    d_popupLinesCharactersAnimInst = animMgr.instantiateAnimation(sizeGrowth);
    d_popupLinesCharactersAnimInst->setTarget(window);
    window = _layout->getChild("InnerButtonsContainer/PopupLinesQuit");
    d_popupLinesQuitAnimInst = animMgr.instantiateAnimation(sizeGrowth);
    d_popupLinesQuitAnimInst->setTarget(window);
}

void MenuState::setupSelectionIconAnimations()
{
    CEGUI::Window* window;
    CEGUI::AnimationManager& animMgr = CEGUI::AnimationManager::getSingleton();

    CEGUI::Animation* iconAnimationLoop = animMgr.getAnimation("LoopRotateRight");
    CEGUI::Animation* iconAnimationStop = animMgr.getAnimation("StopRotate");
    CEGUI::AnimationInstance* iconAnimInst;

    window = _layout->getChild("InnerButtonsContainer/RankingSelectionIcon");
    iconAnimInst = animMgr.instantiateAnimation(iconAnimationLoop);
    iconAnimInst->setTargetWindow(window);
    iconAnimInst = animMgr.instantiateAnimation(iconAnimationStop);
    iconAnimInst->setTargetWindow(window);
    window = _layout->getChild("InnerButtonsContainer/InstructionsSelectionIcon");
    iconAnimInst = animMgr.instantiateAnimation(iconAnimationLoop);
    iconAnimInst->setTargetWindow(window);
    iconAnimInst = animMgr.instantiateAnimation(iconAnimationStop);
    iconAnimInst->setTargetWindow(window);
    window = _layout->getChild("InnerButtonsContainer/NoSelectionIcon");
    iconAnimInst = animMgr.instantiateAnimation(iconAnimationLoop);
    iconAnimInst->setTargetWindow(window);
    iconAnimInst = animMgr.instantiateAnimation(iconAnimationStop);
    iconAnimInst->setTargetWindow(window);
    window = _layout->getChild("InnerButtonsContainer/YesSelectionIcon");
    iconAnimInst = animMgr.instantiateAnimation(iconAnimationLoop);
    iconAnimInst->setTargetWindow(window);
    iconAnimInst = animMgr.instantiateAnimation(iconAnimationStop);
    iconAnimInst->setTargetWindow(window);
    window = _layout->getChild("InnerButtonsContainer/InstructionsSelectionIcon");
    iconAnimInst = animMgr.instantiateAnimation(iconAnimationLoop);
    iconAnimInst->setTargetWindow(window);
    iconAnimInst = animMgr.instantiateAnimation(iconAnimationStop);
    iconAnimInst->setTargetWindow(window);

    window = _layout->getChild("InnerButtonsContainer/CreditsSelectionIcon");
    iconAnimInst = animMgr.instantiateAnimation(iconAnimationLoop);
    iconAnimInst->setTargetWindow(window);
    iconAnimInst = animMgr.instantiateAnimation(iconAnimationStop);
    iconAnimInst->setTargetWindow(window);
}

bool MenuState::close(const CEGUI::EventArgs&)
{
    _exit=true;
    return true;
}
bool MenuState::instructions(const CEGUI::EventArgs&)
{
    changeState(InstructionsState::getSingletonPtr());
    return true;
}
bool MenuState::credits(const CEGUI::EventArgs&)
{
    changeState(CreditsState::getSingletonPtr());
    return true;
}
bool MenuState::ranking(const CEGUI::EventArgs&)
{
    changeState(RankingState::getSingletonPtr());
    return true;
}
void MenuState::setupNaviIconAnimationEventHandlers()
{
    d_botNaviLeftArrowArea->subscribeEvent(CEGUI::Window::EventMouseEntersArea, CEGUI::Event::Subscriber(&MenuState::handleCheckIfNaviIconAnimationNeedsChange, this));
    d_botNaviLeftArrowArea->subscribeEvent(CEGUI::Window::EventMouseLeavesArea, CEGUI::Event::Subscriber(&MenuState::handleCheckIfNaviIconAnimationNeedsChange, this));
    d_botNaviRightArrowArea->subscribeEvent(CEGUI::Window::EventMouseEntersArea, CEGUI::Event::Subscriber(&MenuState::handleCheckIfNaviIconAnimationNeedsChange, this));
    d_botNaviRightArrowArea->subscribeEvent(CEGUI::Window::EventMouseLeavesArea, CEGUI::Event::Subscriber(&MenuState::handleCheckIfNaviIconAnimationNeedsChange, this));
    CEGUI::Window* window;
    window = _layout->getChild("BotNavigationContainer/NaviCenterContainer/NavigationLabel");
    window->subscribeEvent(CEGUI::Window::EventMouseEntersArea, CEGUI::Event::Subscriber(&MenuState::handleCheckIfNaviIconAnimationNeedsChange, this));
    window->subscribeEvent(CEGUI::Window::EventMouseLeavesArea, CEGUI::Event::Subscriber(&MenuState::handleCheckIfNaviIconAnimationNeedsChange, this));
    window = _layout->getChild("BotNavigationContainer/NaviCenterContainer/NavigationIcon");
    window->subscribeEvent(CEGUI::Window::EventMouseEntersArea, CEGUI::Event::Subscriber(&MenuState::handleCheckIfNaviIconAnimationNeedsChange, this));
    window->subscribeEvent(CEGUI::Window::EventMouseLeavesArea, CEGUI::Event::Subscriber(&MenuState::handleCheckIfNaviIconAnimationNeedsChange, this));
    window = _layout->getChild("BotNavigationContainer/NaviCenterContainer/NaviBotSelectionIcon");
    window->subscribeEvent(CEGUI::Window::EventMouseEntersArea, CEGUI::Event::Subscriber(&MenuState::handleCheckIfNaviIconAnimationNeedsChange, this));
    window->subscribeEvent(CEGUI::Window::EventMouseLeavesArea, CEGUI::Event::Subscriber(&MenuState::handleCheckIfNaviIconAnimationNeedsChange, this));
}

void MenuState::setupInnerButtonsSubOptionsLabels()
{
    CEGUI::Window* label;
    label = _layout->getChild("InnerButtonsContainer/PopupLinesRanking/LabelRanking");
    label->subscribeEvent(CEGUI::Window::EventMouseEntersArea, CEGUI::Event::Subscriber(&MenuState::handleInnerButtonsLabelEntered, this));
    label->subscribeEvent(CEGUI::Window::EventMouseLeavesArea, CEGUI::Event::Subscriber(&MenuState::handleInnerButtonsLabelLeft, this));
    label->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&MenuState::ranking, this));
    label = _layout->getChild("InnerButtonsContainer/PopupLinesInstructions/LabelInstructions");
    label->subscribeEvent(CEGUI::Window::EventMouseEntersArea, CEGUI::Event::Subscriber(&MenuState::handleInnerButtonsLabelEntered, this));
    label->subscribeEvent(CEGUI::Window::EventMouseLeavesArea, CEGUI::Event::Subscriber(&MenuState::handleInnerButtonsLabelLeft, this));
    label->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&MenuState::instructions, this));
    label = _layout->getChild("InnerButtonsContainer/PopupLinesCharacters/LabelCredits");
    label->subscribeEvent(CEGUI::Window::EventMouseEntersArea, CEGUI::Event::Subscriber(&MenuState::handleInnerButtonsLabelEntered, this));
    label->subscribeEvent(CEGUI::Window::EventMouseLeavesArea, CEGUI::Event::Subscriber(&MenuState::handleInnerButtonsLabelLeft, this));
    label->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&MenuState::credits, this));

    label = _layout->getChild("InnerButtonsContainer/PopupLinesQuit/LabelYes");
    label->subscribeEvent(CEGUI::Window::EventMouseEntersArea, CEGUI::Event::Subscriber(&MenuState::handleInnerButtonsLabelEntered, this));
    label->subscribeEvent(CEGUI::Window::EventMouseLeavesArea, CEGUI::Event::Subscriber(&MenuState::handleInnerButtonsLabelLeft, this));
    label->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&MenuState::close, this));
    label = _layout->getChild("InnerButtonsContainer/PopupLinesQuit/LabelNo");
    label->subscribeEvent(CEGUI::Window::EventMouseEntersArea, CEGUI::Event::Subscriber(&MenuState::handleInnerButtonsLabelEntered, this));
    label->subscribeEvent(CEGUI::Window::EventMouseLeavesArea, CEGUI::Event::Subscriber(&MenuState::handleInnerButtonsLabelLeft, this));
}

void MenuState::makeAllSelectionIconsInvisible()
{
    CEGUI::EventArgs fireArgs;

    _layout->getChild("InnerButtonsContainer/InstructionsSelectionIcon")->setVisible(false);
    _layout->getChild("InnerButtonsContainer/NoSelectionIcon")->setVisible(false);
    _layout->getChild("InnerButtonsContainer/YesSelectionIcon")->setVisible(false);
    _layout->getChild("InnerButtonsContainer/RankingSelectionIcon")->setVisible(false);
    _layout->getChild("InnerButtonsContainer/CreditsSelectionIcon")->setVisible(false);
}

void MenuState::setupNaviArrowWindows()
{
    d_botNaviLeftArrowArea = _layout->getChild("BotNavigationContainer/LeftArrowArea");
    d_botNaviRightArrowArea = _layout->getChild("BotNavigationContainer/RightArrowArea");
    d_botNaviLeftArrowArea->subscribeEvent(CEGUI::Window::EventMouseEntersArea, CEGUI::Event::Subscriber(&MenuState::handleMouseEntersLeftArrowArea, this));
    d_botNaviLeftArrowArea->subscribeEvent(CEGUI::Window::EventMouseLeavesArea, CEGUI::Event::Subscriber(&MenuState::handleMouseLeavesLeftArrowArea, this));
    d_botNaviRightArrowArea->subscribeEvent(CEGUI::Window::EventMouseEntersArea, CEGUI::Event::Subscriber(&MenuState::handleMouseEntersRightArrowArea, this));
    d_botNaviRightArrowArea->subscribeEvent(CEGUI::Window::EventMouseLeavesArea, CEGUI::Event::Subscriber(&MenuState::handleMouseLeavesRightArrowArea, this));
}

bool MenuState::handleMouseEntersLeftArrowArea(const CEGUI::EventArgs&)
{
    CEGUI::EventArgs fireArgs;
    _layout->getChild("BotNavigationContainer/LeftArrow")->fireEvent("StartTwitching", fireArgs);

    return false;
}

bool MenuState::handleMouseLeavesLeftArrowArea(const CEGUI::EventArgs&)
{
    CEGUI::EventArgs fireArgs;
    _layout->getChild("BotNavigationContainer/LeftArrow")->fireEvent("EndTwitching", fireArgs);

    return false;
}

bool MenuState::handleMouseEntersRightArrowArea(const CEGUI::EventArgs&)
{
    CEGUI::EventArgs fireArgs;
    _layout->getChild("BotNavigationContainer/RightArrow")->fireEvent("StartTwitching", fireArgs);

    return false;
}

bool MenuState::handleMouseLeavesRightArrowArea(const CEGUI::EventArgs&)
{
    CEGUI::EventArgs fireArgs;
    _layout->getChild("BotNavigationContainer/RightArrow")->fireEvent("EndTwitching", fireArgs);

    return false;
}

void MenuState::setupButtonClickHandlers()
{
    CEGUI::Window* buttonInstructions = _layout->getChild("InnerButtonsContainer/ButtonInstructions");
    buttonInstructions->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&MenuState::handleStartPopupLinesInstructionsDisplay, this));
    CEGUI::Window* buttonRanking = _layout->getChild("InnerButtonsContainer/ButtonRanking");
    buttonRanking->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&MenuState::handleStartPopupLinesRankingDisplay, this));
    CEGUI::Window* buttonCharacters = _layout->getChild("InnerButtonsContainer/ButtonCharacters");
    buttonCharacters->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&MenuState::handleStartPopupLinesCharactersDisplay, this));
    CEGUI::Window* buttonQuit = _layout->getChild("InnerButtonsContainer/ButtonQuit");
    buttonQuit->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&MenuState::handleStartPopupLinesQuitDisplay, this));
}

void MenuState::stopStartPopupLinesAnimations()
{
    d_popupLinesCharactersAnimInst->setPosition(d_popupLinesRankingAnimInst->getDefinition()->getDuration());
    d_popupLinesCharactersAnimInst->apply();
    d_popupLinesCharactersAnimInst->pause();
    d_popupLinesRankingAnimInst->setPosition(d_popupLinesRankingAnimInst->getDefinition()->getDuration());
    d_popupLinesRankingAnimInst->apply();
    d_popupLinesRankingAnimInst->pause();
    d_popupLinesInstructionsAnimInst->setPosition(d_popupLinesInstructionsAnimInst->getDefinition()->getDuration());
    d_popupLinesInstructionsAnimInst->apply();
    d_popupLinesInstructionsAnimInst->pause();
    d_popupLinesQuitAnimInst->setPosition(d_popupLinesQuitAnimInst->getDefinition()->getDuration());
    d_popupLinesQuitAnimInst->apply();
    d_popupLinesQuitAnimInst->pause();

    _layout->getChild("InnerButtonsContainer/PopupLinesInstructions")->setVisible(false);
    _layout->getChild("InnerButtonsContainer/PopupLinesRanking")->setVisible(false);
    _layout->getChild("InnerButtonsContainer/PopupLinesCharacters")->setVisible(false);
    _layout->getChild("InnerButtonsContainer/PopupLinesQuit")->setVisible(false);

}

bool MenuState::handleStartPopupLinesInstructionsDisplay(const CEGUI::EventArgs&)
{
    if(!d_startButtonClicked)
        return false;

    makeAllSelectionIconsInvisible();

    stopStartPopupLinesAnimations();
    d_popupLinesInstructionsAnimInst->start();

    _layout->getChild("InnerButtonsContainer/InstructionsSelectionIcon")->setVisible(true);
    _layout->getChild("InnerButtonsContainer/PopupLinesInstructions")->setVisible(true);

    return false;
}

bool MenuState::handleStartPopupLinesRankingDisplay(const CEGUI::EventArgs&)
{
    if(!d_startButtonClicked)
        return false;

    makeAllSelectionIconsInvisible();

    stopStartPopupLinesAnimations();
    d_popupLinesRankingAnimInst->start();

    _layout->getChild("InnerButtonsContainer/RankingSelectionIcon")->setVisible(true);
    _layout->getChild("InnerButtonsContainer/PopupLinesRanking")->setVisible(true);

    return false;
}

bool MenuState::handleStartPopupLinesCharactersDisplay(const CEGUI::EventArgs&)
{
    if(!d_startButtonClicked)
        return false;

    makeAllSelectionIconsInvisible();

    stopStartPopupLinesAnimations();
    d_popupLinesCharactersAnimInst->start();

    _layout->getChild("InnerButtonsContainer/PopupLinesCharacters")->setVisible(true);
   _layout->getChild("InnerButtonsContainer/CreditsSelectionIcon")->setVisible(true);


    return false;
}


bool MenuState::handleStartPopupLinesQuitDisplay(const CEGUI::EventArgs&)
{
    if(!d_startButtonClicked)
        return false;

    makeAllSelectionIconsInvisible();

    stopStartPopupLinesAnimations();
    d_popupLinesQuitAnimInst->start();

    _layout->getChild("InnerButtonsContainer/YesSelectionIcon")->setVisible(true);
    _layout->getChild("InnerButtonsContainer/NoSelectionIcon")->setVisible(true);
    _layout->getChild("InnerButtonsContainer/PopupLinesQuit")->setVisible(true);

    return false;
}

CEGUI::Window* MenuState::getIconWindowFromLabel(CEGUI::Window* window)
{
    const CEGUI::String windowPrefix = "Label";
    const CEGUI::String iconPostfix = "SelectionIcon";

    CEGUI::String windowName = window->getName();

    windowName = windowName.substr(windowPrefix.length());
    return window->getParent()->getParent()->getChild(windowName + iconPostfix);
}


bool MenuState::handleInnerButtonsLabelEntered(const CEGUI::EventArgs& args)
{
    const CEGUI::MouseEventArgs& mouseArgs = static_cast<const CEGUI::MouseEventArgs&>(args);

    CEGUI::Window* iconWindow = getIconWindowFromLabel(mouseArgs.window);

    CEGUI::EventArgs fireArgs;
    iconWindow->fireEvent("StartRotate", fireArgs);

    return false;
}

bool MenuState::handleInnerButtonsLabelLeft(const CEGUI::EventArgs& args)
{
    const CEGUI::MouseEventArgs& mouseArgs = static_cast<const CEGUI::MouseEventArgs&>(args);

    CEGUI::Window* iconWindow = getIconWindowFromLabel(mouseArgs.window);

    CEGUI::EventArgs fireArgs;
    iconWindow->fireEvent("StopRotate", fireArgs);

    return false;
}

bool MenuState::handleLoginAcceptButtonClicked(const CEGUI::EventArgs&)
{
    d_startButtonClickArea->setAlpha(0.0f);
    d_startButtonBlendInAnimInst->start();

    enableNavigationBarElements();

    d_navigationTravelIcon->setEnabled(true);


    d_timeSinceLoginAccepted = 0.0f;
    d_currentWriteFocus = WF_TopBar;

    d_botNaviContainer->setEnabled(true);


    //
    d_interactiveElementsWereInitiallyBlendedOut = true;
    d_interactivePlanetElementsAreEnabled = true;
    d_startButtonClicked = true;
    //

    return false;
}

void MenuState::enableNavigationBarElements()
{
    d_naviPartialBlendOutInst->pause();
    d_naviBlendInInst->start();

    d_navigationLabelPartialBlendOutInst->stop();
    d_navigationLabelBlendInAnimInst->start();

    d_navigationIsEnabled = true;
}

bool MenuState::handleInnerPartStartClickAreaClick(const CEGUI::EventArgs&)
{
    if(!d_interactivePlanetElementsAreEnabled)
        enableInteractivePlanetElements();

    d_startButtonBlendInAnimInst->pause();
    d_startButtonClickArea->setVisible(false);

    d_startButtonClicked = true;
    changeState(PlayState::getSingletonPtr());

    return false;
}

void MenuState::enableInteractivePlanetElements()
{
    d_centerButtonsPartialBlendOutInst->pause();
    d_centerButtonsBlendInInst->start();

    d_interactivePlanetElementsAreEnabled = true;
}


bool MenuState::handleCheckIfNaviIconAnimationNeedsChange(const CEGUI::EventArgs&)
{
    bool mouseIsHovering = false;
    CEGUI::Window* window;

    window = _layout->getChild("BotNavigationContainer/NaviCenterContainer/NavigationLabel");
    mouseIsHovering |= window->isMouseContainedInArea();
    window = _layout->getChild("BotNavigationContainer/NaviCenterContainer/NavigationIcon");
    mouseIsHovering |= window->isMouseContainedInArea();
    window = _layout->getChild("BotNavigationContainer/NaviCenterContainer/NaviBotSelectionIcon");
    mouseIsHovering |= window->isMouseContainedInArea();
    mouseIsHovering |= d_botNaviRightArrowArea->isMouseContainedInArea();
    mouseIsHovering |= d_botNaviLeftArrowArea->isMouseContainedInArea();

    //We fire an event to trigger the animation depending on if the mouse hovers a critical
    //window or not. Additionally we perform a check to not fire an event for an animation that is already running
    if(d_mouseIsHoveringNavi != mouseIsHovering)
    {
        d_mouseIsHoveringNavi = mouseIsHovering;
        CEGUI::EventArgs args;

        if(mouseIsHovering)
        {
            d_navigationSelectionIcon->fireEvent("StartRotate", args);
            d_navigationTravelIcon->fireEvent("StartRotate", args);
        }
        else
        {
            d_navigationSelectionIcon->fireEvent("StopRotate", args);
            d_navigationTravelIcon->fireEvent("StopRotate", args);
        }
    }

    return false;
}

bool MenuState::handleNaviSelectionIconAnimStart(const CEGUI::EventArgs& args)
{
    const CEGUI::AnimationEventArgs& animArgs = static_cast<const CEGUI::AnimationEventArgs&>(args);

    CEGUI::AnimationInstance* animInst = animArgs.instance;

    if(animInst->getDefinition() == CEGUI::AnimationManager::getSingleton().getAnimation("LoopRotateRight"))
    {
        CEGUI::Quaternion curRotation = animInst->getTarget()->getProperty<CEGUI::Quaternion>("Rotation");

        float curAngle = getAngle(curRotation);
        float progress = curAngle / (2.0f * static_cast<float>(M_PI));
        animInst->setPosition(progress * animInst->getDefinition()->getDuration());
    }

    return false;
}

float MenuState::getAngle(const CEGUI::Quaternion& quat)
{
    return 2.0f * std::acos(quat.d_w);
}


bool MenuState::handleLeftArrowClick(const CEGUI::EventArgs&)
{
    i--;
    d_naviLabel->setText(_list_phases[i%_list_phases.size()]);
    std::cout << std::to_string(i%_list_phases.size()) << std::endl;

    return false;
}

bool MenuState::handleRightArrowClick(const CEGUI::EventArgs&)
{
    i++;
    d_naviLabel->setText(_list_phases[i%_list_phases.size()]);
    std::cout << std::to_string(i%_list_phases.size()) << std::endl;

    return false;
}
bool MenuState::handleNaviLabelClick(const CEGUI::EventArgs&)
{
    std::cout << std::to_string(i%_list_phases.size()) << std::endl;

    return false;
}
