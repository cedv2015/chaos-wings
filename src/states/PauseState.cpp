#include "PauseState.h"
#include "Player.h"
#include "TrackManager.h"
#include "GameObjectManager.h"

template <> PauseState* Ogre::Singleton<PauseState>::msSingleton = nullptr;

PauseState::PauseState () :  _root(nullptr), _sceneMgr(nullptr), _camera(nullptr), _pause(nullptr), _keyboard(nullptr), _mouse(nullptr) ,_exit(false){
}
PauseState& PauseState::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}
PauseState* PauseState::getSingletonPtr () {
    return msSingleton;
}

void PauseState::enter () {
    std::cout << "Entered in PauseState" << std::endl;
    _root = Ogre::Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().show( );
    _pause = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("pause.layout");
    CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_pause);
    _pause->getChild("ButtonBack")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&PauseState::play, this));
    _pause->getChild("ButtonQuit")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&PauseState::quit, this));
    _pause->getChild("Button")->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&PauseState::menu, this));
    GameObjectManager::getSingletonPtr()->hideHUD();
    TrackManager::getSingletonPtr()->setLowVolume();
}

void PauseState::exit () {
    CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->removeChild(_pause);
    GameObjectManager::getSingletonPtr()->showHUD();
    TrackManager::getSingletonPtr()->setHighVolume();
}
void PauseState::pause() {}
void PauseState::resume() {}

void PauseState::keyPressed (const OIS::KeyEvent &e) {}
void PauseState::keyReleased (const OIS::KeyEvent &e) {
    switch (e.key) {
        case OIS::KC_P:
            CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().hide( );
            popState();
            break;
        default:
            break;
    }
}

CEGUI::MouseButton PauseState::convertMouseButton(OIS::MouseButtonID id)
{
    CEGUI::MouseButton ceguiId;
    switch(id)
    {
        case OIS::MB_Left:
            ceguiId = CEGUI::LeftButton;
            break;
        case OIS::MB_Right:
            ceguiId = CEGUI::RightButton;
            break;
        case OIS::MB_Middle:
            ceguiId = CEGUI::MiddleButton;
            break;
        default:
            ceguiId = CEGUI::LeftButton;
    }
    return ceguiId;
}

void PauseState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(e.state.X.abs, e.state.Y.abs);
}
void PauseState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}
void PauseState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

bool PauseState::frameStarted (const Ogre::FrameEvent &e) {
    return !_exit;
}
bool PauseState::frameEnded (const Ogre::FrameEvent &e) {
    return !_exit;
}


bool PauseState::povMoved(const OIS::JoyStickEvent &e, int pov) {return true;}
bool PauseState::axisMoved(const OIS::JoyStickEvent &e, int axis) {return true;}
bool PauseState::sliderMoved(const OIS::JoyStickEvent &e, int sliderID) {return true;}
bool PauseState::buttonPressed(const OIS::JoyStickEvent &e, int button) {return true;}
bool PauseState::buttonReleased(const OIS::JoyStickEvent &e, int button) {return true;}

bool PauseState::quit(const CEGUI::EventArgs &e)
{
    _exit = true;
    return true;
}

bool PauseState::menu(const CEGUI::EventArgs &e)
{
    this->popState();
    _pause->hide();
    PlayState::getSingletonPtr()->setMenu(true);
    return true;
}

bool PauseState::play(const CEGUI::EventArgs &e)
{
    this->popState();
    _pause->hide();
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().hide( );
    return true;
}
