#include "PlayState.h"
#include "GameObject.h"
#include "MenuState.h"
#include "Phase.h"
#include "EndGameState.h"
#include "DialogueState.h"
#include "CEGUIComponent.h"


template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = nullptr;


PlayState::PlayState () : _root(nullptr), _sceneMgr(nullptr), _camera(nullptr), _node(nullptr), _entity(nullptr), _exit(false), _controllMouse(true), _menu(false), _finishPhase(false), _finishDialogue(false), _newStage(true), _gameObjectMgr(nullptr), _gameObjectCreator(nullptr), _physicsMgr(nullptr), _hud(nullptr), _fps(nullptr), _gameObjectLabel(nullptr), _enemyLoader(nullptr), _time_fps(0), _phase(0), _percentaje_until_boss(0), _numDialogue(0), _trackMgr(nullptr) {

    _gameObjectMgr = GameObjectManager::getSingletonPtr();
    _gameObjectCreator = GameObjectCreator::getSingletonPtr();
    _cameraMgr = CameraManager::getSingletonPtr();
    _enemyLoader = EnemyLoader::getSingletonPtr();
    _rankingMgr = RankingManager::getSingletonPtr();
    _dialogueMgr = DialogueManager::getSingletonPtr();
    _trackMgr = TrackManager::getSingletonPtr();
}

PlayState::~PlayState () {

}

PlayState* PlayState::getSingletonPtr () {
    return msSingleton;
}

PlayState& PlayState::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}

void PlayState::enter () {
    _phase = 0;
    _time_dialogue = 0.0;
    _time_fade = 0.0;
    _time_new_stage = 0.0;
    std::cout << "Entered in PlayState" << std::endl;
    _root = Ogre::Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");
    _camera->setPosition(Ogre::Vector3::ZERO);
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    _physicsMgr = PhysicsManager::getSingletonPtr();
    _particlesMgr = ParticlesManager::getSingletonPtr();


    if(_hud == nullptr){
        initCEGUI();
        setupAnimations();
    }else
        _hud->show();

    if(getControllMouse())
        _gameObjectCreator->createPlayerMouse();
    else
        _gameObjectCreator->createPlayerJoystick();
    addStage();


    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().hide( );

    Ogre::Light *light = _sceneMgr->createLight("MainLight");
    light->setType(Ogre::Light::LT_DIRECTIONAL);
    light->setDirection(Ogre::Vector3(0, -1, 0));

    _finishPhase = false;
    _rankingMgr->resetScore();
}

void PlayState::exit () {
    _gameObjectMgr->removeAllObjects();
    _cameraMgr->clearPlayers();
    _gameObjectCreator->resetCounters();

    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().show( );

    _particlesMgr->destroyAllParticles();

    _sceneMgr->clearScene();
    _hud->hide();
    _gameObjectCreator->setPlayerJoystick(false);
    _gameObjectCreator->setPlayerMouse(false);
    _dialogueMgr->resetDialogue();
}

void PlayState::pause () {

}

void PlayState::resume () {

}

void PlayState::keyPressed (const OIS::KeyEvent &e) {
    switch (e.key) {
        default:
            break;
    }
}

void PlayState::keyReleased (const OIS::KeyEvent &e) {
    switch (e.key) {
        case OIS::KC_ESCAPE:
            _exit = true;
            break;
        case OIS::KC_O:
            changeState(MenuState::getSingletonPtr());
            TrackManager::getSingletonPtr()->fadeToSong("Menu_song.wav");
            break;
        case OIS::KC_N:
            if(!_gameObjectCreator->getPlayerMouse())
                _gameObjectCreator->createPlayerMouse();
            break;
        case OIS::KC_P:
            this->pushState (PauseState::getSingletonPtr());
            break;
        case OIS::KC_A:
            changeVisibilityInfo();
            break;
        case OIS::KC_M:
            _backgroundAnimInst->start();
            break;
        case OIS::KC_L:
            _cameraMgr->switchState();
            break;
        default:
            break;
    }
}

void PlayState::addStage(){
    _phase++;
    _numDialogue = 0;
    _newStage = true;
    _time_new_stage = 0.0;
    _hud->getChild("Info")->setVisible(false);
    switch(_phase){
        case 1:
            _sceneMgr->setSkyBox(true, "SkyBox", 100);
            _phaseStage =_gameObjectCreator->createPhase(_phase);
            _enemyLoader->loadPhase(_phase);
            break;
        case 2:

            visibilityHUDPhase(false);
            _gameObjectMgr->removeActualPhase();
            _phaseStage =_gameObjectCreator->createPhase(_phase);
            _enemyLoader->loadPhase(_phase);
            break;
        default:
            visibilityHUD(false);
            pushState(EndGameState::getSingletonPtr());
            break;
    }
    _trackMgr->fadeToSong("Phase"+std::to_string(_phase)+"_song.wav", 1.0);

}

void PlayState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(e.state.X.abs, e.state.Y.abs);
}

void PlayState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    return;
};
void PlayState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    return;
};

bool PlayState::povMoved(const OIS::JoyStickEvent &e, int pov) {return true;}
bool PlayState::axisMoved(const OIS::JoyStickEvent &e, int axis) {return true;}
bool PlayState::sliderMoved(const OIS::JoyStickEvent &e, int sliderID) {return true;}
bool PlayState::buttonPressed(const OIS::JoyStickEvent &e, int button) {return true;}
bool PlayState::buttonReleased(const OIS::JoyStickEvent &e, int button) {
    switch (button) {
        case START_BUTTON:
            if(!_gameObjectCreator->getPlayerJoystick())
                _gameObjectCreator->createPlayerJoystick();
            break;
        default:
            break;
    }
    return true;
}

bool PlayState::frameStarted (const Ogre::FrameEvent &evt) {
    Ogre::Real delta = evt.timeSinceLastFrame;
    CEGUI::System::getSingleton().injectTimePulse(evt.timeSinceLastFrame);
    _physicsMgr->update(delta);
    _gameObjectMgr->update(delta);
    _cameraMgr->update(delta);
    _enemyLoader->update(delta);
    _dialogueMgr->update(delta);
    _scoreLabel->setText(Ogre::StringConverter::toString(_rankingMgr->getScore()));
    _particlesMgr->update(delta);
    _trackMgr->update(delta);
    _time_dialogue+=delta;
    _time_fade+=delta;
    _time_new_stage+=delta;
    checkGameState();

    int fps = int(1.0 / delta);
    _time_fps += delta;
    _sum_fps += fps;
    _count_fps += 1;
    if (_time_fps > 1) {
        info((int)(_sum_fps / _count_fps));
        _sum_fps =_count_fps =_time_fps = 0;
    }
    _percentaje_until_boss = _enemyLoader->getPercentajeUntilBoss();
    // TODO: show the percentaje until boss in hud
    if(_menu){
        changeState(MenuState::getSingletonPtr());
        TrackManager::getSingletonPtr()->fadeToSong("Menu_song.wav");
        setMenu(false);
    }
    return !_exit;
};
bool PlayState::frameEnded (const Ogre::FrameEvent &evt) {
    return !_exit;
};



void PlayState::initCEGUI(){
    _hud = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("PlayMenu.layout");
    CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_hud);
    _fps = _hud->getChild("Info/FPSLabel");
    _gameObjectLabel = _hud->getChild("Info/GOLabel");
    _label1 = _hud->getChild("Info/Label1");
    _scoreLabel = _hud->getChild("Score/ScoreLabel");
    _score = _hud->getChild("Score");
}

void PlayState::info(float fps){
    _fps->setText(Ogre::String("FPS:") + Ogre::StringConverter::toString(fps));
    _gameObjectLabel->setText(Ogre::String("GO:") + Ogre::StringConverter::toString(_gameObjectMgr->getNumGameObject()));
    _label1->setText(Ogre::String("Part:") + Ogre::StringConverter::toString(_particlesMgr->getNumberOfParticles()));

}

void PlayState::changeVisibilityInfo(){
    CEGUI::Window* info = _hud->getChild("Info");
    bool visible = info->isVisible();
    info->setVisible(!visible);

}

void PlayState::setupAnimations(){

  CEGUI::AnimationManager& animMgr = CEGUI::AnimationManager::getSingleton();

  CEGUI::Animation* totBarAnim = animMgr.getAnimation("TopBarScoreMoveInAnimation");
  d_totBarAnimInst = animMgr.instantiateAnimation(totBarAnim);
  d_totBarAnimInst->setTargetWindow(_score);

  CEGUI::Animation* backgroundAnim = animMgr.getAnimation("BackgroundAlphaOut");
  _backgroundAnimInst = animMgr.instantiateAnimation(backgroundAnim);
  _backgroundAnimInst->setTargetWindow(_hud->getChild("EndPhaseBackground"));
}

void PlayState::checkGameState(){
    if (_cameraMgr->getNumberOfPlayers()==0){
        _dialogueMgr->resetDialogue();
        pushState(GameOverState::getSingletonPtr());
    }
    if(_finishPhase){
        if(_time_dialogue>TIME_DELAY){
            _finishPhase = false;
            DialogueState::getSingletonPtr()->setDialogue(_phase,2); //Ultimo dialogo
            this->pushState (DialogueState::getSingletonPtr());
            _finishDialogue = true;
            _time_fade = 0.0f;
        }

    }
    if(_finishDialogue){
        if(_time_fade>TIME_FADE_DELAY){
            _finishDialogue = false;
            addStage();
        }
    }
    if(_newStage){
        if(_time_new_stage>TIME_NEW_STAGE_DELAY){
            _newStage = false;
            DialogueState::getSingletonPtr()->setDialogue(_phase,1);
            this->pushState (DialogueState::getSingletonPtr());

        }
    }

}


void PlayState::finishPhase() {
    _finishPhase = true;
    _time_dialogue = 0.0f;
};

void PlayState::finishDialogue(){
    _numDialogue++;
    if(_numDialogue == 2){ //El dialogo que indica el final de fase
        if(_phase<2)
            _backgroundAnimInst->start();
    }
    visibilityHUDPhase(false);
}

void PlayState::visibilityHUDPhase(bool visibility){
      std::static_pointer_cast<CEGUIComponent>(_phaseStage->getComponent(Component::Type::CEGUI))->visibilityHUD(visibility);
}
