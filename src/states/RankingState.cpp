#include "RankingState.h"
#include "PlayState.h"
#include "MenuState.h"
#include "GameManager.h"

template<> RankingState* Ogre::Singleton<RankingState>::msSingleton = nullptr;

RankingState::RankingState () :_root(nullptr),  _sceneMgr(nullptr), _camera(nullptr), _keyboard(nullptr), _mouse(nullptr), _exit(false), _records(nullptr), _rankingManager(nullptr) {
    _rankingManager = RankingManager::getSingletonPtr();
}

RankingState* RankingState::getSingletonPtr () {
    return msSingleton;
}

RankingState& RankingState::getSingleton () {
    assert (msSingleton);
    return *msSingleton;
}

void RankingState::enter () {
	_root = Ogre::Root::getSingletonPtr();
	_sceneMgr = _root->getSceneManager("SceneManager");
	_camera = _sceneMgr->getCamera("MainCamera");
  _camera->setPosition(Ogre::Vector3::ZERO);
	_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
  std::cout << "Entered in RankingState" << std::endl;
  rankingShow();

}

void RankingState::exit () {
	  _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
    _records->hide();
}

void RankingState::pause () {}

void RankingState::resume () {}

bool RankingState::frameStarted (const Ogre::FrameEvent &evt) {
    CEGUI::System::getSingleton().injectTimePulse(evt.timeSinceLastFrame);
    return !_exit;
}

bool RankingState::frameEnded (const Ogre::FrameEvent &evt) {
    return !_exit;
}

void RankingState::keyPressed (const OIS::KeyEvent &e) {
    switch (e.key) {
        default:
            break;
    }
}

void RankingState::keyReleased (const OIS::KeyEvent &e) {
    switch (e.key) {
        case OIS::KC_ESCAPE:
            _exit = true;
            break;
        case OIS::KC_P:
            changeState(PlayState::getSingletonPtr());
            break;
        default:
            break;
    }
}

void RankingState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(e.state.X.abs, e.state.Y.abs);
}
void RankingState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertMouseButton(id));
}
void RankingState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertMouseButton(id));
}

CEGUI::MouseButton RankingState::convertMouseButton(OIS::MouseButtonID id)
{
  CEGUI::MouseButton ceguiId;
  switch(id)
    {
    case OIS::MB_Left:
      ceguiId = CEGUI::LeftButton;
      break;
    case OIS::MB_Right:
      ceguiId = CEGUI::RightButton;
      break;
    case OIS::MB_Middle:
      ceguiId = CEGUI::MiddleButton;
      break;
    default:
      ceguiId = CEGUI::LeftButton;
    }
  return ceguiId;
}

void RankingState::rankingShow()
{
    if(_records == NULL){
      _records = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("Ranking.layout");
      CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_records);
      setupWindows();
      resetAnimations();
    }else{
        _records->show();
        resetAnimations();
    }

    std::vector<std::string> ranking = _rankingManager->getRanking();
    std::string name, score;
    for (unsigned int i=0; i<ranking.size(); i+=2){
        name = std::string("RankingContainer/Name") + std::to_string(i/2+1);
        score = std::string("RankingContainer/Score") + std::to_string(i/2+1);
        CEGUI::Window* name_record = _records->getChild(name);
        name_record->setText(ranking[i]);
        CEGUI::Window* score_record = _records->getChild(score);
        score_record ->setText(ranking[i+1]);
    }
    _sceneMgr->setSkyBox(true, "SkyBox", 100);
    Ogre::Entity *entity = _sceneMgr->createEntity("ScenarioRanking", "Scenario1.mesh");
    Ogre::SceneNode *node = _sceneMgr->createSceneNode("ScenarioRanking_Node");
    _sceneMgr->getRootSceneNode()->addChild(node);
    node->attachObject(entity);
    node->translate(Ogre::Vector3(0,-20,0));

}

void RankingState::resetAnimations(){
    d_size->start();
    d_topBarAnimInst->start();
    d_botBarAnimInst->start();
    d_topBarLabel->setText("Ranking");
    d_buttonBack->start();
    d_fistPlace->start();
    d_secondPlace->start();
    d_thirdPlace->start();


}

bool RankingState::hide(const CEGUI::EventArgs &e)
{
    _records->hide();
    changeState(RankingState::getSingletonPtr());
    return true;

}

CEGUI::MouseButton RankingState::convertJoystickButton(int id)
{
    CEGUI::MouseButton ceguiId;
    switch(id)
    {
        case BUTTON_A:
            ceguiId = CEGUI::RightButton;
            break;
        case BUTTON_B:
            ceguiId = CEGUI::LeftButton;
            break;
        default:
            break;
    }
    return ceguiId;
}

bool RankingState::povMoved(const OIS::JoyStickEvent &e, int pov) {
    CEGUI::Vector2f mousePos = CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().getPosition();
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition(mousePos.d_x-0.1,0);
    return true;
}
bool RankingState::axisMoved(const OIS::JoyStickEvent &e, int axis) {
    PlayState::getSingletonPtr()->setControllMouse(false);
    Ogre::RenderWindow* render = GameManager::getSingletonPtr()->getRenderWindow();
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMousePosition((e.state.mAxes[0].abs+RANGE_JOYSTICK)/(RANGE_JOYSTICK-(-RANGE_JOYSTICK))*render->getWidth()+1, (e.state.mAxes[1].abs+RANGE_JOYSTICK)/(RANGE_JOYSTICK-(-RANGE_JOYSTICK))*render->getHeight()+1);

    return true;
}
bool RankingState::sliderMoved(const OIS::JoyStickEvent &e, int sliderID) {return true;}
bool RankingState::buttonPressed(const OIS::JoyStickEvent &e, int button) {
    // e.state.buttonDown(button)
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(convertJoystickButton(button));
    return true;
}
bool RankingState::buttonReleased(const OIS::JoyStickEvent &e, int button) {
    CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(convertJoystickButton(button));
    return true;
}

void RankingState::setupWindows()
{
    d_botBarLabel = _records->getChild("BotBar/BotBarLabel");
    d_topBarLabel = _records->getChild("TopBar/TopBarLabel");

    CEGUI::AnimationManager& animMgr = CEGUI::AnimationManager::getSingleton();


    CEGUI::Animation* topBarAnim = animMgr.getAnimation("TopBarMoveInAnimation");
    d_topBarAnimInst = animMgr.instantiateAnimation(topBarAnim);
    CEGUI::Window* topBarWindow = _records->getChild("TopBar");
    d_topBarAnimInst->setTargetWindow(topBarWindow);

    CEGUI::Animation* botBarAnim = animMgr.getAnimation("BotBarMoveInAnimation");
    d_botBarAnimInst = animMgr.instantiateAnimation(botBarAnim);
    CEGUI::Window* botBarWindow = _records->getChild("BotBar");
    d_botBarAnimInst->setTargetWindow(botBarWindow);

    CEGUI::Animation* alphaButtonAnim = animMgr.getAnimation("InsideBlendIn");

    CEGUI::Window* window = _records->getChild("ButtonBack");
    d_buttonBack = animMgr.instantiateAnimation(alphaButtonAnim);
    d_buttonBack->setTargetWindow(window);
    window->subscribeEvent(CEGUI::Window::EventMouseClick, CEGUI::Event::Subscriber(&RankingState::handleBackMenuClick, this));

    CEGUI::Animation* ButtonFirstAnim = animMgr.getAnimation("imageRanking1Rotate");

    window = _records->getChild("RankingContainer/FirstPlace");
    d_fistPlace = animMgr.instantiateAnimation(ButtonFirstAnim);
    d_fistPlace->setTargetWindow(window);

    CEGUI::Animation* ButtonSecondAnim = animMgr.getAnimation("imageRanking2Rotate");
    window = _records->getChild("RankingContainer/SecondPlace");
    d_secondPlace = animMgr.instantiateAnimation(ButtonSecondAnim);
    d_secondPlace->setTargetWindow(window);

    CEGUI::Animation* ButtonThirdAnim = animMgr.getAnimation("imageRanking3Rotate");
    window = _records->getChild("RankingContainer/ThirdPlace");
    d_thirdPlace = animMgr.instantiateAnimation(ButtonThirdAnim);
    d_thirdPlace->setTargetWindow(window);

    CEGUI::Animation* sizeAnim = animMgr.getAnimation("RankingFadeIn");

    window = _records->getChild("RankingContainer");
    d_size = animMgr.instantiateAnimation(sizeAnim);
    d_size->setTargetWindow(window);



}

bool RankingState::handleBackMenuClick(const CEGUI::EventArgs&)
{
    changeState(MenuState::getSingletonPtr());

    return false;
}
